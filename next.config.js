/** @type {import('next').NextConfig} */

module.exports = {
    typescript:{
       ignoreBuildErrors: true,
    },
    reactStrictMode: false,
    output: 'standalone',
}
