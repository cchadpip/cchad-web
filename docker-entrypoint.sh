#!/bin/sh
# Entrypoint básico para establecer la dirección de la API.

if [ "${#NEXT_PUBLIC_API_URL}" -gt 0 ]; then
    find .next \
        -type f \
        -exec sed -i "s|%NEXT_PUBLIC_API_URL%|${NEXT_PUBLIC_API_URL}|g" '{}' \; 2>/dev/null
else
    echo "Debes establecer NEXT_PUBLIC_API_URL para poder usar el contenedor."
    exit 1
fi

exec "$@"
