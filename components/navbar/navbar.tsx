"use client";
import {
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarItem,
} from "@nextui-org/react";
import { Button } from "@nextui-org/react";
import Link from "next/link";
import { useCallback, useEffect, useState } from "react";
import useUserStore from "@/config/storage/user";
import Swal from "sweetalert2";

export const NavBar = () => {
  console.log("Navbar");
  const [token, setToken] = useState<string | null>(null);
  const [userType, setUserType] = useState<string | null>(null);

  const types = useUserStore((state) => state.type);
  const tokens = useUserStore((state) => state.token);
  const removeUser = useUserStore((state) => state.clearUser);
  console.log(types);
  console.log(tokens);

  const handleComingSoonAlert = () => {
    Swal.fire({
      title: "Próximamente",
      text: "Esta funcionalidad estará disponible pronto.",
      icon: "info",
      confirmButtonText: "OK",
    });
  };
  console.log(types);
  console.log(tokens);

  useEffect(() => {
    // Actualiza el token y el tipo de usuario cada vez que se monta o desmonta el componente
    const updateAuth = () => {
      setToken(tokens);
      setUserType(types);
    };

    updateAuth(); // Llamada inicial para establecer el estado
    window.addEventListener("storage", updateAuth); // Escucha los cambios en localStorage

    return () => {
      window.removeEventListener("storage", updateAuth);
    };
  }, []);
  const isLoggedIn = Boolean(token);
  const Logout = useCallback(() => {
    removeUser();
    setToken(null); // Actualiza el estado después de logout
    setUserType(null);
  }, []);

  const renderMenuItems = useCallback(() => {
    switch (userType) {
      case "Administrador":
        return <></>;
      case "Secretaria":
        return <></>;
      case "Paciente":
        return (
          <>
            <NavbarItem>
              <Button
                as={Link}
                color="default"
                href="/patient/profile"
                variant="flat"
              >
                Perfil
              </Button>
            </NavbarItem>
            <NavbarItem></NavbarItem>
            <NavbarItem>
              <Button
                as={Link}
                color="default"
                href="/patient/viewAppointment"
                variant="flat"
              >
                Historial de citas
              </Button>
            </NavbarItem>
          </>
        );
      default:
        return null;
    }
  }, [userType]);

  return (
    <Navbar
      position="static"
      className="bg-custom-richblack text-white mytheme"
    >
      <NavbarBrand>
        <Link color="foreground" href="/">
          <p className="font-bold text-inherit">Clinica Cchad</p>
        </Link>
      </NavbarBrand>

      <NavbarContent justify="end">
        {isLoggedIn ? (
          <>
            {renderMenuItems()}
            <NavbarItem>
              <Button
                as={Link}
                color="default"
                variant="flat"
                href="/"
                onClick={Logout}
              >
                Logout
              </Button>
            </NavbarItem>
          </>
        ) : (
          <>
            <NavbarItem>
              <Button
                as={Link}
                color="primary"
                variant="ghost"
                href="/auth/Login"
              >
                Iniciar sesión
              </Button>
            </NavbarItem>
            <NavbarItem>
              <Button
                as={Link}
                color="primary"
                variant="flat"
                href="/auth/Register/patientRegister"
              >
                Registrarse
              </Button>
            </NavbarItem>
          </>
        )}
      </NavbarContent>
    </Navbar>
  );
};
