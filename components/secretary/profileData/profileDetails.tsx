"use client";

import { useState } from "react";
import { Input, Button } from "@nextui-org/react";
import { MailIcon, EditIcon } from "../../incons";

const ProfileDetails = ({ userData }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [name, setName] = useState(userData.name);
  const [lastName, setLastName] = useState(userData.lastName);
  const [email, setEmail] = useState(userData.email);
  const [phone, setPhone] = useState(userData.phone);

  const handleSubmit = (e) => {
    e.preventDefault();
    //llamar funcion para actualizar datos
    console.log("Datos actualizados:", {
      name,
      lastName,
      email,
      phone,
    });
    alert("Datos actualizados"); //cambiar
    window.location.href = "/medic/profile";
  };

  //validar datos

  const handleCancel = () => {
    setName(userData.name);
    setLastName(userData.lastName);
    setEmail(userData.email);
    setPhone(userData.phone);
    setIsEditing(false);
  };

  return (
    <form onSubmit={handleSubmit} className="p-4 rounded shadow-md">
      <div className="flex justify-between items-center mb-4">
        <h2 className="text-lg font-semibold mb-2">Datos Personales</h2>
        {!isEditing && (
          <Button isIconOnly onClick={() => setIsEditing(true)}>
            <EditIcon />
          </Button>
        )}
      </div>
      <div className="block text-bg font-semibold mb-2">
        <Input
          label="Nombre"
          labelPlacement="outside"
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          className="pb-4"
          isReadOnly={!isEditing}
        />
      </div>
      <div className="block text-bg font-semibold mb-2">
        <Input
          label="Apellido"
          labelPlacement="outside"
          type="text"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          className="pb-4"
          isReadOnly={!isEditing}
        />
      </div>
      <div className="block text-bg font-semibold mb-2">
        <Input
          label="Email"
          type="text"
          value={email}
          labelPlacement="outside"
          onChange={(e) => setEmail(e.target.value)}
          startContent={
            <MailIcon className="text-2xl text-default-400 pointer-events-none flex-shrink-0" />
          }
          className="pb-4"
          isReadOnly={!isEditing}
        />
      </div>
      <div className="block text-bg font-semibold mb-2">
        <Input
          isReadOnly={!isEditing}
          label="Teléfono"
          labelPlacement="outside"
          type="text"
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
          className="pb-4"
          startContent={
            <div className="pointer-events-none flex items-center">
              <span className="text-default-400 text-small">+56</span>
            </div>
          }
        />
      </div>
      {isEditing && (
        <div className="flex justify-between">
          <Button
            type="submit"
            className="w-full bg-blue-500 text-bg p-2 rounded hover:bg-blue-600 mr-2"
          >
            Guardar Cambios
          </Button>
          <Button
            color="danger"
            className="w-full text-bg p-2 rounded hover:bg-gray-600"
            onClick={handleCancel}
          >
            Cancelar
          </Button>
        </div>
      )}
    </form>
  );
};

export default ProfileDetails;
