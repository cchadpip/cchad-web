import { useState, useEffect, useMemo } from 'react';
import { getClient } from '@/config/graphql/apolloClient';
import { 
  GET_MEDICS_FOR_SECRETARY, 
  GET_MEDIC_FOR_SECRETARY, 
  GET_BOXES, 
  GET_BOX, 
  CREATE_MEDIC_SCHEDULES_MUTATION, 
  PUBLIC_MEDIC_SCHEDULE_MUTATION 
} from '@/config/graphql/mutations/serviceSecretary/serviceSchedule/service';
import moment from 'moment';
import { Schedule, Slot, Box, CalendarEvent, Medic } from '@/config/Interface/interface';
import Swal from 'sweetalert2';

interface TimeSlot {
  startTime: string;
  endTime: string;
}

interface ActiveDays {
  [key: string]: TimeSlot[];
}

interface Branch {
  name: string;
}

interface Appointment {
  id: number;
  state: string;
  confirmed: boolean;
  type: string;
  patient: {
    id: number;
    name: string;
    lastName: string;
  };
}


interface SlotWithSchedule {
  slot: Slot;
  scheduleEnable: boolean;
}

interface CalendarEventWithSlot extends CalendarEvent {
  slot: Slot;
}

export function useMedicScheduleLogic() {
  const [medics, setMedics] = useState<Medic[]>([]);
  const [selectedMedic, setSelectedMedic] = useState<number | null>(null);
  const [medic, setMedic] = useState<Medic | null>(null);
  const [activeDays, setActiveDays] = useState<ActiveDays>({
    Lunes: [],
    Martes: [],
    Miercoles: [],
    Jueves: [],
    Viernes: [],
    Sabado: [],
    Domingo: [],
  });
  const [selectedBox, setSelectedBox] = useState<number | null>(null);
  const [boxes, setBoxes] = useState<Box[]>([]);
  const [box, setBox] = useState<Box | null>(null);
  const [medicSchedules, setMedicSchedules] = useState<Schedule[]>([]);
  const [medicSlots, setMedicSlots] = useState<SlotWithSchedule[]>([]);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [selectedEvent, setSelectedEvent] = useState<any>(null);
  const [boxSchedule, setBoxSchedule] = useState<Schedule[]>([]);
  const [boxSlots, setBoxSlots] = useState<Slot[]>([]);

  useEffect(() => {
    fetchMedics();
    fetchBoxes();
  }, []);

  const fetchBoxes = async () => {
    try {
      const { data } = await getClient().query({ query: GET_BOXES });
      setBoxes(data.boxes);
    } catch (error) {
      handleError(error, 'Error fetching boxes');
    }
  };

  const fetchBox = async (boxID: number) => {
    try {
      const { data } = await getClient().query<{ box: Box }>({
        query: GET_BOX,
        variables: { id: boxID },
      });
      setBox(data.box);
      if (data.box && data.box.schedules) {
        setBoxSchedule(data.box.schedules);
      }
      
      const allSlots = data.box.schedules.flatMap(schedule => schedule.slots);
      setBoxSlots(allSlots);
      console.log('Box:', data.box.schedules);
      console.log('Box slots:', allSlots);
    } catch (error) {
      handleError(error, 'Error fetching box details');
    }
  };

  const handleBoxesSelectionChange = (boxID: number) => {
    setSelectedBox(boxID);
    fetchBox(boxID);
  };

  const fetchMedics = async () => {
    try {
      const { data } = await getClient().query({ query: GET_MEDICS_FOR_SECRETARY });
      setMedics(data.medics);
    } catch (error) {
      handleError(error, 'Error fetching medics');
    }
  };

  const fetchMedic = async (medicId: number) => {
    try {
      const { data } = await getClient().query<{ medic: Medic }>({
        query: GET_MEDIC_FOR_SECRETARY,
        variables: { id: medicId },
      });
      setMedic(data.medic);
      if (data.medic && data.medic.schedules) {
        setMedicSchedules(data.medic.schedules);
        const allSlotsWithSchedule = data.medic.schedules.flatMap(schedule =>
          schedule.slots.map(slot => ({
            slot,
            scheduleEnable: schedule.public
          }))
        );
        setMedicSlots(allSlotsWithSchedule);
      }
    } catch (error) {
      handleError(error, 'Error fetching medic details');
    }
  };

  const fetchUpdatedEvents = async () => {
    if (selectedMedic !== null) {
      await fetchMedic(selectedMedic);
    }
  };

  const handleMedicSelectionChange = (medicId: number) => {
    setSelectedMedic(medicId);
    fetchMedic(medicId);
  };

  const publicSchedule = async () => {
    try {
      const { data } = await getClient().mutate({
        mutation: PUBLIC_MEDIC_SCHEDULE_MUTATION,
      });

      console.log('Public schedule result:', data);

      // Actualizar horarios del médico después de publicarlos
      if (selectedMedic) {
        fetchMedic(selectedMedic);
      }
    } catch (error) {
      handleError(error, 'Error publicizing medic schedules');
    }
  };

  const handleCreateSchedule = async (
    input: { time: string; slotDuration: string; medicId: number; boxId: number }
  ) => {
    try {
      const { time, slotDuration, medicId, boxId } = input;
      const schedule = {
        time,
        slotDuration,
        medicId,
        boxId,
      };

      const query = `
        mutation CreateMedicSchedule($schedule: CreateScheduleDto!) {
          createMedicSchedule(input: $schedule) {
            id
          }
        }
      `;

      console.log('Query:', query);
      console.log('Variables:', { schedule });

      let response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          query: query,
          variables: { schedule },
        }),
      });

      const result = await response.json();
      console.log('Result:', result);

      if (response.ok && !result.errors) {
        console.log('Schedule created:', result);

        // Actualizar horarios del médico después de crear uno nuevo
        if (selectedMedic) {
          fetchMedic(selectedMedic);
        }
        Swal.fire("Éxito", "Horario creado correctamente.", "success");
      } else {
        console.error('Error creating medic schedules:', result);
        Swal.fire({
          icon: 'error',
          title: 'Error creating medic schedules',
          text: result.errors ? result.errors[0].message : 'An unexpected error occurred while creating medic schedules.',
        });
        throw new Error(result.errors ? result.errors[0].message : 'An unexpected error occurred while creating medic schedules.');
      }
    } catch (error) {
      handleError(error, 'Error creating medic schedules');
    }
  };

const generateCalendarEvents = useMemo(() => {
  if (!medicSchedules) return [];

  const events = medicSlots.map(({ slot, scheduleEnable }) => {
    if (!slot.id) {
      console.error('Slot ID is missing:', slot);
      return null;
    }

    // Verificar la cadena de tiempo
    console.log('Raw slot time:', slot.time);

    // Esperar el formato ["YYYY-MM-DD HH:mm:ss","YYYY-MM-DD HH:mm:ss"]
    const timeRange = JSON.parse(slot.time);
    if (!Array.isArray(timeRange) || timeRange.length < 2) {
      console.error('Invalid schedule time format:', slot.time);
      return null;
    }

    // Depurar el rango de tiempo
    console.log('Time range:', timeRange);

    let color;
    if (slot.blocked) {
      color = '#EED3D9'; // Color para bloqueado
    } else if (slot.enabled && !scheduleEnable) {
      color = '#CCD3CA'; // Color para reservado
    } else {
      color = scheduleEnable ? '#F5E8DD' : '#B5C0D0'; // Color para habilitado y no publicado
    }

    return {
      id: slot.id,
      start: moment(timeRange[0], 'YYYY-MM-DD HH:mm:ss').toDate(),
      end: moment(timeRange[1], 'YYYY-MM-DD HH:mm:ss').toDate(),
      title: 'Scheduled Time',
      color: color,
      slot: slot, // Incluye el Slot completo para acceder a su Schedule
    } as CalendarEventWithSlot;
  }).filter((event): event is CalendarEventWithSlot => event !== null); // Remove null events

  return events;
}, [medicSlots, medicSchedules]);

const generateCalendarBoxEvents = useMemo(() => {
  if (!boxSchedule) {
    console.log('Box Schedule:', boxSchedule);
    return [];
  }

  const events = boxSchedule.flatMap(schedule =>
    schedule.slots.map((slot) => {
      const timeRange = JSON.parse(slot.time);
      if (!Array.isArray(timeRange) || timeRange.length < 2) {
        console.error('Invalid schedule time format:', slot.time);
        return null;
      }
      console.log('Time range:', timeRange);
      return {
        start: moment(timeRange[0], 'YYYY-MM-DD HH:mm:ss').toDate(),
        end: moment(timeRange[1], 'YYYY-MM-DD HH:mm:ss').toDate(),
        title: 'Box Time',
        color: slot.enabled ? '#EED3D9' : '#B5C0D0',
      };
    })
  ).filter(event => event !== null); // Remove null events

  return events;
}, [boxSchedule]);


  const handleError = (error: unknown, title: string) => {
    let message = 'An unexpected error occurred';
    if (error instanceof Error) {
      message = error.message;
    } else if (typeof error === 'string') {
      message = error;
    }
    Swal.fire({
      icon: 'error',
      title: title,
      text: message,
    });
  };
 
  return {
    medics,
    selectedMedic,
    setSelectedMedic,
    generateCalendarEvents,
    handleCreateSchedule,
    activeDays,
    setActiveDays,
    selectedBox,
    setSelectedBox,
    boxes,
    generateCalendarBoxEvents,
    publicSchedule,
    fetchBox,
    fetchMedic,
    fetchUpdatedEvents,
  };
}
