import React from "react";
import "@fortawesome/fontawesome-free/css/all.min.css"; // Import Font Awesome

const TimePicker = ({
  label,
  value,
  onChange,
}: {
  label: string;
  value: string;
  onChange: (value: string) => void;
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onChange(event.target.value);
  };

  return (
    <div style={styles.container}>
      {label && (
        <label htmlFor="timeInput" style={styles.label}>
          {label}
        </label>
      )}
      <div style={styles.inputContainer}>
        <i className="fas fa-clock" style={styles.icon}></i>
        <input
          type="time"
          id="timeInput"
          value={value}
          onChange={handleChange}
          style={styles.input}
        />
      </div>
    </div>
  );
};

const styles = {
  container: {
    margin: "20px",
  },
  label: {
    display: "block",
    marginBottom: "5px",
    fontSize: "1.125rem", // Ajustado al tamaño de fuente 'lg' del tema
    color: "#333333", // Usando el color 'text' del tema
    fontFamily: "Inter, sans-serif", // Usando la fuente 'body' del tema
  } as React.CSSProperties,
  inputContainer: {
    position: "relative" as "relative",
    display: "flex" as "flex",
    alignItems: "center" as "center",
  } as React.CSSProperties,
  icon: {
    position: "absolute" as "absolute",
    left: "10px",
    color: "#3F8F8F", // Usando el color 'primary' del tema
    pointerEvents: "none" as "none",
  } as React.CSSProperties,
  input: {
    padding: "10px 10px 10px 35px", // Ajustado para dar espacio al icono
    border: "2px solid #3F8F8F", // Usando el color 'primary' del tema
    borderRadius: "0.375rem", // Usando el radio del borde 'lg' del tema
    backgroundColor: "#ffffff", // Usando el color 'card' del tema
    color: "#333333", // Usando el color 'text' del tema
    fontSize: "1.125rem", // Ajustado al tamaño de fuente 'lg' del tema
    outline: "none",
    boxShadow: "0 4px 6px -1px rgba(0,0,0,0.1)", // Usando la sombra 'md' del tema
    paddingLeft: "35px", // Asegura que el texto no se superponga al ícono
  } as React.CSSProperties,
};

export default TimePicker;
