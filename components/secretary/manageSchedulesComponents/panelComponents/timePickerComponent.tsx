"use client";
import React, { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { addDays, startOfWeek } from "date-fns";

const MyDatePickerComponent: React.FC<{
  onSelectWeek: (startOfWeekDate: Date) => void;
  onCalendarOpen: () => void;
  onCalendarClose: () => void;
}> = ({ onSelectWeek, onCalendarOpen, onCalendarClose }) => {
  const [selectedDate, setSelectedDate] = useState<Date | null>(null);

  const handleDateChange = (date: Date) => {
    setSelectedDate(date);
    const startOfWeekDate = startOfWeek(date, { weekStartsOn: 1 });
    onSelectWeek(startOfWeekDate);
  };

  return (
    <DatePicker
      selected={selectedDate}
      onChange={handleDateChange}
      dateFormat="yyyy-MM-dd"
      placeholderText="Selecciona una fecha"
      inline
      onCalendarOpen={onCalendarOpen}
      onCalendarClose={onCalendarClose}
    />
  );
};

export default MyDatePickerComponent;
