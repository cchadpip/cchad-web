"use client";
import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Divider,
  Button,
  Switch,
} from "@nextui-org/react";
import TimePicker from "./timeComponent";
import { format } from "date-fns";
import { es } from "date-fns/locale";

interface TimeSlot {
  startTime: string;
  endTime: string;
}

interface ActiveDays {
  [key: string]: TimeSlot[];
}

interface SchedulePanelProps {
  onPublish: () => void;
  onSaveDay: (name: string, date: Date) => void;
  onCancel: () => void;
  activeDays: ActiveDays;
  setActiveDays: React.Dispatch<React.SetStateAction<ActiveDays>>;
  blockDuration: string;
  setBlockDuration: React.Dispatch<React.SetStateAction<string>>;
  weekDays: { name: string; date: Date }[];
}

const daysOfWeekMap: { [key: string]: string } = {
  Monday: "Lunes",
  Tuesday: "Martes",
  Wednesday: "Miércoles",
  Thursday: "Jueves",
  Friday: "Viernes",
  Saturday: "Sábado",
  Sunday: "Domingo",
};

const SchedulePanel: React.FC<SchedulePanelProps> = ({
  onPublish,
  onSaveDay,
  onCancel,
  activeDays,
  setActiveDays,
  blockDuration,
  setBlockDuration,
  weekDays,
}) => {
  const addTimeSlot = (day: string) => {
    setActiveDays((prev) => ({
      ...prev,
      [day]: [...(prev[day] || []), { startTime: "08:00", endTime: "17:00" }],
    }));
  };

  const removeTimeSlot = (day: string) => {
    setActiveDays((prev) => ({
      ...prev,
      [day]: prev[day] ? prev[day].slice(0, -1) : [],
    }));
  };

  const handleSwitchChange =
    (day: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
      if (event.target.checked) {
        addTimeSlot(day);
      } else {
        setActiveDays((prev) => ({ ...prev, [day]: [] }));
      }
    };

  const updateTime = (
    day: string,
    index: number,
    field: string,
    newValue: string
  ) => {
    setActiveDays((prev) => ({
      ...prev,
      [day]: prev[day].map((time, i) =>
        i === index ? { ...time, [field]: newValue } : time
      ),
    }));
  };

  return (
    <Card radius="md" className="flex max-w-[600px] bg-white text-black">
      <CardHeader className="flex gap-3 justify-around items-center">
        <div className="flex">
          <p className="text-lg w-1/2">Horarios</p>
        </div>
        <div className="flex">
          <Button
            className="mr-3"
            color="primary"
            variant="bordered"
            onPress={onCancel}
          >
            Cancelar
          </Button>
          <Button onPress={onPublish} color="primary" variant="bordered">
            Publicar
          </Button>
        </div>
      </CardHeader>

      <Divider className="bg-black bg-opacity-10" />

      <CardBody className="flex flex-col space-y-2">
        <TimePicker
          label="Duración del Bloque"
          value={blockDuration}
          onChange={setBlockDuration}
        />
        {weekDays.map(({ name, date }) => (
          <div key={name} className="flex flex-col space-y-2">
            <div className="flex items-center justify-between">
              <Switch
                checked={activeDays[name] && activeDays[name].length > 0}
                onChange={handleSwitchChange(name)}
                color="secondary"
                aria-label={`Switch for ${name}`}
              >
                <p className="text-black">{`${daysOfWeekMap[name]} - ${format(
                  date,
                  "dd 'de' MMMM 'de' yyyy",
                  { locale: es }
                )}`}</p>
              </Switch>
            </div>
            {activeDays[name] &&
              activeDays[name].map((time, index) => (
                <div key={index} className="flex gap-4">
                  <TimePicker
                    label="Inicio"
                    value={time.startTime}
                    onChange={(newValue) =>
                      updateTime(name, index, "startTime", newValue)
                    }
                  />
                  <TimePicker
                    label="Fin"
                    value={time.endTime}
                    onChange={(newValue) =>
                      updateTime(name, index, "endTime", newValue)
                    }
                  />
                </div>
              ))}
            {activeDays[name] && activeDays[name].length > 0 && (
              <>
                <div className="flex justify-center space-x-2 mt-2">
                  <Button
                    onClick={() => addTimeSlot(name)}
                    color="warning"
                    variant="ghost"
                    radius="none"
                    className="h-7 w-25"
                  >
                    Agregar
                  </Button>
                  <Button
                    onClick={() => removeTimeSlot(name)}
                    isDisabled={activeDays[name].length === 1}
                    color="danger"
                    variant="ghost"
                    radius="none"
                    className="h-7"
                  >
                    Quitar
                  </Button>
                </div>
                <div className="flex justify-center">
                  <Button
                    onClick={() => onSaveDay(name, date)}
                    color="success"
                    variant="ghost"
                    radius="none"
                    className="h-7 w-25"
                  >
                    Guardar
                  </Button>
                </div>
              </>
            )}
          </div>
        ))}
      </CardBody>

      <Divider />
    </Card>
  );
};

export default SchedulePanel;
