"use client";
import { Calendar, momentLocalizer, SlotInfo } from "react-big-calendar";
import moment from "moment";
import "moment/locale/es";
import React, { SyntheticEvent, useState } from "react";
import NotPublishedBlockCalendarComponent from "./../blocksForCalendar/NotPublishedCalendarComponent";
import BlockedBlockCalendarComponent from "./../blocksForCalendar/BlockedCalendarComponent";
import PublishedBlockCalendarComponent from "./../blocksForCalendar/PublishedBlockCalendarComponent";
import { Slot, CalendarEvent } from "@/config/Interface/interface";
import {
  Button,
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  useDisclosure,
} from "@nextui-org/react";
import Swal from "sweetalert2";
import { getClient } from "@/config/graphql/apolloClient";
import { DELETE_MEDIC_SCHEDULE_MUTATION } from "@/config/graphql/mutations/serviceSecretary/serviceSchedule/service";

moment.locale("es");
const localizer = momentLocalizer(moment);

const messages = {
  previous: "<",
  next: ">",
  today: "Hoy",
  month: "Mes",
  week: "Semana",
  day: "Día",
  date: "Fecha",
  time: "Hora",
  event: "Evento",
  allDay: "Todo el día",
  work_week: "Semana de trabajo",
  yesterday: "Ayer",
  tomorrow: "Mañana",
  agenda: "Agenda",
  noEventsInRange: "No hay eventos en este rango",
  showMore: (total: number) => `+${total} más`,
};

const eventStyleGetter = (event: CalendarEvent) => {
  var style = {
    backgroundColor: event.color,
    borderRadius: "1px",
    opacity: 0.8,
    color: "black",
    border: "0px",
    display: "block",
  };
  return {
    style: style,
  };
};

interface SecretaryCalendarViewProps {
  events: CalendarEvent[];
  fetchMedic: (medicId: number) => Promise<void>;
}

// Definir las interfaces combinadas
export interface CustomSlotInfo extends SlotInfo {
  title?: string;
  action: "click";
}

export interface CalendarEventWithSlot extends CalendarEvent {
  slot: Slot;
}

const SecretaryCalendarView: React.FC<SecretaryCalendarViewProps> = ({
  events,
  fetchMedic,
}) => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [selectedSlot, setSelectedSlot] = useState<
    CustomSlotInfo | CalendarEventWithSlot | null
  >(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [selectedMedic, setSelectedMedic] = useState<number | null>(null); // Asegúrate de tener selectedMedic en el estado

  const handleSelectSlot = (slotInfo: SlotInfo) => {
    setSelectedSlot(slotInfo as CustomSlotInfo);
    onOpenChange();
  };

  const handleSelectEvent = (
    event: CalendarEvent,
    e: SyntheticEvent<HTMLElement, Event>
  ) => {
    if (event.slot) {
      const scheduleId = event.slot.schedule.id; // Accede al ID del Schedule desde el Slot
      console.log("Selected Schedule ID:", scheduleId);
      setSelectedSlot(event as CalendarEventWithSlot); // Maneja el slot seleccionado
      onOpenChange(); // Abre el modal o maneja la acción deseada
    }
  };

  const handleDeleteSchedule = async (
    id: number,
    setIsLoading: (loading: boolean) => void,
    fetchMedic: (medicId: number) => Promise<void>,
    selectedMedic: number | null
  ) => {
    setIsLoading(true);
    try {
      console.log("Attempting to delete schedule with id:", id);
      const client = getClient();
      const result = await client.mutate({
        mutation: DELETE_MEDIC_SCHEDULE_MUTATION,
        variables: { id: id },
      });
      if (result.errors) {
        console.error("GraphQL errors:", result.errors);
      } else {
        console.log("Schedule deleted successfully:", result.data);
        Swal.fire("Éxito", "Horario eliminado correctamente.", "success");
        if (selectedMedic !== null) {
          await fetchMedic(selectedMedic); // Actualiza los eventos después de eliminar el horario
        }
      }
    } catch (error: any) {
      if (error.networkError) {
        console.error("Network error:", error.networkError);
      } else if (error.graphQLErrors) {
        console.error("GraphQL errors:", error.graphQLErrors);
      } else {
        console.error("Unknown error:", error);
      }
    } finally {
      setIsLoading(false);
    }
  };

  const handleDeleteButtonClick = async () => {
    if (selectedSlot && (selectedSlot as CalendarEventWithSlot).slot) {
      const result = await Swal.fire({
        title: "¿Estás seguro?",
        text: "Esta acción eliminará el horario completo con todos sus slots y no se podrá revertir.",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Sí, eliminar",
        cancelButtonText: "Cancelar",
      });

      if (result.isConfirmed) {
        await handleDeleteSchedule(
          (selectedSlot as CalendarEventWithSlot).slot.schedule.id,
          setIsLoading,
          fetchMedic,
          selectedMedic
        );
        onOpenChange(); // Cierra el modal después de eliminar el horario
      }
    }
  };

  const components = {
    event: ({ event }: { event: CalendarEvent }) => {
      const type = event.color;
      if (type === "#B5C0D0")
        return <NotPublishedBlockCalendarComponent event={event} />;
      if (type === "#EED3D9")
        return (
          <BlockedBlockCalendarComponent
            event={event}
            fetchMedic={fetchMedic}
          />
        );
      if (type === "#F5E8DD")
        return (
          <PublishedBlockCalendarComponent
            event={event}
            fetchMedic={fetchMedic}
          />
        );
    },
  };

  const minTime = new Date();
  minTime.setHours(8, 0, 0);

  const maxTime = new Date();
  maxTime.setHours(21, 0, 0);

  return (
    <div>
      <Calendar
        localizer={localizer}
        events={events}
        defaultView="month"
        views={["month", "week", "day"]}
        messages={messages}
        style={{
          height: "860px",
          width: "800px",
          backgroundColor: "#F9F5F6",
          color: "black",
        }}
        min={minTime}
        max={maxTime}
        eventPropGetter={eventStyleGetter}
        components={components}
        selectable
        onSelectSlot={handleSelectSlot}
        onSelectEvent={handleSelectEvent} // Actualiza aquí
      />
      <Modal
        size="4xl"
        backdrop="blur"
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        isDismissable={false}
        isKeyboardDismissDisabled={true}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col gap-1">
                Detalles del Horario Seleccionado
              </ModalHeader>
              <ModalBody>
                {selectedSlot && (
                  <div>
                    {selectedSlot.title && <p>Título: {selectedSlot.title}</p>}
                    <p>Inicio: {selectedSlot.start.toString()}</p>
                    <p>Fin: {selectedSlot.end.toString()}</p>
                    {/* Añade más detalles según tus necesidades */}
                  </div>
                )}
              </ModalBody>
              <ModalFooter>
                <Button
                  color="warning"
                  onPress={handleDeleteButtonClick}
                  isLoading={isLoading}
                >
                  Eliminar
                </Button>
                <Button color="primary" onPress={onClose}>
                  Cancelar
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </div>
  );
};

export default SecretaryCalendarView;
