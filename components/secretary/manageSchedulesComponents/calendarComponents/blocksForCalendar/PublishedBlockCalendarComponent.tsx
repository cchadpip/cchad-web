"use client";
import {
  Button,
  Popover,
  PopoverContent,
  PopoverTrigger,
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  useDisclosure,
  CircularProgress,
} from "@nextui-org/react";
import { useState } from "react";
import { BLOCK_SLOT_MUTATION } from "@/config/graphql/mutations/serviceSecretary/serviceSchedule/service";
import { getClient } from "@/config/graphql/apolloClient";

interface PublishedBlockCalendarComponentProps {
  event: {
    id: number;
    title: string;
    start: Date;
    end: Date;
    [key: string]: any;
  };
  fetchMedic: (medicId: number) => Promise<void>;
}

const handleButtonBlock = async (
  id: number,
  setIsLoading: (loading: boolean) => void,
  fetchMedic: (medicId: number) => Promise<void>,
  selectedMedic: number | null
) => {
  setIsLoading(true);
  try {
    console.log("Attempting to block slot with id:", id);
    const client = getClient();
    const result = await client.mutate({
      mutation: BLOCK_SLOT_MUTATION,
      variables: { id: id },
    });
    if (result.errors) {
      console.error("GraphQL errors:", result.errors);
    } else {
      console.log("Slot blocked successfully:", result.data);
      if (selectedMedic !== null) {
        await fetchMedic(selectedMedic); // Actualiza los eventos después de bloquear el slot
      }
    }
  } catch (error: any) {
    if (error.networkError) {
      console.error("Network error:", error.networkError);
    } else if (error.graphQLErrors) {
      console.error("GraphQL errors:", error.graphQLErrors);
    } else {
      console.error("Unknown error:", error);
    }
  } finally {
    setIsLoading(false);
  }
};

const PublishedBlockCalendarComponent: React.FC<
  PublishedBlockCalendarComponentProps
> = ({ event, fetchMedic }) => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [selectedMedic, setSelectedMedic] = useState<number | null>(null);

  return (
    <>
      <Popover placement="bottom" showArrow={true}>
        <PopoverTrigger>
          <Button
            color="warning"
            size="sm"
            variant="light"
            onPress={onOpenChange}
          >
            Disponible
          </Button>
        </PopoverTrigger>
        <PopoverContent>
          <Button
            isDisabled={isLoading} // Desactiva el botón mientras se está cargando
            onPress={() =>
              handleButtonBlock(
                event.id,
                setIsLoading,
                fetchMedic,
                selectedMedic
              )
            }
            color="danger"
          >
            {isLoading ? (
              <CircularProgress size="sm" color="danger" isIndeterminate /> // Muestra el indicador de carga circular
            ) : (
              "Bloquear"
            )}
          </Button>
        </PopoverContent>
      </Popover>
      <Modal
        size="4xl"
        backdrop="blur"
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        isDismissable={false}
        isKeyboardDismissDisabled={true}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col gap-1">
                Detalles del Horario Bloqueado
              </ModalHeader>
              <ModalBody>
                {event && (
                  <div>
                    <p>Título: {event.title}</p>
                    <p>Inicio: {event.start.toString()}</p>
                    <p>Fin: {event.end.toString()}</p>
                    {/* Añade más detalles según tus necesidades */}
                  </div>
                )}
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onPress={onClose}>
                  Ok
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </>
  );
};

export default PublishedBlockCalendarComponent;
