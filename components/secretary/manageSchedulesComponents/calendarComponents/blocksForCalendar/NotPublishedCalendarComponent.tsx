import React, { useState } from "react";
import { Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, Button, useDisclosure } from "@nextui-org/react";

const NotPublishedBlockCalendarComponent = ({ event }: any) => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure();

  return (
		<>
		<div className="text-xs" onClick={onOpen}>
			No publicado
		</div>
		<Modal
			size="4xl"
			backdrop="blur"
			isOpen={isOpen}
			onOpenChange={onOpenChange}
			isDismissable={false}
			isKeyboardDismissDisabled={true}
		>
			<ModalContent>
			{(onClose) => (
				<>
				<ModalHeader className="flex flex-col gap-1">Detalles del Horario</ModalHeader>
				<ModalBody>
					{event && (
					<div>
						<p>Título: {event.title}</p>
						<p>Inicio: {event.start.toString()}</p>
						<p>Fin: {event.end.toString()}</p>
						{/* Añade más detalles según tus necesidades */}
					</div>
					)}
				</ModalBody>
				<ModalFooter>
					<Button color="primary" onPress={onClose}>
					Ok
					</Button>
				</ModalFooter>
				</>
			)}
			</ModalContent>
		</Modal>
		</>
	);
};

export default NotPublishedBlockCalendarComponent;
