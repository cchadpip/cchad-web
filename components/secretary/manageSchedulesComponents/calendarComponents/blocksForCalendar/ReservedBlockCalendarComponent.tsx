"use client";
import {
  Popover,
  PopoverTrigger,
  Button,
  PopoverContent,
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  useDisclosure,
  CircularProgress,
} from "@nextui-org/react";
import { useState } from "react";
import { BLOCK_SLOT_MUTATION } from "@/config/graphql/mutations/serviceSecretary/serviceSchedule/service";
import { getClient } from "@/config/graphql/apolloClient";

interface ReservedBlockCalendarComponentProps {
  appointments: {
    id: number;
    state: string;
    confirmed: boolean;
    type: string;
    patient: {
      id: number;
      name: string;
      lastName: string;
    };
  }[];
  fetchMedic: (medicId: number) => Promise<void>;
}

const handleButtonBlock = async (
  id: number,
  setIsLoading: (loading: boolean) => void,
  fetchMedic: (medicId: number) => Promise<void>,
  selectedMedic: number | null
) => {
  setIsLoading(true);
  try {
    console.log("Attempting to block slot with id:", id);
    const client = getClient();
    const result = await client.mutate({
      mutation: BLOCK_SLOT_MUTATION,
      variables: { id: id },
    });
    if (result.errors) {
      console.error("GraphQL errors:", result.errors);
    } else {
      console.log("Slot blocked successfully:", result.data);
      if (selectedMedic !== null) {
        await fetchMedic(selectedMedic); // Actualiza los eventos después de bloquear el slot
      }
    }
  } catch (error: any) {
    if (error.networkError) {
      console.error("Network error:", error.networkError);
    } else if (error.graphQLErrors) {
      console.error("GraphQL errors:", error.graphQLErrors);
    } else {
      console.error("Unknown error:", error);
    }
  } finally {
    setIsLoading(false);
  }
};

const ReservedBlockCalendarComponent: React.FC<
  ReservedBlockCalendarComponentProps
> = ({ appointments, fetchMedic }) => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [selectedMedic, setSelectedMedic] = useState<number | null>(null);

  return (
    <>
      <div className="flex flex-row">
        <Popover placement="bottom" showArrow={true}>
          <PopoverTrigger>
            <Button color="default" size="sm" variant="faded">
              {appointments[0].patient.name} {appointments[0].patient.lastName}
            </Button>
          </PopoverTrigger>
          <PopoverContent>
            <div className="px-1 py-2">
              <div className="text-small font-bold">
                {appointments[0].patient.name}{" "}
                {appointments[0].patient.lastName}
              </div>
              <div className="text-tiny">{appointments[0].type}</div>
            </div>
          </PopoverContent>
        </Popover>
        <Popover placement="bottom" showArrow={true}>
          <PopoverTrigger>
            <Button
              color="default"
              size="sm"
              variant="faded"
              isIconOnly
            ></Button>
          </PopoverTrigger>
          <PopoverContent>
            <Button
              isDisabled={isLoading} // Desactiva el botón mientras se está cargando
              onPress={() =>
                handleButtonBlock(
                  appointments[0].id,
                  setIsLoading,
                  fetchMedic,
                  selectedMedic
                )
              }
              color="danger"
            >
              {isLoading ? (
                <CircularProgress size="sm" color="danger" isIndeterminate /> // Muestra el indicador de carga circular
              ) : (
                "Bloquear"
              )}
            </Button>
          </PopoverContent>
        </Popover>
      </div>
      <Modal
        size="4xl"
        backdrop="blur"
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        isDismissable={false}
        isKeyboardDismissDisabled={true}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col gap-1">
                Detalles del Horario Bloqueado
              </ModalHeader>
              <ModalBody>
                {appointments[0] && (
                  <div>
                    <p>
                      Paciente: {appointments[0].patient.name}{" "}
                      {appointments[0].patient.lastName}
                    </p>
                    <p>Tipo de cita: {appointments[0].type}</p>
                    {/* Añade más detalles según tus necesidades */}
                  </div>
                )}
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onPress={onClose}>
                  Ok
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </>
  );
};

export default ReservedBlockCalendarComponent;
