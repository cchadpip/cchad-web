"use client";
import {
  Popover,
  PopoverTrigger,
  Button,
  PopoverContent,
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  useDisclosure,
} from "@nextui-org/react";
import { useState } from "react";
import { UNLOCK_SLOT_MUTATION } from "@/config/graphql/mutations/serviceSecretary/serviceSchedule/service";
import { getClient } from "@/config/graphql/apolloClient";

interface PublishedBlockCalendarComponentProps {
  event: {
    id: number;
    title: string;
    start: Date;
    end: Date;
    [key: string]: any;
  };
  fetchMedic: (medicId: number) => Promise<void>;
}
const BlockedBlockCalendarComponent: React.FC<
  PublishedBlockCalendarComponentProps
> = ({ event, fetchMedic }) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [selectedMedic, setSelectedMedic] = useState<number | null>(null);

  const handleButtonBlock = async (
    id: number,
    setIsLoading: (loading: boolean) => void,
    fetchMedic: (medicId: number) => Promise<void>,
    selectedMedic: number | null
  ) => {
    setIsLoading(true);
    try {
      console.log("Attempting to block slot with id:", id);
      const client = getClient();
      const result = await client.mutate({
        mutation: UNLOCK_SLOT_MUTATION,
        variables: { id: id },
      });
      if (result.errors) {
        console.error("GraphQL errors:", result.errors);
      } else {
        console.log("Slot blocked successfully:", result.data);
        if (selectedMedic !== null) {
          await fetchMedic(selectedMedic); // Actualiza los eventos después de bloquear el slot
        }
      }
    } catch (error: any) {
      if (error.networkError) {
        console.error("Network error:", error.networkError);
      } else if (error.graphQLErrors) {
        console.error("GraphQL errors:", error.graphQLErrors);
      } else {
        console.error("Unknown error:", error);
      }
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      <Popover placement="bottom" showArrow={true}>
        <PopoverTrigger>
          <Button color="danger" size="sm" variant="light" onPress={onOpen}>
            Bloqueado
          </Button>
        </PopoverTrigger>
        <PopoverContent>
          <Button
            isLoading={isLoading}
            onPress={() =>
              handleButtonBlock(
                event.id,
                setIsLoading,
                fetchMedic,
                selectedMedic
              )
            }
          >
            Desbloquear
          </Button>
        </PopoverContent>
      </Popover>
      <Modal
        size="4xl"
        backdrop="blur"
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        isDismissable={false}
        isKeyboardDismissDisabled={true}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col gap-1">
                Detalles del Horario Bloqueado
              </ModalHeader>
              <ModalBody>
                {event && (
                  <div>
                    <p>Título: {event.title}</p>
                    <p>Inicio: {event.start.toString()}</p>
                    <p>Fin: {event.end.toString()}</p>
                    {/* Añade más detalles según tus necesidades */}
                  </div>
                )}
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onPress={onClose}>
                  Ok
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </>
  );
};

export default BlockedBlockCalendarComponent;
