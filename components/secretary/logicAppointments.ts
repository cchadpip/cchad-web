"use client";
import { getClient } from "@/config/graphql/apolloClient";
import { GET_MEDICS_FOR_SECRETARY, GET_MEDIC_FOR_SECRETARY, BOOK_APPOINTMENT } from "@/config/graphql/mutations/serviceSecretary/serviceAppointment/service";
import { Medic, Schedule, User } from "@/config/Interface/interface";
import { useCallback, useEffect, useState } from "react";
import { parseISO, format } from 'date-fns';
import Swal from "sweetalert2";
import useUserStore from "@/config/storage/user";
import { GET_PATIENTS } from "@/config/graphql/mutations/servicePatient/service";
export default function useAppointmentLogic() {
    const [users, setUsers] = useState<User[]>([]);
    const [medics, setMedics] = useState<Medic[]>([]);
    const [medic, setMedic] = useState<Medic | null>(null);
    const [medicSchedules, setMedicSchedules] = useState<Schedule[]>([]);
    const [selectedSlot, setSelectedSlot] = useState<number | null>(null);
    
    const fetchMedics = async () => {
        try {
            const { data } = await getClient().query({ query: GET_MEDICS_FOR_SECRETARY });
            setMedics(data.medics);
        } catch (error) {
            handleError(error, 'Error fetching medics');
        }
    };

    const fetchMedic = useCallback(async (medicId: number, dateAppoint: string) => {
        try {
            const { data } = await getClient().query<{ medic: Medic }>({
                query: GET_MEDIC_FOR_SECRETARY,
                variables: { id: medicId },
            });
            setMedic(data.medic);
            if (data.medic && data.medic.schedules) {
                const filteredSchedules = data.medic.schedules.filter((schedule) => {
                    const validJSON = schedule.time.replace(/[\[\]\(\)]/g, '');
                    const times = validJSON.split('","').map(time => time.replace(/^"|"$/g, ''));
                    const scheduleStartDate = format(parseISO(times[0]), 'yyyy-MM-dd');
                    return scheduleStartDate === dateAppoint;
                });
                setMedicSchedules(filteredSchedules);
            }
        } catch (error) {
            handleError(error, 'Error fetching medic details');
        }
    }, []);

    const createAppointment = async ({ type, patientId, slotId }: { type: string; patientId: number; slotId: number; }) => {
        try {
            console.log("Datos enviados a la mutación:", { type, patientId, slotId });

            const { data } = await getClient().mutate({
                mutation: BOOK_APPOINTMENT,
                variables: {
                    input: {
                        type,
                        patientId,
                        slotId,
                    },
                },
            });
            Swal.fire({
                icon: 'success',
                title: 'Appointment booked',
                text: `Your appointment has been booked successfully with ID ${data.bookAppointment.id}.`,
            });
        } catch (error: any) {
            Swal.fire({
                icon: 'error',
                title: 'Error booking appointment',
                text: `There was an error booking your appointment: ${error.message}. Please try again.`,
            });
        }
    };

    const handleUsers = async () => {
        try{
            const { data } = await getClient().query({
                query: GET_PATIENTS,
            });
            setUsers(data.patients);
        }
        catch (error) {
            handleError(error, 'Error fetching patients');
        }
    }

    const handleError = (error: unknown, title: string) => {
        let message = 'An unexpected error occurred';
        if (error instanceof Error) {
            message = error.message;
        } else if (typeof error === 'string') {
            message = error;
        }
        Swal.fire({
            icon: 'error',
            title: title,
            text: message,
        });
    };

    useEffect(() => {
        fetchMedics();
        handleUsers();
    }, []);

    return {
        medics,
        users,
        fetchMedics,
        fetchMedic,
        medicSchedules,
        selectedSlot,
        setSelectedSlot,
        createAppointment,
        
    };
}
