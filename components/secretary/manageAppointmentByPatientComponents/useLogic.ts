import { Patient } from "@/config/Interface/interface";
import { getClient } from "@/config/graphql/apolloClient";
import { GET_PATIENT } from "@/config/graphql/mutations/servicePatient/service";
import { GET_PATIENTS } from "@/config/graphql/mutations/serviceSecretary/serviceAppointment/service";
import { useState, useEffect } from "react";

export function useLogic() {
  const [patients, setPatients] = useState<Patient[]>([]);
  const [patient, setPatient] = useState<Patient | null>(null);

  useEffect(() => {
    fetchPatients();
  }, []);

  const fetchPatients = async () => {
    try {
      const { data } = await getClient().query({ query: GET_PATIENTS });
      setPatients(data.patients);
    } catch (error) {
      console.error("Error fetching patients:", error);
    }
  };

  const fetchPatient = async (patientId: number) => {
    try {
      const { data } = await getClient().query({ query: GET_PATIENT, variables: { id: patientId } });
      setPatient(data.patient);
    } catch (error) {
      console.error("Error fetching patient:", error);
    }
  };

  return {
    patients,
    fetchPatients,
    fetchPatient,
    patient,
  };
}
