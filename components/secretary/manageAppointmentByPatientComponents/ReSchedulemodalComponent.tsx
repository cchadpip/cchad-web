"use client";
import React, { useState, useEffect } from "react";
import useSecretaryAppointmentLogic from "@/components/appointmentComponents/logicAppointmentSecretary/useLogicAppointmentSecretary";
import Swal from "sweetalert2";
import {
  Card,
  Button,
  Input,
  Spacer,
  CardHeader,
  CardBody,
} from "@nextui-org/react";

const AppointmentCalendar = ({
  selectedMedicId,
  appointmentId,
  onUpdateAppointments,
}) => {
  const {
    fetchMedic,
    medicSchedules,
    selectedSlot,
    setSelectedSlot,
    rescheduleAppointment,
    availableSlots,
    filterSlotsByDate,
  } = useSecretaryAppointmentLogic();

  const [selectedDate, setSelectedDate] = useState<string>("");
  const [isLoadingSearch, setIsLoadingSearch] = useState<boolean>(false);
  const [isLoadingReschedule, setIsLoadingReschedule] =
    useState<boolean>(false);

  useEffect(() => {
    if (selectedMedicId && selectedDate) {
      handleSearch();
    }
  }, [selectedMedicId, selectedDate]);

  const handleSearch = async () => {
    console.log(" medicos: ", selectedMedicId, selectedDate);
    if (selectedMedicId && selectedDate) {
      setIsLoadingSearch(true);
      try {
        await fetchMedic(Number(selectedMedicId));
        filterSlotsByDate(selectedDate);
      } catch (error) {
        handleError(error, "Error al buscar horarios");
      } finally {
        setIsLoadingSearch(false);
      }
    } else {
      Swal.fire({
        icon: "warning",
        title: "Campos incompletos",
        text: "Seleccione una fecha para buscar horarios.",
      });
    }
  };

  const handleDateChange = (e) => {
    setSelectedDate(e.target.value);
  };

  const handleSlotSelect = (slotId) => {
    setSelectedSlot(slotId);
  };

  const handleRescheduleAppointment = async () => {
    if (selectedSlot && appointmentId) {
      setIsLoadingReschedule(true);
      try {
        await rescheduleAppointment(appointmentId, selectedSlot);
        Swal.fire({
          icon: "success",
          title: "Cita reprogramada",
          text: "La cita ha sido reprogramada exitosamente.",
        });
        onUpdateAppointments(); // Llama a la función para actualizar el listado de citas
      } catch (error) {
        handleError(error, "Error al reprogramar cita");
      } finally {
        setIsLoadingReschedule(false);
      }
    } else {
      Swal.fire({
        icon: "warning",
        title: "Campos incompletos",
        text: "Seleccione un horario disponible.",
      });
    }
  };

  const formatTime = (time) => {
    try {
      const date = new Date(time);
      if (isNaN(date.getTime())) {
        return "Fecha Inválida";
      }
      date.setHours(date.getHours() - 4);
      const hours = date.getUTCHours().toString().padStart(2, "0");
      const minutes = date.getUTCMinutes().toString().padStart(2, "0");
      return `${hours}:${minutes}`;
    } catch (error) {
      return "Fecha Inválida";
    }
  };

  const sortAndFilterSlots = (slots) => {
    return [...slots]
      .filter(
        (slot) =>
          slot.enabled &&
          !slot.blocked &&
          (!slot.appointments || slot.appointments.length === 0)
      )
      .sort((a, b) => {
        const timeA = new Date(JSON.parse(a.time)[0]);
        const timeB = new Date(JSON.parse(b.time)[0]);
        timeA.setHours(timeA.getHours() - 4);
        timeB.setHours(timeB.getHours() - 4);
        return timeA.getTime() - timeB.getTime();
      });
  };

  const handleError = (error, title) => {
    let message = "An unexpected error occurred";
    if (error instanceof Error) {
      message = error.message;
    } else if (typeof error === "string") {
      message = error;
    }
    Swal.fire({
      icon: "error",
      title: title,
      text: message,
    });
  };

  return (
    <Card className="max-w-4xl mx-auto mt-8 shadow-md bg-background">
      <CardHeader>
        <h2 className="text-2xl font-bold text-black">Reagendar Cita</h2>
      </CardHeader>
      <CardBody>
        <p className="text-base text-black">
          Seleccione una fecha para ver los horarios disponibles.
        </p>
        <Spacer y={0.5} />
        <Input
          type="date"
          value={selectedDate ?? ""}
          onChange={handleDateChange}
          fullWidth
          className="mt-1 p-2 border rounded"
        />
        <Button
          onClick={handleSearch}
          disabled={!selectedDate || isLoadingSearch}
          color="primary"
          className="w-full mt-4 bg-primary text-white p-2 rounded"
          isLoading={isLoadingSearch}
        >
          {isLoadingSearch ? "Buscando..." : "Buscar"}
        </Button>
        <Spacer y={1} />
        <h3 className="text-xl font-semibold text-black">
          Horarios Disponibles
        </h3>
        <p className="text-base text-black">
          Seleccione un horario disponible para reagendar su cita.
        </p>
        <Spacer y={0.5} />
        <div className="flex flex-wrap gap-2">
          {sortAndFilterSlots(availableSlots).map((slot) => {
            const startTime = JSON.parse(slot.time)[0];
            const formattedTime = formatTime(startTime);
            return (
              <Button
                key={slot.id}
                onClick={() => handleSlotSelect(slot.id)}
                disabled={!slot.enabled || slot.blocked}
                color={selectedSlot === slot.id ? "success" : "primary"}
                className={`p-2 border rounded ${
                  selectedSlot === slot.id
                    ? "bg-success text-white"
                    : slot.enabled
                    ? "bg-primary text-white"
                    : "bg-gray-300 text-gray-500"
                }`}
              >
                {formattedTime}
              </Button>
            );
          })}
        </div>
        <Spacer y={1} />
        <Button
          onClick={handleRescheduleAppointment}
          disabled={!selectedSlot || isLoadingReschedule}
          color="success"
          className="w-full mt-4 bg-success text-white p-2 rounded"
          isLoading={isLoadingReschedule}
        >
          {isLoadingReschedule ? "Reprogramando..." : "Reprogramar Cita"}
        </Button>
      </CardBody>
    </Card>
  );
};

export default AppointmentCalendar;
