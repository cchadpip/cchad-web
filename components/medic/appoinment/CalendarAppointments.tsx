import React, { useState } from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import 'moment/locale/es';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, Button, useDisclosure } from '@nextui-org/react';
import { TodayAppointmentsProps } from '@/config/Interface/medic';

const localizer = momentLocalizer(moment);
const messages = {
  previous: '<',
  next: '>',
  today: 'Hoy',
  month: 'Mes',
  week: 'Semana',
  day: 'Día',
  date: 'Fecha',
  time: 'Hora',
  event: 'Evento',
  allDay: 'Todo el día',
  work_week: 'Semana de trabajo',
  yesterday: 'Ayer',
  tomorrow: 'Mañana',
  agenda: 'Agenda',
  noEventsInRange: 'No hay eventos en este rango',
  showMore: (total: number) => `+${total} más`,
};

interface CalendarEvent {
  id: number;
  title: string;
  start: Date;
  end: Date;
  backgroundColor: string;
  patient: {
    name: string;
    lastName: string;
  };
  boxName: string;
  appointmentId: number;
  state: string;
}

const CalendarAppointments = ({ appoinmentData }: TodayAppointmentsProps) => {
  console.log("Calendar", appoinmentData);
  const [selectedEvent, setSelectedEvent] = useState<CalendarEvent | null>(null);
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const minTime = new Date();
  minTime.setHours(8, 0, 0);
  const maxTime = new Date();
  maxTime.setHours(21, 0, 0);

  const getColorByState = (state: string) => {
    switch (state) {
      case 'Reservada':
        return '#60a5fa';
      case 'Reprogramada':
        return '#ffcc00';
      case 'Completada':
        return '#af7ac5';
      default:
        return '#60a5fa';
    }
  };

  const events: CalendarEvent[] = [];

  appoinmentData?.schedules.forEach((schedule) => {
    schedule.slots.forEach((slot) => {
      const slotTimes = JSON.parse(slot.time);
      const startSlot = moment(slotTimes[0]).toDate();
      const endSlot = moment(slotTimes[1]).toDate();

      slot.appointments.forEach((appointment) => {
        events.push({
          id: appointment.id,
          title: `${appointment.patient.name} ${appointment.patient.lastName}`,
          start: startSlot,
          end: endSlot,
          backgroundColor: getColorByState(appointment.state),
          patient: appointment.patient,
          boxName: schedule.box.name,
          appointmentId: appointment.id,
          state: appointment.state,
        });
      });
    });
  });

  const eventPropGetter = (event: CalendarEvent) => {
    return {
      style: { backgroundColor: event.backgroundColor },
    };
  };

  const handleEventClick = (event: CalendarEvent) => {
    setSelectedEvent(event);
    onOpen();
  };

  return (
    <div style={{ height: 500, backgroundColor: 'white', color: 'black' }}>
      <Calendar
        localizer={localizer}
        events={events}
        defaultView="month"
        views={['month', 'week', 'day']}
        messages={messages}
        min={minTime}
        max={maxTime}
        eventPropGetter={eventPropGetter}
        onSelectEvent={handleEventClick}
      />
      {selectedEvent && (
        <Modal isOpen={isOpen} onOpenChange={onOpenChange}>
          <ModalContent>
            {(onClose) => (
              <>
                <ModalHeader className="flex flex-col gap-1">Detalles de la Cita</ModalHeader>
                <ModalBody>
                  <p><strong>Paciente:</strong> {selectedEvent.patient.name} {selectedEvent.patient.lastName}</p>
                  <p><strong>Hora:</strong> {moment(selectedEvent.start).format('YYYY-MM-DD HH:mm')} - {moment(selectedEvent.end).format('HH:mm')}</p>
                  <p><strong>Box:</strong> {selectedEvent.boxName}</p>
                  <p><strong>Estado:</strong> {selectedEvent.state}</p>
                </ModalBody>
                <ModalFooter>
                  <Button color="danger" variant="light" onPress={onClose}>
                    Cerrar
                  </Button>
                </ModalFooter>
              </>
            )}
          </ModalContent>
        </Modal>
      )}
    </div>
  );
};

export default CalendarAppointments;
