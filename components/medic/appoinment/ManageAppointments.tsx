import { useState, useEffect, ChangeEventHandler } from "react";
import {
  Input,
  Table,
  Calendar,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  Button,
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  useDisclosure,
  Tooltip,
  Radio,
  RadioGroup,
  input,
} from "@nextui-org/react";
import { FaSort, FaSortUp, FaSortDown, FaTimes, FaEdit } from "react-icons/fa";
import { Appointment, TodayAppointmentsProps } from "@/config/Interface/medic";
import { today, getLocalTimeZone } from "@internationalized/date";
import { Slot, Schedule, AppoinmentData } from "@/config/Interface/medics";
import { CalendarDate } from "@internationalized/date";
import { id } from "date-fns/locale";
import { getClient } from "@/config/graphql/apolloClient";
import {
  CANCEL_APPOINTMENT,
  RESCHEDULE_APPOINTMENT,
} from "@/config/graphql/mutations/serviceMedic/service";
import SweetAlert from "sweetalert2";
import { parse } from 'date-fns';

import { hasDirectives } from "@apollo/client/utilities";
const ManageAppointments = ({ appoinmentData }: TodayAppointmentsProps) => {
  console.log("manage", appoinmentData);

  const [selectedDate, setSelectedDate] = useState<Date | null>(null);
  const [availableSlots, setAvailableSlots] = useState<
    { time: string; box: string; id: number }[]
  >([]);
  const [selectedSlot, setSelectedSlot] = useState<number | null>(null);
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [appointments, setAppointments] = useState<Appointment[]>([]);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [sortConfig, setSortConfig] = useState<{
    key: string;
    direction: string;
  } | null>(null);
  const [isSaveDisabled, setIsSaveDisabled] = useState<boolean>(true); // Estado para habilitar/deshabilitar botón Guardar
  const [slot, setSlot] = useState<number>(0);
  const [appointmentId, setAppointment] = useState<number>(0);
  useEffect(() => {
    if (appoinmentData) {
      const allAppointments = appoinmentData.schedules.flatMap((schedule) =>
        schedule.slots.flatMap((slot) =>
          slot.appointments.map((appointment) => ({
            id: appointment.id,
            name: `${appointment.patient.name} ${appointment.patient.lastName}`,
            time: slot.time,
            type: appointment.type,
            state: appointment.state,
            gender: appointment.patient.gender,
            rut: appointment.patient.rut,
            birthdate: appointment.patient.birthdate,
          }))
        )
      );
      setAppointments(allAppointments);
    }
  }, [appoinmentData]);

  const formatDate = (dateString: string) => {
    const options: Intl.DateTimeFormatOptions = {
      year: "numeric",
      month: "long",
      day: "numeric",
    };
    return new Date(dateString).toLocaleDateString(undefined, options);
  };

  const formatTime = (timeString: string) => {
    const timeArray = JSON.parse(timeString);
    return `${new Date(timeArray[0]).toLocaleTimeString()} - ${new Date(
      timeArray[1]
    ).toLocaleTimeString()}`;
  };

  const handleSort = (key: string) => {
    let direction = "ascending";
    if (
      sortConfig &&
      sortConfig.key === key &&
      sortConfig.direction === "ascending"
    ) {
      direction = "descending";
    }
    setSortConfig({ key, direction });
  };

  const sortedAppointments = [...appointments].sort((a, b) => {
    if (sortConfig !== null) {
      const { key, direction } = sortConfig;
      if (key === "date") {
        const dateA = new Date(JSON.parse(a.time)[0]).getTime();
        const dateB = new Date(JSON.parse(b.time)[0]).getTime();
        return direction === "ascending" ? dateA - dateB : dateB - dateA;
      }
      if (key === "patient") {
        const nameA = a.name.toLowerCase();
        const nameB = b.name.toLowerCase();
        if (nameA < nameB) return direction === "ascending" ? -1 : 1;
        if (nameA > nameB) return direction === "ascending" ? 1 : -1;
        return 0;
      }
    }
    return 0;
  });

  const filteredAppointments = sortedAppointments.filter(appointment => {
    const [startTime, endTime] = JSON.parse(appointment.time); // Convert the string into an array
    const now = new Date();
  
    // Convertir las cadenas de fecha a objetos Date
    const appointmentStartTime = new Date(startTime);
    const appointmentEndTime = new Date(endTime);
  
    // Filtrar por estado 'Reservada' y que la fecha de la cita sea después de la fecha actual
    return appointment.name.toLowerCase().includes(searchTerm.toLowerCase()) &&
           appointment.state === 'Reservada' &&
           appointmentStartTime >= now;
  });
  
  
  
  
  console.log("filtered", filteredAppointments);
  const getAvailableSlots = (schedules: Schedule[]) => {
    const availableSlots: { time: string; box: string; id: number }[] = [];
    schedules.forEach((schedule) => {
      schedule.slots.forEach((slot) => {
        if (slot.appointments.length === 0 && !slot.blocked && slot.enabled) {
          availableSlots.push({
            id: slot.id,
            time: slot.time,
            box: schedule.box.name,
          });
        }
      });
    });
    return availableSlots;
  };

  const getAvailableSlotsForDate = (schedules: Schedule[], date: string) => {
    const slotsForDate: { time: string; box: string; id: number }[] = [];
    schedules.forEach((schedule) => {
      schedule.slots.forEach((slot) => {
        const slotDate = JSON.parse(slot.time)[0].split(" ")[0];
        if (
          slotDate === date &&
          slot.appointments.length === 0 &&
          !slot.blocked &&
          slot.enabled
        ) {
          console.log("slot", slot);
          slotsForDate.push({
            id: slot.id,
            time: slot.time,
            box: schedule.box.name,
          });
        }
      });
    });
    return slotsForDate;
  };

  const handleDateChange = (date: CalendarDate) => {
    const dateStr = date.toString(); // Convertir fecha a string
    setSelectedDate(new Date(date.toString()));
    const slotsForDate = appoinmentData?.schedules
      ? getAvailableSlotsForDate(appoinmentData.schedules, dateStr)
      : [];
    setAvailableSlots(slotsForDate);
    setSelectedSlot(null); // Reiniciar la selección de slot cuando se cambia la fecha
    setIsSaveDisabled(true); // Deshabilitar el botón Guardar cuando no hay slot seleccionado
  };

  const handleSlotChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    const value = event.target.value;
    const slotId = parseInt(value); // Convertir el valor del slot a número
    setSelectedSlot(slotId);
    setIsSaveDisabled(false); // Habilitar el botón Guardar cuando se selecciona un slot
  };

  const getSortIcon = (key: string) => {
    if (!sortConfig || sortConfig.key !== key) {
      return <FaSort />;
    }
    if (sortConfig.direction === "ascending") {
      return <FaSortUp />;
    }
    return <FaSortDown />;
  };

  const handleReprogram = async () => {
    console.log(
      `Reprogramar cita con ID: ${appointmentId} usando Slot ID: ${slot}`
    );
    try {
      const { data } = await getClient().mutate({
        mutation: RESCHEDULE_APPOINTMENT,
        variables: {
          input: {
            appointmentId: appointmentId,
            newSlotId: slot,
          },
        },
      });
      if (data) {
        SweetAlert.fire({
          title: "Cita reprogramada",
          icon: "success",
          confirmButtonText: "Aceptar",
        });
      }
    } catch (e) {
      console.log(e);
    }
  };

  const handleCancel = async () => {
    console.log(`Cancelar cita con ID: ${appointmentId}`);
    SweetAlert.fire({
      title: "¿Estás seguro de cancelar la cita?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Sí",
      cancelButtonText: "No",
    }).then(async (result) => {
      if (result.isConfirmed) {
        setAppointment(0);
        try {
          const { data } = await getClient().mutate({
            mutation: CANCEL_APPOINTMENT,
            variables: {
              id: appointmentId,
            },
          });
          if (data) {
            SweetAlert.fire({
              title: "Cita cancelada",
              icon: "success",
              confirmButtonText: "Aceptar",
            });
          }
        } catch (e) {
          console.log(e);
          SweetAlert.fire({
            title:
              "No se puede eliminar el appointment con menos de 24 horas de anticipación antes del comienzo de su slot",
            icon: "error",
            confirmButtonText: "Aceptar",
          });
        }
      }
    });
  };

  return (
    <div className="bg-white p-4 rounded-lg">
      <Input
        isClearable
        className="w-full sm:max-w-[44%] mb-2"
        placeholder="Buscar por nombre de paciente"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
        variant="bordered"

      />
      <Table 
        aria-label="Historial de citas"
        className="bg-white text-gray-800 border border-gray-200"
        bgcolor="white"
        classNames={{
          table: "bg-white border border-gray-200",
          th: "bg-gray-200 text-gray-800 border-b border-gray-200",
          td: "bg-white text-gray-600 border-t border-gray-200",
        }}
      >
        <TableHeader>
          <TableColumn onClick={() => handleSort("date")}>
            <div className="flex items-center">Fecha {getSortIcon("date")}</div>
          </TableColumn>
          <TableColumn>Hora</TableColumn>
          <TableColumn onClick={() => handleSort("patient")}>
            <div className="flex items-center">
              Paciente {getSortIcon("patient")}
            </div>
          </TableColumn>
          <TableColumn>Estado</TableColumn>
          <TableColumn>Acciones</TableColumn>
        </TableHeader>
        <TableBody emptyContent={"No hay citas para mostrar."}>
          {filteredAppointments.map((appointment, index) => (
            <TableRow key={index}>
              <TableCell>
                {formatDate(JSON.parse(appointment.time)[0])}
              </TableCell>
              <TableCell>{formatTime(appointment.time)}</TableCell>
              <TableCell>{appointment.name}</TableCell>
              <TableCell>{appointment.state}</TableCell>
              <TableCell>
                <div className="flex gap-2">
                  <Tooltip
                    content="Reagendar"
                    color="warning"
                    className="capitalize"
                  >
                    <Button
                      onPress={() => {
                        onOpen();
                        setAppointment(appointment.id);
                      }}
                      className="capitalize"
                      isIconOnly
                      color="warning"
                    >
                      <FaEdit />
                    </Button>
                  </Tooltip>
                  <Modal isOpen={isOpen} onOpenChange={onOpenChange} className="bg-white">
                    <ModalContent>
                      {(onClose) => (
                        <>
                          <ModalHeader className="flex flex-col gap-1">
                            Reagendar Cita
                          </ModalHeader>
                          <ModalBody>
                            <Calendar
                              aria-label="Seleccione una fecha"
                              defaultValue={today(getLocalTimeZone())}
                              minValue={today(getLocalTimeZone())}
                              onChange={handleDateChange}
                              className="bg-white"
                            />
                            {availableSlots.length > 0 ? (
                              <RadioGroup
                                label="Selecciona un slot"
                                onChange={handleSlotChange}
                              >
                                {availableSlots.map((slot, index) => (
                                  <Radio
                                    key={index}
                                    value={slot.time}
                                    onChange={() => setSlot(slot.id)}
                                  >
                                    {`${slot.box}: Hora: ${JSON.parse(slot.time)
                                      .map((time) =>
                                        new Date(time).toLocaleTimeString()
                                      )
                                      .join(" - ")}`}
                                  </Radio>
                                ))}
                              </RadioGroup>
                            ) : (
                              <p>No hay slots disponibles para esta fecha.</p>
                            )}
                          </ModalBody>
                          <ModalFooter>
                            <Button
                              color="danger"
                              variant="flat"
                              onPress={onClose}
                            >
                              Cancelar
                            </Button>
                            {availableSlots.length > 0 && (
                              <Button
                                color="primary"
                                onPress={() => handleReprogram()}
                                disabled={isSaveDisabled}
                              >
                                Guardar cambios
                              </Button>
                            )}
                          </ModalFooter>
                        </>
                      )}
                    </ModalContent>
                  </Modal>
                  <Tooltip
                    content="Cancelar"
                    color="danger"
                    className="capitalize"
                  >
                    <Button
                      className="capitalize"
                      isIconOnly
                      color="danger"
                      onPress={() => {
                        setAppointment(appointment.id);
                        handleCancel();
                      }}
                    >
                      <FaTimes />
                    </Button>
                  </Tooltip>
                </div>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  );
};

export default ManageAppointments;
