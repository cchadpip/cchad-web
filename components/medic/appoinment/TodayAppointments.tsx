"use client";
import { format, isToday, parseISO } from "date-fns";
import { Appointment, TodayAppointmentsProps} from "../../../config/Interface/medic"
import { Button } from "@nextui-org/react";
import usePacientStore from "@/config/storage/pacient";

const TodayAppointments = ({ appoinmentData }: TodayAppointmentsProps) => {
    console.log("Today", appoinmentData);
    const getInitials = (name: string) => {
        const names = name.split(" ");
        const initials = names.map((n) => n[0]).join("");
        return initials.toUpperCase();
    };
    const setId = usePacientStore((state) => state.setId);
    const setIdConsultation = usePacientStore((state) => state.setIdConsultation);


    const handleStartConsultation = () => {
        window.location.href = "/medic/appointment/consultation";
    };
    
    const appointments: Appointment[] = [];
    if (appoinmentData) {
            appoinmentData.schedules.forEach((schedule) => {
            schedule.slots.forEach((slot) => {
                const slotTime = parseISO(slot.time.split(",")[0].replace(/[\[\]"]/g, ""));
                if (isToday(slotTime)) {
                slot.appointments.forEach((appointment) => {     
                    if (appointment.confirmed) {
                        setId(appointment.patient.id);
                        setIdConsultation(appointment.id);
                        appointments.push({
                            id: appointment.id, 
                            name: `${appointment.patient.name} ${appointment.patient.lastName}`,
                            time: format(slotTime, "hh:mm a"),
                            type: appointment.type,
                            state: appointment.state,
                            gender: appointment.patient.gender,
                            rut: appointment.patient.rut,
                            birthdate: appointment.patient.birthdate,
                        });
                    }
                });
                }
            });
            });
        }

        return (
            <div className="flex flex-col gap-4">
              {appointments.length > 0 ? (
                appointments.map((appointment) => (
                  <div
                    key={appointment.id}
                    className={`flex justify-between items-center p-4 border rounded-lg shadow-sm ${
                      appointment.state === 'Completada' || appointment.state === 'Reprogramada' ? 'opacity-50' : ''
                    }`}
                    style={{
                      borderColor: '#e0e0e0', 
                      backgroundColor: '#fafafa',
                    }}
                  >
                    <div className="flex items-center">
                      <div className="flex items-center justify-center w-10 h-10 bg-blue-500 text-white rounded-full mr-4">
                        {getInitials(appointment.name)}
                      </div>
                      <div>
                        <div className="font-bold text-lg text-gray-900">
                          {appointment.name}
                        </div>
                        <div className="text-gray-500">
                          {appointment.time}
                        </div>
                      </div>
                      <div className="text-blue-500 text-sm ml-4">
                        {appointment.type}
                      </div>
                    </div>
                    {appointment.state === 'Reservada' ? (
                      <button 
                        onClick={() => handleStartConsultation()} 
                        className="px-4 py-2 bg-green-500 text-white rounded-lg hover:bg-green-600 transition-colors duration-200"
                      >
                        Start Consultation
                      </button>
                    ) : (
                      <div className="text-green-500 font-medium">
                        Completed
                      </div>
                    )}
                  </div>
                ))
              ) : (
                <div className="text-center text-gray-600">
                  No hay citas para el día de hoy
                </div>
              )}
            </div>
          );
          
};

export default TodayAppointments;
