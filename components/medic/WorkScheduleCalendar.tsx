"use client";
import React from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "moment/locale/es";
import "react-big-calendar/lib/css/react-big-calendar.css";

const localizer = momentLocalizer(moment);
const messages = {
  previous: "<",
  next: ">",
  today: "Hoy",
  month: "Mes",
  week: "Semana",
  day: "Día",
  date: "Fecha",
  time: "Hora",
  event: "Evento",
  allDay: "Todo el día",
  work_week: "Semana de trabajo",
  yesterday: "Ayer",
  tomorrow: "Mañana",
  agenda: "Agenda",
  noEventsInRange: "No hay eventos en este rango",
  showMore: (total: number) => `+${total} más`,
};

const WorkScheduleCalendar = ({ events }) => {
  const minTime = new Date();
  minTime.setHours(8, 0, 0);

  const maxTime = new Date();
  maxTime.setHours(21, 0, 0);
  const eventPropGetter = (event) => {
    const backgroundColor = event.title.includes("Trabajo")
      ? "#3F8F8F "
      : "#57B9BB"; // Ejemplo de colores basados en el título
    return {
      style: { backgroundColor },
    };
  };

  return (
    <Calendar
      localizer={localizer}
      events={events}
      defaultView="day"
      views={["month", "week", "day"]}
      messages={messages}
      min={minTime}
      max={maxTime}
      style={{ height: 500, backgroundColor: "white", color: "black" }}
      eventPropGetter={eventPropGetter}
    />
  );
};

export default WorkScheduleCalendar;
