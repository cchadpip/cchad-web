import { useState, useEffect } from "react";
import { Input,Table, TableHeader, TableColumn, TableBody, TableRow, TableCell } from "@nextui-org/react";
import { Appointments, AppointmentHistoryProps } from "@/config/Interface/medic";
import { FaSort, FaSortUp, FaSortDown } from "react-icons/fa";


const AppointmentHistory = ({ userData }: AppointmentHistoryProps) => {
    const [appointments, setAppointments] = useState<Appointments[]>([]);
    const [searchTerm, setSearchTerm] = useState<string>("");
    const [sortConfig, setSortConfig] = useState<{ key: string; direction: string } | null>(null);
  
    useEffect(() => {
      if (userData) {
        const allAppointments = userData.schedules.flatMap(schedule =>
          schedule.slots.flatMap(slot =>
            slot.appointments.map(appointment => ({
              ...appointment,
              time: slot.time // Añadir la hora del slot a la cita
            }))
          )
        );
        setAppointments(allAppointments);
      }
    }, [userData]);
  
    const formatDate = (dateString: string) => {
      const options: Intl.DateTimeFormatOptions = { year: 'numeric', month: 'long', day: 'numeric' };
      return new Date(dateString).toLocaleDateString(undefined, options);
    };
  
    const formatTime = (timeString: string) => {
      const timeArray = JSON.parse(timeString);
      return `${new Date(timeArray[0]).toLocaleTimeString()} - ${new Date(timeArray[1]).toLocaleTimeString()}`;
    };
  
    const handleSort = (key: string) => {
      let direction = 'ascending';
      if (sortConfig && sortConfig.key === key && sortConfig.direction === 'ascending') {
        direction = 'descending';
      }
      setSortConfig({ key, direction });
    };
  
    const sortedAppointments = [...appointments].sort((a, b) => {
      if (sortConfig !== null) {
        const { key, direction } = sortConfig;
        if (key === 'date') {
          const dateA = new Date(JSON.parse(a.time)[0]).getTime();
          const dateB = new Date(JSON.parse(b.time)[0]).getTime();
          return direction === 'ascending' ? dateA - dateB : dateB - dateA;
        }
        if (key === 'patient') {
          const nameA = `${a.patient.name} ${a.patient.lastName}`.toLowerCase();
          const nameB = `${b.patient.name} ${b.patient.lastName}`.toLowerCase();
          if (nameA < nameB) return direction === 'ascending' ? -1 : 1;
          if (nameA > nameB) return direction === 'ascending' ? 1 : -1;
          return 0;
        }
      }
      return 0;
    });
  
    const filteredAppointments = sortedAppointments.filter(appointment =>
        (appointment.patient.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
        appointment.patient.lastName.toLowerCase().includes(searchTerm.toLowerCase())) &&
        appointment.state === 'Completada'
      );
  
    const getSortIcon = (key: string) => {
      if (!sortConfig || sortConfig.key !== key) {
        return <FaSort />;
      }
      if (sortConfig.direction === 'ascending') {
        return <FaSortUp />;
      }
      return <FaSortDown />;
    };
  
    return (
      <div className="bg-white p-4 rounded-lg">
        <Input
          isClearable
          className="w-full sm:max-w-[44%] mb-4"
          placeholder="Buscar por nombre de paciente"
          value={searchTerm}
          onChange={e => setSearchTerm(e.target.value)}
          variant="bordered"
        />
        <Table 
          aria-label="Historial de citas"
          className="bg-white text-gray-800 border border-gray-200"
          bgcolor="white"
          classNames={{
            table: "bg-white border border-gray-200",
            th: "bg-gray-200 text-gray-800 border-b border-gray-200",
            td: "bg-white text-gray-600 border-t border-gray-200",
          }}
        >
          <TableHeader>
            <TableColumn onClick={() => handleSort('date')}>
              <div className="flex items-center">
                Fecha {getSortIcon('date')}
              </div>
            </TableColumn>
            <TableColumn>Hora</TableColumn>
            <TableColumn onClick={() => handleSort('patient')}>
              <div className="flex items-center">
                Paciente {getSortIcon('patient')}
              </div>
            </TableColumn>
            <TableColumn>Tipo</TableColumn>
            <TableColumn>Diagnóstico</TableColumn>
            <TableColumn>Tratamiento</TableColumn>
            <TableColumn>Medicación</TableColumn>
          </TableHeader>
          <TableBody emptyContent={"No hay citas para mostrar."}>
            {filteredAppointments.map((appointment, index) => (
              <TableRow key={index}>
                <TableCell>{formatDate(JSON.parse(appointment.time)[0])}</TableCell>
                <TableCell>{formatTime(appointment.time)}</TableCell>
                <TableCell>{appointment.patient.name} {appointment.patient.lastName}</TableCell>
                <TableCell>{appointment.type}</TableCell>
                <TableCell>{appointment.diagnosis || 'N/A'}</TableCell>
                <TableCell>{appointment.treatment || 'N/A'}</TableCell>
                <TableCell>{appointment.prescriptionDrugs || 'N/A'}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    );    
}   

export default AppointmentHistory;
