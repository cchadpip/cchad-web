"use client";

import { useState } from "react";
import { Input, Button } from "@nextui-org/react";
import { MailIcon, EditIcon } from "../../incons";
import { getClient } from "@/config/graphql/apolloClient";
import useUserStore from "@/config/storage/user";
import { gql } from "@apollo/client";
import Swal from "sweetalert2";

const ProfileDetails = ({ userData }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [name, setName] = useState(userData.name);
  const [lastName, setLastName] = useState(userData.lastName);
  const [email, setEmail] = useState(userData.email);
  const [specialty, setSpecialty] = useState(userData.specialty);
  const [phone, setPhone] = useState(userData.phone);
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const userId = useUserStore((state) => state.id);
  const [showModal, setShowModal] = useState(false);
  const [error, setError] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    //llamar funcion para actualizar datos
    console.log("Datos actualizados:", {
      name,
      lastName,
      email,
      specialty,
      phone,
    });
    alert("Datos actualizados"); //cambiar
    window.location.href = "/medic/profile";
  };

  //validar datos

  const handleCancel = () => {
    setName(userData.name);
    setLastName(userData.lastName);
    setEmail(userData.email);
    setSpecialty(userData.specialty);
    setPhone(userData.phone);
    setIsEditing(false);
  };

  const handleChangePassword = async () => {
    if (newPassword !== confirmPassword) {
      setError("Las contraseñas no coinciden");
      return;
    }
    if (newPassword.length < 8) {
      setError("La contraseña debe tener al menos 8 caracteres");
      return;
    }
    try {
      const { data } = await getClient().mutate({
        mutation: gql`
          mutation ChangePassword($input: changePasswordDto!) {
            changePassword(input: $input) {
              patient {
                id
              }
            }
          }
        `,
        variables: {
          input: {
            userid: userId,
            role: "Paciente",
            newPassword: newPassword,
          },
        },
      });
      Swal.fire({
        icon: "success",
        title: "Contraseña cambiada exitosamente",
        showConfirmButton: false,
        timer: 1500,
      });
      setShowModal(false);
      setNewPassword("");
      setConfirmPassword("");
      setError("");
    } catch (error) {
      setError("Error al cambiar la contraseña");
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "Error al cambiar la contraseña",
      });
    }
  };

  return (
    <form onSubmit={handleSubmit} className="p-4 rounded shadow-md">
      <div className="flex justify-between items-center mb-4">
        {/*!isEditing && (
            <Button   isIconOnly onClick={() => setIsEditing(true)}>
                <EditIcon  />
            </Button>
            )*/}
      </div>
      <div className="block text-bg font-semibold mb-2">
        <Input
          label="Nombre"
          labelPlacement="outside"
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          className="pb-4 bg-withe"
          isReadOnly={!isEditing}
          variant="bordered"
        />
      </div>
      <div className="block text-bg font-semibold mb-2">
        <Input
          label="Apellido"
          labelPlacement="outside"
          type="text"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          className="pb-4 bg-white"
          isReadOnly={!isEditing}
          variant="bordered"
        />
      </div>
      <div className="block text-bg font-semibold mb-2">
        <Input
          label="Email"
          type="text"
          value={email}
          labelPlacement="outside"
          onChange={(e) => setEmail(e.target.value)}
          startContent={
            <MailIcon className="text-2xl text-default-400 pointer-events-none flex-shrink-0" />
          }
          className="pb-4"
          variant="bordered"
          isReadOnly={!isEditing}
        />
      </div>
      <div className="block text-bg font-semibold mb-2">
        <Input
          label="Especialidad"
          labelPlacement="outside"
          type="text"
          value={specialty}
          onChange={(e) => setSpecialty(e.target.value)}
          className="pb-4"
          variant="bordered"
          isReadOnly={!isEditing}
        />
      </div>
      <div className="block text-bg font-semibold mb-2">
        <Input
          isReadOnly={!isEditing}
          label="Teléfono"
          labelPlacement="outside"
          type="text"
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
          className="pb-4"
          variant="bordered"
          startContent={
            <div className="pointer-events-none flex items-center">
              <span className="text-default-400 text-small">+56</span>
            </div>
          }
        />
      </div>
      {isEditing && (
        <div className="flex justify-between">
          <Button
            type="submit"
            className="w-full bg-blue-500 text-bg p-2 rounded hover:bg-blue-600 mr-2"
          >
            Guardar Cambios
          </Button>
          <Button
            color="danger"
            className="w-full text-bg p-2 rounded hover:bg-gray-600"
            onClick={handleCancel}
          >
            Cancelar
          </Button>
        </div>
      )}
    </form>
  );
};

export default ProfileDetails;
