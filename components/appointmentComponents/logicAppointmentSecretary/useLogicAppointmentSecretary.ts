"use client";
import { getClient } from "@/config/graphql/apolloClient";
import { GET_MEDICS_FOR_SECRETARY, GET_MEDIC_FOR_SECRETARY, CONFIRM_APPOINTMENT, CANCEL_APPOINTMENT, RESCHEDULE_APPOINTMENT } from "@/config/graphql/mutations/serviceSecretary/serviceAppointment/service";
import { Medic, Schedule } from "@/config/Interface/interface";
import { useCallback, useEffect, useState } from "react";
import Swal from "sweetalert2";

export default function useSecretaryAppointmentLogic() {
    const [medics, setMedics] = useState<Medic[]>([]); 
    const [medic, setMedic] = useState<Medic | null>(null);
    const [medicSchedules, setMedicSchedules] = useState<Schedule[]>([]);
    const [selectedAppointment, setSelectedAppointment] = useState<any | null>(null);
    const [appointments, setAppointments] = useState<any[]>([]);
    const [selectedDate, setSelectedDate] = useState<string>("");
    const [availableSlots, setAvailableSlots] = useState<any[]>([]);
    const [selectedSlot, setSelectedSlot] = useState<number | null>(null); // Añadido

    const fetchMedics = async () => {
        try {
            const { data } = await getClient().query({ query: GET_MEDICS_FOR_SECRETARY });
            setMedics(data.medics);
        } catch (error) {
            handleError(error, 'Error al obtener los médicos');
        }
    };

    const fetchMedic = useCallback(async (medicId: number) => {
        try {
            const { data } = await getClient().query<{ medic: Medic }>({
                query: GET_MEDIC_FOR_SECRETARY,
                variables: { id: medicId },
            });
            setMedic(data.medic);
            if (data.medic && data.medic.schedules) {
                setMedicSchedules(data.medic.schedules);
                const allAppointments = data.medic.schedules.flatMap(schedule =>
                    schedule.slots.flatMap(slot =>
                        slot.appointments.map(appointment => ({
                            ...appointment,
                            time: JSON.parse(slot.time)[0], // Assuming you want the start time of the slot
                        }))
                    )
                );
                setAppointments(allAppointments);
            }
        } catch (error) {
            handleError(error, 'Error al obtener los detalles del médico');
        }
    }, []);

    const confirmAppointment = async (appointmentId: number) => {
        const result = await Swal.fire({
            title: '¿Estás seguro?',
            text: "¡No podrás revertir esto!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, confirmar',
            cancelButtonText: 'Cancelar'
        });
        if (result.isConfirmed) {
            try {
                const { data } = await getClient().mutate({
                    mutation: CONFIRM_APPOINTMENT,
                    variables: { id: appointmentId },
                });
                Swal.fire("Éxito", "Cita confirmada exitosamente", "success");
                setAppointments(prevAppointments =>
                    prevAppointments.map(appointment =>
                        appointment.id === appointmentId
                            ? { ...appointment, confirmed: data.confirmAppointment.confirmed, diagnosis: data.confirmAppointment.diagnosis || '' }
                            : appointment
                    )
                );
            } catch (error) {
                handleError(error, 'Error al confirmar la cita');
            }
        }
    };

    const cancelAppointment = async (appointmentId: number) => {
        const result = await Swal.fire({
            title: '¿Estás seguro?',
            text: "¡No podrás revertir esto!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, cancelar',
            cancelButtonText: 'Cancelar'
        });
        if (result.isConfirmed) {
            try {
                const { data } = await getClient().mutate({
                    mutation: CANCEL_APPOINTMENT,
                    variables: { id: appointmentId },
                });
                Swal.fire("Éxito", "Cita cancelada exitosamente", "success");
                setAppointments(prevAppointments =>
                    prevAppointments.filter(appointment => appointment.id !== appointmentId)
                );
            } catch (error) {
                handleError(error, 'Error al cancelar la cita');
            }
        }
    };

    const rescheduleAppointment = async (appointmentId: number, newSlotId: number) => {
        const result = await Swal.fire({
            title: '¿Estás seguro?',
            text: "¡No podrás revertir esto!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, reprogramar',
            cancelButtonText: 'Cancelar'
        });
        if (result.isConfirmed) {
            try {
                const { data } = await getClient().mutate({
                    mutation: RESCHEDULE_APPOINTMENT,
                    variables: { input: { appointmentId, newSlotId } },
                });
                Swal.fire("Éxito", "Cita reprogramada exitosamente", "success");
                setAppointments(prevAppointments =>
                    prevAppointments.map(appointment =>
                        appointment.id === appointmentId
                            ? { ...appointment, slot: { ...appointment.slot, id: newSlotId }, diagnosis: data.rescheduleAppointment.diagnosis || '' }
                            : appointment
                    )
                );
            } catch (error) {
                handleError(error, 'Error al reprogramar la cita');
            }
        }
    };

    const filterSlotsByDate = (date) => {
        const formattedDate = date.toString();
        setSelectedDate(formattedDate);
        console.log('Fecha seleccionada:', formattedDate);
        if (medicSchedules.length > 0) {
            const availableSlots = medicSchedules.flatMap(schedule =>
                schedule.slots.filter(slot =>
                    JSON.parse(slot.time)[0].startsWith(formattedDate)
                )
            );
            setAvailableSlots(availableSlots);
        }
    };

    const handleError = (error: unknown, title: string) => {
        let message = 'Ocurrió un error inesperado';
        if (error instanceof Error) {
            message = error.message;
        } else if (typeof error === 'string') {
            message = error;
        }
        Swal.fire({
            icon: 'error',
            title: title,
            text: message,
        });
    };

    useEffect(() => {
        fetchMedics();
    }, []);

    return {
        medics,
        fetchMedics,
        fetchMedic,
        medicSchedules,
        medic,
        selectedAppointment,
        setSelectedAppointment,
        appointments,
        confirmAppointment,
        cancelAppointment,
        rescheduleAppointment,
        selectedDate,
        setSelectedDate,
        filterSlotsByDate,
        availableSlots,
        selectedSlot, // Añadido
        setSelectedSlot, // Añadido
    }
}
