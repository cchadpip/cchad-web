// components/withAuth.tsx
"use client";
import { useEffect, useState } from "react";
import { NextPage } from "next";
import useUserStore from "@/config/storage/user";
import LoadingBar from "./loadingBar";

type WithAuthProps = {};

const withAuth = (WrappedComponent: NextPage, allowedRoles: string[]) => {
  const ComponentWithAuth: NextPage<WithAuthProps> = (props) => {
    const type = useUserStore((state) => state.type);
    const token = useUserStore((state) => state.token);
    const [loading, setLoading] = useState(true);
    useEffect(() => {
      if (!token || !type || !allowedRoles.includes(type)) {
        window.location.href = "/404"; // Redirige a la página de login si no está autenticado o no autorizado
      } else {
        setLoading(false);
      }
    }, []);

    if (loading) {
      return (
        <div>
          <LoadingBar isLoading={loading} />
        </div>
      );
    }
    return <WrappedComponent {...props} />;
  };

  // Copia las propiedades estáticas del componente original al componente envuelto
  if (WrappedComponent.getInitialProps) {
    ComponentWithAuth.getInitialProps = WrappedComponent.getInitialProps;
  }

  return ComponentWithAuth;
};

export default withAuth;
