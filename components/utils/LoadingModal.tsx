import React from "react";
import { Modal, ModalContent, ModalBody, Progress } from "@nextui-org/react";

interface LoadingModalProps {
  isOpen: boolean;
  onClose: () => void;
}

const LoadingModal: React.FC<LoadingModalProps> = ({ isOpen, onClose }) => {
  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      isDismissable={false}
      aria-labelledby="loading-modal"
      size="lg"
    >
      <ModalContent>
        <ModalBody>
          <div className="flex flex-col items-center justify-center">
            <p className="mb-4">Actualizando calendario...</p>
            <Progress isIndeterminate />
          </div>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default LoadingModal;
