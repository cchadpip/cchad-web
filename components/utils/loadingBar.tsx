import React from "react";
import { Progress } from "@nextui-org/react";

const LoadingBar: React.FC<{ isLoading: boolean }> = ({ isLoading }) => {
  return isLoading ? (
    <Progress isIndeterminate size="lg" color="primary" />
  ) : null;
};

export default LoadingBar;
