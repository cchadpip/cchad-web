import { ROLES } from "@/config/Interface/interface";

export const ROLE_MAPPINGS: { [key in ROLES]: string } = {
    [ROLES.Admin]: "Administrador",
    [ROLES.Secretary]: "Secretaria",
    [ROLES.Patient]: "Paciente",
    [ROLES.Doctor]: "Médico",
};

export const getRoleFromEmail = (email: string | null): ROLES | null => {
    if (!email) {
        console.error('Email is undefined or null');
        return null;
    }
    
    if (email.startsWith("/")) return ROLES.Admin;
    if (email.startsWith("#")) return ROLES.Secretary;
    return null;
};
