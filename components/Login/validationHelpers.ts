export const validateEmail = (email: string): boolean => {
    const regex = /^#?\/?[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    return regex.test(email);
};


export const validatePassword = (password: string): boolean => {
    return password.length >= 8;
};

export const normalizeEmail = (email: string): string => {
    if (email.startsWith("/") || email.startsWith("#")) {
        return email.substring(1);
    }
    return email;
};
