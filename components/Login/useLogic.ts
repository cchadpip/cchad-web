"use client"
import React, { useEffect, useState } from "react";
import { getClient } from '@/config/graphql/apolloClient';
import { AUTHENTICATE_USER } from '@/config/graphql/mutations/mutationLogin/mutation';
import Swal from "sweetalert2";
import { redirect } from 'next/navigation';
import { validateEmail, validatePassword, normalizeEmail } from './validationHelpers';
import { ROLE_MAPPINGS, getRoleFromEmail } from './roleManagement';
import { ROLES } from "@/config/Interface/interface";
import useUserStore
 from "@/config/storage/user";
export const useLogic = () => {
    // Estado para verificar si el login fue exitoso.
    const [loginSuccessful, setLoginSuccessful] = useState(false);
    // Estado para controlar la visibilidad de la contraseña en el formulario.
    const [isVisible, setIsVisible] = useState(false);
    // Estado para almacenar el correo electrónico ingresado por el usuario.
    const [email, setEmail] = useState<string>("");
    // Estado para almacenar la contraseña ingresada por el usuario.
    const [password, setPass] = useState<string>("");
    // Estado para almacenar el rol del usuario basado en su correo electrónico.
    const [role, setRole] = useState<ROLES | null>(null);
    
    const {setId, setToken, setType } = useUserStore();


    /**
     * Alterna la visibilidad de la contraseña en la interfaz de usuario.
     */
    const toggleVisibility = () => setIsVisible(!isVisible);

    /**
     * Determina si el correo electrónico ingresado es inválido.
     * @returns {boolean} True si el correo electrónico es inválido, false en caso contrario.
     */
    const isInvalid = React.useMemo(() => {
        return email ? !validateEmail(email) : false;
    }, [email]);

    /**
     * Determina si la contraseña ingresada es inválida.
     * @returns {boolean} True si la contraseña es inválida, false en caso contrario.
     */
    const isInvalidPassword = React.useMemo(() => {
        return password ? !validatePassword(password) : false;
    }, [password]);

    /**
     * Establece el rol del usuario en el estado, basado en la selección del usuario o derivado del correo electrónico.
     * @param {ROLES} selectedRole - El rol seleccionado o inferido para el usuario.
     */
    const handleRoleSelection = (selectedRole: ROLES) => {
        
        setRole(selectedRole);
    };

    /**
     * Maneja el proceso de login, incluyendo la validación y la comunicación con el servidor.
     * @param {ROLES} selectedRole - El rol del usuario que intenta iniciar sesión.
     * @async
     */
const handleLogin = async (selectedRole: ROLES) => {
    const normalizedEmail = normalizeEmail(email);
    let inferredRole = getRoleFromEmail(email);
    let finalRole: ROLES | null = inferredRole !== null ? inferredRole : selectedRole;

    if (finalRole === null) {
        console.error("Role is not defined");
        return;
    }

    let finalRoleName = ROLE_MAPPINGS[finalRole];

    if (!email || !password) {
        Swal.fire({
            icon: 'error',
            title: 'Campos vacíos',
            text: 'Por favor, rellene los campos vacíos.',
        });
        return;
    }

    if (!validateEmail(email) || !validatePassword(password)) {
        Swal.fire({
            icon: 'error',
            title: 'Campos inválidos',
            text: 'Por favor, asegúrese de que su correo electrónico y contraseña sean correctos.',
        });
        return;
    }

    try {
        const { data } = await getClient().mutate({
            mutation: AUTHENTICATE_USER,
            variables: {
                email: normalizedEmail,
                password: password,
                role: finalRoleName,
            },
        });

        if (data) {
            console.log("Data from login mutation:");
            console.log(data);
            
            const user = data.authenticateUser.user;
            let userId = null;

            if (user.admin) {
                userId = user.admin.id;
            } else if (user.patient) {
                userId = user.patient.id;
            } else if (user.medic) {
                userId = user.medic.id;
                console.log("User ID:", userId);
            }

            setType(finalRoleName);
            setToken(data.authenticateUser.token);
            if(userId){setId(userId);}
             // Almacena el ID del usuario

            Swal.fire('Logged in!', '', 'success');
            setLoginSuccessful(true); // Verifica si estamos en el navegador
        }
    } catch (error) {
        console.error("An error occurred during login:", error);
        Swal.fire({
            icon: 'error',
            title: 'Error de autenticación',
            text: 'Por favor, asegúrese de que su correo electrónico y contraseña sean correctos.',
        });
    }
};


    /**
     * Efecto que redirige al usuario a la página de inicio una vez que el login es exitoso.
     */
    useEffect(() => {
        if (loginSuccessful) {
            window.location.href = "/";
        }
    }, [loginSuccessful]);

    return {
        setEmail,
        setPass,
        handleLogin,
        isInvalid,
        isInvalidPassword,
        toggleVisibility,
        isVisible
    };
};

