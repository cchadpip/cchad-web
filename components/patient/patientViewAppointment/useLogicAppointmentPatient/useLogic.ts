import { useState } from "react";
import Swal from "sweetalert2";
import useUserStore from "@/config/storage/user";
import { getClient } from "@/config/graphql/apolloClient";
import { GET_PATIENT } from "@/config/graphql/mutations/servicePatient/service";
import { Appointment } from "@/config/Interface/patient";
import { CANCEL_APPOINTMENT } from "@/config/graphql/mutations/serviceSecretary/serviceAppointment/service";
import { AppointmentState } from "@/config/Interface/ENUM";

export function useLogicAppointmentPatient() {
  const [appointments, setAppointments] = useState<Appointment[]>([]);
  const [fetched, setFetched] = useState(false);
  const patientId = useUserStore((state) => state.id);

  const handleError = (error: unknown, title: string) => {
    let message = "An unexpected error occurred";
    if (error instanceof Error) {
      message = error.message;
    } else if (typeof error === "string") {
      message = error;
    }
    Swal.fire({
      icon: "error",
      title: title,
      text: message,
    });
  };

  const fetchPatient = async () => {
    if (patientId === null) {
      handleError("Patient ID is null", "Error fetching patient");
      return;
    }

    try {
      const { data } = await getClient().query({
        query: GET_PATIENT,
        variables: {
          id: parseFloat(patientId.toString()),
        },
      });

      // Obtener todas las citas sin filtrar
      const allAppointments = data.patient.appointments;
      setAppointments(allAppointments);
      setFetched(true); // Marcar como completado
    } catch (error: any) {
      handleError(error, "Error fetching patient");
    }
  };

  const cancelAppointment = async (appointmentId: number) => {
    try {
      await getClient().mutate({
        mutation: CANCEL_APPOINTMENT,
        variables: { id: appointmentId },
      });
      Swal.fire("Exito", "Cita Cancelada exitosamente", "success");
      fetchPatient(); // Refrescar las citas
    } catch (error: any) {
      handleError(error, "Error cancelling appointment");
    }
  };

  return { appointments, fetchPatient, cancelAppointment };
}
