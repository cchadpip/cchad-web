import React, { useEffect, useState } from "react";
import { useLogicAppointmentPatient } from "./useLogicAppointmentPatient/useLogic";
import { Card, CardBody, Spacer, Skeleton } from "@nextui-org/react";
import { AppointmentState } from "@/config/Interface/patient";
import { format } from "date-fns";
import { es } from "date-fns/locale";
import "@/styles/viewAppointment.css";

const AppointmentView: React.FC = () => {
  const { appointments, fetchPatient, cancelAppointment } =
    useLogicAppointmentPatient();

  const [loading, setLoading] = useState(true); // Estado para controlar la carga inicial

  useEffect(() => {
    const fetchData = async () => {
      await fetchPatient(); // Fetch patient data on page load
      setLoading(false); // Marcar la carga como completada al recibir datos
    };
    fetchData();
  }, []);

  if (loading) {
    return (
      <div className="container mx-auto p-4">
        <h1 className="text-3xl font-semibold mb-6 text-black">
          Historial de Citas Médicas
        </h1>
        <div className="grid grid-cols-2 gap-6">
          <div>
            <h2 className="text-2xl font-medium mb-4 text-black">
              Citas Futuras
            </h2>
            <Card className="mb-6 bg-white shadow-lg">
              <CardBody>
                <Skeleton className="w-full h-6 bg-gray-100 rounded-md" />
                <Spacer y={1} />
                <Skeleton className="w-full bg-gray-100 h-4 rounded-md mb-2" />
                <Skeleton className="w-full bg-gray-100 h-4 rounded-md mb-2" />
                <Skeleton className="w-full bg-gray-100 h-4 rounded-md mb-2" />
              </CardBody>
            </Card>
          </div>
          <div>
            <h2 className="text-2xl font-medium mb-4 text-black">
              Citas Completadas
            </h2>
            <Card className="mb-6 bg-white shadow-lg">
              <CardBody>
                <Skeleton className="w-full h-6 bg-gray-100 rounded-md" />
                <Spacer y={1} />
                <Skeleton className="w-full bg-gray-100 h-4 rounded-md mb-2" />
                <Skeleton className="w-full bg-gray-100 h-4 rounded-md mb-2" />
                <Skeleton className="w-full bg-gray-100 h-4 rounded-md mb-2" />
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
    );
  }

  // Una vez que la carga ha terminado y hay citas disponibles
  const upcomingAppointments = appointments.filter(
    (appointment) =>
      appointment.state !== AppointmentState.Completada &&
      new Date(JSON.parse(appointment.slot.time)[0]) >= new Date()
  );

  const completedAppointments = appointments.filter(
    (appointment) => appointment.state === AppointmentState.Completada
  );

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-3xl font-semibold mb-6 text-black">
        Historial de Citas Médicas
      </h1>
      <div className="grid grid-cols-1 gap-8 lg:grid-cols-2">
        <div>
          <h2 className="text-2xl font-medium mb-4 text-black">
            Citas Futuras
          </h2>
          {upcomingAppointments.length === 0 && (
            <p className="text-black">No hay citas futuras.</p>
          )}
          {upcomingAppointments.map((appointment) => (
            <Card key={appointment.id} className="mb-6 bg-white shadow-lg">
              <CardBody className="grid grid-cols-2 gap-4">
                <div>
                  <div className="font-semibold text-black">Fecha:</div>
                  <div className="text-black">
                    {format(
                      new Date(JSON.parse(appointment.slot.time)[0]),
                      "PPPP",
                      { locale: es }
                    )}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Hora:</div>
                  <div className="text-black">
                    {format(
                      new Date(JSON.parse(appointment.slot.time)[0]),
                      "p",
                      { locale: es }
                    )}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Médico:</div>
                  <div className="text-black">
                    {appointment.slot.schedule.medic.name}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Especialidad:</div>
                  <div className="text-black">
                    {appointment.slot.schedule.medic.specialty}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Estado:</div>
                  <div className="text-black">{appointment.state}</div>
                </div>
                <div>
                  <div className="font-semibold text-black">Confirmada:</div>
                  <div className="text-black">
                    {appointment.confirmed ? "Sí" : "No"}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Tipo:</div>
                  <div className="text-black">{appointment.type}</div>
                </div>
                <div>
                  <div className="font-semibold text-black">Diagnóstico:</div>
                  <div className="text-black">
                    {appointment.diagnosis || "N/A"}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Tratamiento:</div>
                  <div className="text-black">
                    {appointment.treatment || "N/A"}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">
                    Medicamentos Recetados:
                  </div>
                  <div className="text-black">
                    {appointment.prescriptionDrugs || "N/A"}
                  </div>
                </div>
                <div className="col-span-2">
                  <button
                    onClick={() => cancelAppointment(appointment.id)}
                    className="mt-4 bg-red-500 text-white p-2 rounded"
                  >
                    Cancelar Cita
                  </button>
                </div>
              </CardBody>
            </Card>
          ))}
        </div>
        <div>
          <h2 className="text-2xl font-medium mb-4 text-black">
            Citas Completadas
          </h2>
          {completedAppointments.length === 0 && (
            <p className="text-black">No hay citas completadas.</p>
          )}
          {completedAppointments.map((appointment) => (
            <Card key={appointment.id} className="mb-6 bg-white shadow-lg">
              <CardBody className="grid grid-cols-2 gap-4">
                <div>
                  <div className="font-semibold text-black">Fecha:</div>
                  <div className="text-black">
                    {format(
                      new Date(JSON.parse(appointment.slot.time)[0]),
                      "PPPP",
                      { locale: es }
                    )}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Hora:</div>
                  <div className="text-black">
                    {format(
                      new Date(JSON.parse(appointment.slot.time)[0]),
                      "p",
                      { locale: es }
                    )}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Médico:</div>
                  <div className="text-black">
                    {appointment.slot.schedule.medic.name}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Especialidad:</div>
                  <div className="text-black">
                    {appointment.slot.schedule.medic.specialty}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Estado:</div>
                  <div className="text-black">{appointment.state}</div>
                </div>
                <div>
                  <div className="font-semibold text-black">Confirmada:</div>
                  <div className="text-black">
                    {appointment.confirmed ? "Sí" : "No"}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Tipo:</div>
                  <div className="text-black">{appointment.type}</div>
                </div>
                <div>
                  <div className="font-semibold text-black">Diagnóstico:</div>
                  <div className="text-black">
                    {appointment.diagnosis || "N/A"}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">Tratamiento:</div>
                  <div className="text-black">
                    {appointment.treatment || "N/A"}
                  </div>
                </div>
                <div>
                  <div className="font-semibold text-black">
                    Medicamentos Recetados:
                  </div>
                  <div className="text-black">
                    {appointment.prescriptionDrugs || "N/A"}
                  </div>
                </div>
              </CardBody>
            </Card>
          ))}
        </div>
      </div>
    </div>
  );
};

export default AppointmentView;
