import { useState } from "react";
import {
  Input,
  Button,
  Popover,
  PopoverTrigger,
  PopoverContent,
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Link,
  Avatar,
} from "@nextui-org/react";
import { MailIcon, EditIcon, UserIcon } from "@/components/incons";
import { getClient } from "@/config/graphql/apolloClient";
import { gql } from "@apollo/client";
import useUserStore from "@/config/storage/user";
import Swal from "sweetalert2";

const ProfileDetails = ({ userData }) => {
  const [name] = useState(userData.name);
  const [lastName] = useState(userData.lastName);
  const [email] = useState(userData.email);
  const [phone] = useState(userData.phone);
  const [birthdate] = useState(userData.birthdate);
  const [rut] = useState(userData.rut);

  const userId = useUserStore((state) => state.id);

  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [error, setError] = useState("");

  const handleChangePassword = async () => {
    if (newPassword !== confirmPassword) {
      setError("Las contraseñas no coinciden");
      return;
    }
    if (newPassword.length < 8) {
      setError("La contraseña debe tener al menos 8 caracteres");
      return;
    }
    try {
      const { data } = await getClient().mutate({
        mutation: gql`
          mutation ChangePassword($input: changePasswordDto!) {
            changePassword(input: $input) {
              patient {
                id
              }
            }
          }
        `,
        variables: {
          input: {
            userid: userId,
            role: "Paciente",
            newPassword: newPassword,
          },
        },
      });
      Swal.fire({
        icon: "success",
        title: "Contraseña cambiada exitosamente",
        showConfirmButton: false,
        timer: 1500,
      });
      setShowModal(false);
      setNewPassword("");
      setConfirmPassword("");
      setError("");
    } catch (error) {
      setError("Error al cambiar la contraseña");
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "Error al cambiar la contraseña",
      });
    }
  };

  return (
    <div className="w-full max-w-3xl mx-auto bg-white rounded-lg shadow-lg p-6 md:p-8">
      <div className="flex flex-col md:flex-row items-center md:items-start gap-6 md:gap-8">
        <div className="flex-shrink-0">
          <Avatar
            showFallback
            src="https://images.unsplash.com/broken"
            size="lg"
            className="w-24 h-24"
          ></Avatar>
        </div>
        <div className="flex-1 space-y-4">
          <div className="flex justify-between items-center mb-4">
            <h2 className="text-lg font-heading text-black mb-2">
              Datos Personales
            </h2>
            <Popover>
              <PopoverTrigger>
                <Button isIconOnly className="text-primary">
                  <EditIcon />
                </Button>
              </PopoverTrigger>
              <PopoverContent>
                <Button
                  onClick={() => setShowModal(true)}
                  className="text-primary"
                >
                  Cambiar contraseña
                </Button>
              </PopoverContent>
            </Popover>
          </div>
          <div className="grid grid-cols-2 gap-4">
            <div>
              <label htmlFor="name" className="text-black">
                Nombre
              </label>
              <Input
                id="name"
                value={name}
                isReadOnly
                className="bg-white text-primary"
              />
            </div>
            <div>
              <label htmlFor="surname" className="text-black">
                Apellido
              </label>
              <Input
                id="surname"
                value={lastName}
                isReadOnly
                className="bg-white"
              />
            </div>
          </div>
          <div className="max-w-80">
            <label htmlFor="phone" className="text-black">
              Rut
            </label>
            <Input
              id="phone"
              value={rut}
              startContent={
                <div className="pointer-events-none flex items-center">
                  <span className="text-primary text-lg "></span>
                </div>
              }
              isReadOnly
              className="bg-white"
            />
          </div>
          <div className="grid grid-cols-2 gap-4">
            <div>
              <label htmlFor="email" className="text-black">
                Email
              </label>
              <Input
                id="email"
                value={email}
                startContent={
                  <MailIcon className="text-2xl text-primary pointer-events-none flex-shrink-0" />
                }
                isReadOnly
                className="bg-white"
              />
            </div>
            <div>
              <label htmlFor="phone" className="text-black">
                Teléfono
              </label>
              <Input
                id="phone"
                value={phone}
                startContent={
                  <div className="pointer-events-none flex items-center">
                    <span className="text-primary text-lg ">+56</span>
                  </div>
                }
                isReadOnly
                className="bg-white"
              />
            </div>
            <div>
              <label htmlFor="phone" className="text-black">
                Nacimiento
              </label>
              <Input
                id="phone"
                value={birthdate}
                startContent={
                  <div className="pointer-events-none flex items-center">
                    <span className="text-primary text-lg "></span>
                  </div>
                }
                isReadOnly
                className="bg-white"
              />
            </div>
          </div>
        </div>
      </div>

      <Modal isOpen={showModal} onClose={() => setShowModal(false)}>
        <ModalContent>
          <ModalHeader className="bg-primary text-white">
            Cambiar Contraseña
          </ModalHeader>
          <ModalBody>
            <p className="">Nueva Contraseña</p>
            <Input
              type="password"
              value={newPassword}
              onChange={(e) => setNewPassword(e.target.value)}
              className="pb-4"
            />
            <p className="">Repetir Contraseña</p>
            <Input
              type="password"
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
              className="pb-4"
            />
            {error && <p className="text-danger">{error}</p>}
          </ModalBody>
          <ModalFooter>
            <Button
              onClick={handleChangePassword}
              className="bg-primary text-white"
            >
              Cambiar Contraseña
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </div>
  );
};

export default ProfileDetails;

function PencilIcon(props) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M17 3a2.85 2.83 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5Z" />
      <path d="m15 5 4 4" />
    </svg>
  );
}
