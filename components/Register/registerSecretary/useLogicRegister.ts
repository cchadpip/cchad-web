// src/hooks/useRegisterSecretary.js

import { useMutation } from '@apollo/client';
import { REGISTER_SECRETARY } from '@/config/graphql/mutations/mutationsRegister/mutations';
import Swal from 'sweetalert2';
import {SecretaryInput} from '@/config/Interface/interface'; 

export const useRegisterSecretary = () => {
    const [registerSecretaryMutation] = useMutation<{ registerSecretary: SecretaryInput }>(REGISTER_SECRETARY);

    const registerSecretary = async (input: SecretaryInput) => {
        const { name, lastName, email } = input;

        if (name === "" || lastName === "" || email === "") {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Todos los campos son obligatorios',
        });
        return;
        }

        try {
        const { data } = await registerSecretaryMutation({
            variables: input
        });
        Swal.fire("Registro exitoso de Secretaria", "La secretaria ha sido registrada exitosamente", "success");
        console.log(data);
        } catch (error) {
        console.error("Error registrando a la secretaria", error);
        Swal.fire("Error", "No se pudo registrar a la secretaria", "error");
        }
    };

    return registerSecretary;
};
