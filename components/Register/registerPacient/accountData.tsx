import React, { useState } from "react";
import { Input, Button, Avatar } from "@nextui-org/react";
import { Patient } from "@/config/Interface/interface";
import Swal from "sweetalert2";

interface DatosCuentaProps {
  pasoAnterior: () => void;
  actualizarDatosPaciente: (nuevosDatos: Partial<Patient>) => void;
  handleRegister: () => void;
  datosPaciente: Partial<Patient>;
}

const DatosCuenta: React.FC<DatosCuentaProps> = ({
  pasoAnterior,
  actualizarDatosPaciente,
  handleRegister,
  datosPaciente,
}) => {
  const [password, setPassword] = useState(datosPaciente.password ?? "");
  const [repeatPassword, setRepeatPassword] = useState("");

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newPassword = e.target.value;
    setPassword(newPassword);
    actualizarDatosPaciente({ password: newPassword });
  };

  const handleRepeatPasswordChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRepeatPassword(e.target.value);
  };

  const handleFinalRegister = () => {
    if (password !== repeatPassword) {
      Swal.fire({
        icon: "error",
        title: "Contraseñas no coinciden",
        text: "Por favor, asegúrate de que ambas contraseñas coincidan.",
      });
      return;
    }
    handleRegister();
  };

  const validatePassword = (value: string) => value.length >= 8;
  const isInvalid = React.useMemo(() => {
    if (password === "") return false;
    return !validatePassword(password);
  }, [password]);

  return (
    <div className="w-full max-w-3xl mx-auto bg-background rounded-lg shadow-lg p-6 md:p-8">
      <div className="flex flex-col md:flex-row items-center md:items-start gap-6 md:gap-8">
        <div className="flex-shrink-0">
          <Avatar
            className="w-20 h-20 md:w-24 md:h-24 text-large"
            src="/placeholder-user.jpg"
            showFallback
          />
        </div>
        <div className="flex-1 space-y-4 w-full">
          <Input
            size="lg"
            type="email"
            label="Correo Electrónico"
            fullWidth
            value={datosPaciente.email ?? ""}
            onChange={(e) => actualizarDatosPaciente({ email: e.target.value })}
            variant="underlined"
            className="mb-4"
          />
          <Input
            size="lg"
            color={isInvalid ? "danger" : "primary"}
            type="password"
            label="Contraseña"
            fullWidth
            value={password}
            className="mb-4"
            isInvalid={isInvalid}
            onChange={handlePasswordChange}
            errorMessage={
              isInvalid ? "La contraseña debe tener al menos 8 caracteres" : ""
            }
            variant="underlined"
          />
          <Input
            size="lg"
            type="password"
            label="Repetir Contraseña"
            fullWidth
            value={repeatPassword}
            onChange={handleRepeatPasswordChange}
            variant="underlined"
            className="mb-4"
          />

          <div className="flex justify-end mt-6 gap-4">
            <Button color="primary" variant="ghost" onClick={pasoAnterior}>
              Atrás
            </Button>
            <Button color="primary" onClick={handleFinalRegister}>
              Registrar
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DatosCuenta;
