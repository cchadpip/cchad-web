import React, { useEffect, useState } from "react";
import { Input, Button, Select, SelectItem } from "@nextui-org/react";
import { PatientForecast, PatientGender } from "@/config/Interface/ENUM";
import { Patient } from "@/config/Interface/interface";
import Swal from "sweetalert2";

interface DatosPersonalesProps {
  siguientepaso: () => void;
  actualizarDatosPaciente: (nuevosDatos: Partial<Patient>) => void;
  handleRegister: () => void;
  datosPaciente: Partial<Patient>;
}

const forecastOptions = Object.values(PatientForecast).map((forecast) => ({
  label: forecast,
  value: forecast,
}));

const genderOptions = Object.values(PatientGender).map((gender) => ({
  label: gender === PatientGender.MALE ? "Masculino" : "Femenino",
  value: gender === PatientGender.MALE ? "Masculino" : "Femenino",
}));

const DatosPersonales: React.FC<DatosPersonalesProps> = ({
  siguientepaso,
  actualizarDatosPaciente,
  handleRegister,
  datosPaciente,
}) => {
  const [password, setPassword] = useState(datosPaciente.password ?? "");
  const [repeatPassword, setRepeatPassword] = useState("");

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newPassword = e.target.value;
    setPassword(newPassword);
    actualizarDatosPaciente({ password: newPassword });
  };

  const handleRepeatPasswordChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRepeatPassword(e.target.value);
  };

  const handleFinalRegister = () => {
    if (password !== repeatPassword) {
      Swal.fire({
        icon: "error",
        title: "Contraseñas no coinciden",
        text: "Por favor, asegúrate de que ambas contraseñas coincidan.",
      });
      return;
    }

    handleRegister();
  };

  const handleBirthdateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const date = e.target.value;
    // Convertir la fecha al formato yyyy/mm/dd
    const formattedDate = date.split("-").join("/");
    actualizarDatosPaciente({ birthdate: formattedDate });
  };

  return (
    <div className="w-full max-w-3xl mx-auto bg-background rounded-lg shadow-lg p-6 md:p-8">
      <div className="flex flex-col md:flex-row items-center md:items-start gap-6 md:gap-8">
        <div className="flex-shrink-0">
          <div className="w-20 h-20 md:w-24 md:h-24 bg-primary text-primary-foreground rounded-full flex items-center justify-center">
            <span>JD</span>
          </div>
        </div>
        <div className="flex-1 space-y-4">
          <div className="grid grid-cols-2 gap-4">
            <div>
              <label
                htmlFor="rut"
                className="block text-sm font-medium text-gray-700"
              >
                Rut del paciente
              </label>
              <Input
                id="rut"
                size="lg"
                value={datosPaciente.rut ?? ""}
                onChange={(e) =>
                  actualizarDatosPaciente({ rut: e.target.value })
                }
                variant="underlined"
              />
            </div>
            <div>
              <label
                htmlFor="name"
                className="block text-sm font-medium text-gray-700"
              >
                Nombre del Paciente
              </label>
              <Input
                id="name"
                size="lg"
                value={datosPaciente.name ?? ""}
                onChange={(e) =>
                  actualizarDatosPaciente({ name: e.target.value })
                }
                variant="underlined"
              />
            </div>
          </div>
          <div className="grid grid-cols-2 gap-4">
            <div>
              <label
                htmlFor="lastName"
                className="block text-sm font-medium text-gray-700"
              >
                Apellido del paciente
              </label>
              <Input
                id="lastName"
                size="lg"
                value={datosPaciente.lastName ?? ""}
                onChange={(e) =>
                  actualizarDatosPaciente({ lastName: e.target.value })
                }
                variant="underlined"
              />
            </div>
            <div>
              <label
                htmlFor="address"
                className="block text-sm font-medium text-gray-700"
              >
                Dirección
              </label>
              <Input
                id="address"
                size="lg"
                value={datosPaciente.address ?? ""}
                onChange={(e) =>
                  actualizarDatosPaciente({ address: e.target.value })
                }
                variant="underlined"
              />
            </div>
          </div>
          <div className="grid grid-cols-2 gap-4">
            <div>
              <label
                htmlFor="gender"
                className="block text-sm font-medium text-gray-700"
              >
                Género
              </label>
              <Select
                id="gender"
                items={genderOptions}
                placeholder="Seleccione una opción"
                size="lg"
                value={datosPaciente.gender ?? ""}
                onChange={(e) =>
                  actualizarDatosPaciente({
                    gender: e.target.value as PatientGender,
                  })
                }
                variant="underlined"
              >
                {genderOptions.map((item) => (
                  <SelectItem key={item.value} value={item.value}>
                    {item.label}
                  </SelectItem>
                ))}
              </Select>
            </div>
            <div>
              <label
                htmlFor="phone"
                className="block text-sm font-medium text-gray-700"
              >
                N° de celular
              </label>
              <Input
                id="phone"
                size="lg"
                value={datosPaciente.phone ?? ""}
                onChange={(e) =>
                  actualizarDatosPaciente({ phone: e.target.value })
                }
                variant="underlined"
              />
            </div>
          </div>
          <div className="grid grid-cols-2 gap-4">
            <div>
              <label
                htmlFor="birthdate"
                className="block text-sm font-medium text-gray-700"
              >
                Fecha de nacimiento
              </label>
              <Input
                type="date"
                variant="underlined"
                size="lg"
                value={
                  datosPaciente.birthdate
                    ? datosPaciente.birthdate.split("/").join("-")
                    : ""
                }
                onChange={handleBirthdateChange}
                // Asegurarse de que el formato de entrada sea yyyy-mm-dd
                placeholder="yyyy/mm/dd"
              />
            </div>
            <div>
              <label
                htmlFor="forecast"
                className="block text-sm font-medium text-gray-700"
              >
                Previsión
              </label>
              <Select
                id="forecast"
                items={forecastOptions}
                placeholder="Seleccione una opción"
                size="lg"
                value={datosPaciente.forecast ?? ""}
                onChange={(e) =>
                  actualizarDatosPaciente({
                    forecast: e.target.value as PatientForecast,
                  })
                }
                variant="underlined"
              >
                {forecastOptions.map((item) => (
                  <SelectItem key={item.value} value={item.value}>
                    {item.label}
                  </SelectItem>
                ))}
              </Select>
            </div>
          </div>
        </div>
      </div>

      <div className="flex justify-center mt-10">
        <Button color="primary" variant="shadow" onClick={siguientepaso}>
          Siguiente
        </Button>
      </div>
    </div>
  );
};

export default DatosPersonales;
