import {nextui} from '@nextui-org/theme'

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
    './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}'
  ],
  theme: {
    extend: {
      colors:{
        custom:{
          mayablue:"#5fbff9",
          magnolia:"#efe9f4",
          neonblue:"#5863f8",
          blackolive:"#423e37",
          richblack:"#121a2b",
          azure:"#CDDFDB",
          newrichblack:"#020F25",
        }
      }
    },
  },
  darkMode: "class",
    plugins: [
    nextui({
      themes: {
        light: {
          colors: {
            background: "#FFFFFF", // or DEFAULT
            foreground: "#11181C", // or 50 to 900 DEFAULT
            primary: {
              //... 50 to 900
              foreground: "#FFFFFF",
              DEFAULT: "#006FEE",
            },
            // ... rest of the colors
          },
        },
        dark: {
          colors: {
            background: "#000000", // or DEFAULT
            foreground: "#ECEDEE", // or 50 to 900 DEFAULT
            primary: {
              //... 50 to 900
              foreground: "#FFFFFF",
              DEFAULT: "#006FEE",
            },
            default: {
              foreground: "#FFFFFF",            
              DEFAULT: "#45D483",
            }
            
          },
          // ... rest of the colors
        },
        mytheme: {
  extend: "dark",
  colors: {
    background: '#F0F7FF',
    primary: {
      DEFAULT: '#3F8F8F',
      foreground: '#333333', // Texto para componentes primarios
    },
    secondary: '#57B9BB',
    text: '#333333',
    card: '#ffffff',
    success: '#28a745',
    warning: '#ffc107',
    danger: '#dc3545',
    info: '#17a2b8',
    foreground: '#333333', // Color de texto general
    focus: '#3F8F8F', // Color para elementos enfocados
  },
  fonts: {
    body: 'Inter, sans-serif',
    heading: 'Merriweather, serif',
  },
  boxShadow: {
    md: '0 4px 6px -1px rgba(0,0,0,0.1)',
  },
  borderRadius: {
    lg: '0.375rem',
  },
  spacing: {
    4: '1rem',
    8: '2rem',
    16: '4rem',
  },
  fontSize: {
    lg: '1.125rem',
  },
  lineHeight: {
    snug: '1.375',
  },
},
      },
    }),
  ],
}
