"use client";
import { Tabs, Tab, Card, CardBody } from "@nextui-org/react";
import TodayAppointments from "@/components/medic/appoinment/TodayAppointments";
import CalendarAppointments from "@/components/medic/appoinment/CalendarAppointments";
import ManageAppointments from "@/components/medic/appoinment/ManageAppointments";
import { use, useEffect, useState } from "react";
import { getClient } from "@/config/graphql/apolloClient";
import {
  GET_DAILY_APPOINTMENTS,
  GET_MEDIC,
} from "@/config/graphql/mutations/serviceMedic/service";
import { Medic } from "@/config/Interface/medic";
import useUserStore from "@/config/storage/user";
import withAuth from "@/components/utils/withAuth";
import LoadingBar from "@/components/utils/loadingBar";

const AppointmentsPage = () => {
  const [appoinmentData, setAppoinmentData] = useState<Medic | null>(null);
  const medicId = useUserStore((state) => state.id);
  const [loading, setLoading] = useState(true);
  const handleRequest = async () => {
    try {
      const { data } = await getClient().query({
        query: GET_DAILY_APPOINTMENTS,
        variables: { id: medicId }, //ir a buscar al storage el id
      });
      setAppoinmentData(data.medic);
    } catch (e) {
      console.log(e);
    } finally {
      setLoading(false);
    }
  };
  useEffect(() => {
    handleRequest();
  }, []);

  if (loading) {
    return (
      <div>
        <LoadingBar isLoading={loading} />
      </div>
    );
  }

  return (
    <div className="flex w-full flex-col">
      <div className="w-full">
        <Tabs
          aria-label="Profile Tabs"
          size="lg"
          radius="md"
          className="w-full mb-4"
          color="primary"
          classNames={{
            tabList: "bg-card",
            cursor: "bg-secondary",
            tab: "text-text data-[selected=true]:text-primary",
          }}
        >
          <Tab key="appointmentsToday" title="Citas de hoy">
            <Card className="mt-4 bg-card shadow-md">
              <CardBody className="pt-4">
                <TodayAppointments appoinmentData={appoinmentData} />
              </CardBody>
            </Card>
          </Tab>
          <Tab key="appointmentsCalendar" title="Calendario de citas">
            <CalendarAppointments appoinmentData={appoinmentData} />
          </Tab>
          <Tab key="manageAppointments" title="Gestionar citas">
            <Card className="mt-4 bg-card shadow-md">
              <CardBody className="pt-4">
                <ManageAppointments appoinmentData={appoinmentData} />
              </CardBody>
            </Card>
          </Tab>
        </Tabs>
      </div>
    </div>
  );
};

export default withAuth(AppointmentsPage, ["Médico"]);
