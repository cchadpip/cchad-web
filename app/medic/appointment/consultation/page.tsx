"use client";
import usePacientStore from "@/config/storage/pacient";
import { getClient } from "@/config/graphql/apolloClient";
import {
  COMPLETE_APPOINTMENT,
  GET_PATIENT_DATA,
  UPDATE_MEDICAL_RECORD,
} from "@/config/graphql/mutations/serviceMedic/service";
import { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardHeader,
  Input,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  Textarea,
  useDisclosure,
} from "@nextui-org/react";
import { differenceInYears, parseISO } from "date-fns";
import Swal from "sweetalert2";
import withAuth from "@/components/utils/withAuth";
import LoadingBar from "@/components/utils/loadingBar";

const ConsultationPage = () => {
  const ids = usePacientStore((state) => state.id);
  const idConsultation = usePacientStore((state) => state.idConsultation);
  const removePacient = usePacientStore((state) => state.clearPacient);
  const [patientData, setPatientData] = useState<any>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [appointment, setAppointment] = useState<any>(null);
  console.log("id");
  console.log(ids);
  console.log("idConsultation");
  console.log(idConsultation);
  const handleRequest = async () => {
    try {
      const { data } = await getClient().query({
        query: GET_PATIENT_DATA,
        variables: { id: ids }, //ir a buscar al storage el id
      });
      setPatientData(data.patient);
      setAppointment(data.patient.appointments);
    } catch (e) {
      setError(e as any);
    } finally {
      setLoading(false);
    }
  };
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  const [temperature, setTemperature] = useState("98.6");
  const [bloodPressure, setBloodPressure] = useState("120/80");
  const [heartRate, setHeartRate] = useState("72");
  const [respiratoryRate, setRespiratoryRate] = useState("16");
  const [showMedicalHistory, setShowMedicalHistory] = useState(false);
  const [diagnosis, setDiagnosis] = useState("");
  const [treatment, setTreatment] = useState("");
  const [medication, setMedication] = useState("");

  useEffect(() => {
    handleRequest();
  }, []); //arreglar con el id luego

  const calculateAge = (birthdate) => {
    if (!birthdate) return null;
    const birthDate = parseISO(birthdate);
    return differenceInYears(new Date(), birthDate);
  };

  if (loading)
    return (
      <div>
        <LoadingBar isLoading={loading} />
      </div>
    );
  if (error) return <div>Error: {error}</div>;
  console.log(patientData);
  console.log(appointment);
  const completedAppointments = patientData?.appointments.filter(
    (appointment) => appointment.state === "Completada"
  );

  const handleUpdateRecord = async () => {
    try {
      const { data } = await getClient().mutate({
        mutation: UPDATE_MEDICAL_RECORD,
        variables: {
          id: idConsultation,
          input: {
            diagnosis,
            treatment,
            prescriptionDrugs: medication,
          },
        },
      });
      console.log("Record updated:", data.updateMedicalRecord);
      Swal.fire({
        icon: "success",
        title: "Historial medico Actualizado!",
        text: "El historial medico ha sido actualizado satisfactoriamente",
        timer: 2000,
        showConfirmButton: false,
      });
    } catch (e) {
      console.error("Error updating record:", e);
      Swal.fire({
        icon: "error",
        title: "Error al actualizar el historial medico",
        text: "Hubo un error al actualizar el historial medico",
        timer: 2000,
        showConfirmButton: false,
      });
    }
  };

  const handleFinishConsultation = async () => {
    try {
      const { data } = await getClient().mutate({
        mutation: COMPLETE_APPOINTMENT,
        variables: {
          id: idConsultation,
        },
      });
      console.log("Consultation finished:", data.completeAppointment);
      Swal.fire({
        icon: "success",
        title: "Consulta Finalizada!",
        text: "La consulta ha sido finalizada satisfactoriamente",
        timer: 2000,
        showConfirmButton: false,
      });
      removePacient();
      window.location.href = "/medic/appointment";
    } catch (e) {
      console.error("Error finishing consultation:", e);
      Swal.fire({
        icon: "error",
        title: "Error al finalizar la consulta",
        text: "Hubo un error al finalizar la consulta",
        timer: 2000,
        showConfirmButton: false,
      });
    }
  };

  return (
    <div className="flex flex-col h-full bg-gray-50">
      <header className="bg-primary text-white py-4 px-6">
        <h1 className="text-2xl font-bold">Cita medica </h1>
      </header>
      <main className="flex-1 p-6 space-y-6">
        <div className="grid grid-cols-2 gap-6">
          <div>
            <h2 className="text-lg font-medium mb-2 text-background">
              Informacion del Paciente
            </h2>
            <div className="bg-white rounded-lg shadow p-4 space-y-2 text-text">
              <div>
                <span className="font-medium">Paciente:</span>{" "}
                {patientData?.name || "N/A"}
              </div>
              <div>
                <span className="font-medium">Edad:</span>{" "}
                {calculateAge(patientData?.birthdate) || "N/A"}
              </div>
              <div>
                <span className="font-medium">Genero:</span>{" "}
                {patientData?.gender || "N/A"}
              </div>
              <div>
                <span className="font-medium">Rut:</span>{" "}
                {patientData?.rut || "N/A"}
              </div>
              <div>
                <span className="font-medium">Prevision:</span>{" "}
                {patientData?.forecast || "N/A"}
              </div>
              <div>
                <span className="font-medium">email:</span>{" "}
                {patientData?.email || "N/A"}
              </div>
              <div>
                <span className="font-medium">Historial Medico:</span>
                <Button
                  color="primary"
                  variant="bordered"
                  size="sm"
                  className="ml-2"
                  onPress={onOpen}
                >
                  Ver
                </Button>
              </div>
            </div>
          </div>
          <div>
            <h2 className="text-lg font-medium mb-2 text-background">
              Signos Vitales
            </h2>
            <div className="bg-white rounded-lg shadow p-4 space-y-2 text-background">
              <div className="text-text">
                <span className="font-medium">Temperatura:</span>
                <Input
                  type="number"
                  value={temperature}
                  onChange={(e) => setTemperature(e.target.value)}
                  className="w-20 ml-2"
                />
                °F
              </div>
              <div className="text-text">
                <span className="font-medium">Presion Sanguinea:</span>
                <Input
                  value={bloodPressure}
                  onChange={(e) => setBloodPressure(e.target.value)}
                  className="w-32 ml-2"
                />
                mmHg
              </div>
              <div className="text-text">
                <span className="font-medium">Pulso Cardiaco:</span>
                <Input
                  type="number"
                  value={heartRate}
                  onChange={(e) => setHeartRate(e.target.value)}
                  className="w-20 ml-2"
                />
                bpm
              </div>
              <div className="text-text">
                <span className="font-medium text-text">
                  Velocidad de respiracion:
                </span>
                <Input
                  type="number"
                  value={respiratoryRate}
                  onChange={(e) => setRespiratoryRate(e.target.value)}
                  className="w-20 ml-2"
                />
                r/m
              </div>
            </div>
          </div>
        </div>
        <div>
          <h2 className="text-lg font-medium mb-2 text-text">
            Diagnostico y tratamiento
          </h2>
          <div className="bg-white rounded-lg shadow p-4 space-y-4">
            <div>
              <label
                htmlFor="diagnosis"
                className="block font-medium text-text m-2"
              >
                Diagnositco
              </label>
              <Textarea
                variant="bordered"
                id="diagnosis"
                rows={3}
                placeholder="Enter diagnosis"
                className="bg-gray-200"
                value={diagnosis}
                onChange={(e) => setDiagnosis(e.target.value)}
              />
            </div>
            <div>
              <label
                htmlFor="treatment"
                className="block font-medium text-text m-2"
              >
                Tratamiento
              </label>
              <Textarea
                variant="bordered"
                id="treatment"
                rows={3}
                placeholder="Enter treatment"
                className="bg-gray-200"
                value={treatment}
                onChange={(e) => setTreatment(e.target.value)}
              />
            </div>
            <div>
              <label
                htmlFor="medication"
                className="block font-medium text-text m-2"
              >
                Medicacion
              </label>
              <Textarea
                variant="bordered"
                id="medication"
                rows={3}
                placeholder="Enter medication"
                className="bg-gray-200"
                value={medication}
                onChange={(e) => setMedication(e.target.value)}
              />
            </div>
          </div>
        </div>
      </main>
      <footer className="bg-background py-4 px-6 flex justify-end space-x-2">
        <Button onPress={handleUpdateRecord} color="primary" variant="bordered">
          Actualizar Historial
        </Button>
        <Button onPress={handleFinishConsultation} color="secondary">
          {" "}
          Terminar Consulta
        </Button>
      </footer>
      <Modal isOpen={isOpen} onOpenChange={onOpenChange} className="text-white">
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col gap-1">
                Historial medico
              </ModalHeader>
              <ModalBody>
                {completedAppointments?.length > 0 ? (
                  completedAppointments.map((appointment, index) => (
                    <div key={index} className="border rounded-lg p-4 mb-4">
                      <h3 className="text-lg font-medium mb-2">
                        Cita {index + 1}
                      </h3>
                      <p>
                        <strong>Diagnostico:</strong>{" "}
                        {appointment.diagnosis || "N/A"}
                      </p>
                      <p>
                        <strong>Tratamiento:</strong>{" "}
                        {appointment.treatment || "N/A"}
                      </p>
                      <p>
                        <strong>Medicacion:</strong>{" "}
                        {appointment.prescriptionDrugs || "N/A"}
                      </p>
                      <p>
                        <strong>Tipo de cita:</strong>{" "}
                        {appointment.type || "N/A"}
                      </p>
                    </div>
                  ))
                ) : (
                  <p>No completed appointments found.</p>
                )}
              </ModalBody>
              <ModalFooter>
                <Button color="danger" variant="light" onPress={onClose}>
                  Cerrar
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </div>
  );
};

export default withAuth(ConsultationPage, ["Médico"]);
