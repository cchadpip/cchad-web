"use client";

import { useState, useEffect, SetStateAction } from "react";
import { Card, CardBody } from "@nextui-org/react";
import WorkScheduleCalendar from "@/components/medic/WorkScheduleCalendar";
import { GET_MEDIC_SCHEDULE } from "@/config/graphql/mutations/serviceMedic/service";
import { getClient } from "@/config/graphql/apolloClient";
import useUserStore from "@/config/storage/user";
import withAuth from "@/components/utils/withAuth";
import LoadingBar from "@/components/utils/loadingBar";

const WorkSchedulePage = () => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [events, setEvents] = useState([]);
  const medicId = useUserStore((state) => state.id);

  const handleRequest = async () => {
    console.log("handleRequest called");
    try {
      const client = getClient();
      const { data } = await client.query({
        query: GET_MEDIC_SCHEDULE,
        variables: { id: medicId }, // Reemplaza con el ID del médico actual obtenido del storage si es necesario
      });

      const publicSchedules = data.medic.schedules.filter(
        (schedule) => schedule.public
      );
      const scheduleEvents = publicSchedules.flatMap((schedule) =>
        schedule.slots.map((slot) => {
          const [start, end] = JSON.parse(
            slot.time.replace("(", "[").replace(")", "]")
          );
          return {
            id: slot.id,
            title: `Trabajo en ${schedule.box.name} (${schedule.box.branch.name})`,
            start: new Date(start),
            end: new Date(end),
          };
        })
      );

      setEvents(scheduleEvents);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching data:", error);
      setError(error as SetStateAction<null>);
      setLoading(false);
    }
  };

  useEffect(() => {
    console.log("useEffect called");
    handleRequest();
  }, []);

  if (loading) {
    return (
      <div>
        <LoadingBar isLoading={loading} />
      </div>
    );
  }

  if (error) {
    return <div>Error</div>;
  }

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-4xl font-bold mb-4 text-center text-gray-800">
        Horario de Trabajo
      </h1>
      <Card className="bg-white p-6 rounded-lg shadow-md">
        <CardBody>
          <WorkScheduleCalendar events={events} />
        </CardBody>
      </Card>
    </div>
  );
};

export default withAuth(WorkSchedulePage, ["Médico"]);
