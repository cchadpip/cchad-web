"use client";
import { useEffect, useState } from "react";
import { Tabs, Tab, Card, CardBody, CardHeader } from "@nextui-org/react";
import ProfileDetails from "@/components/medic/profile/profileDetails";
import AppointmentHistory from "@/components/medic/profile/appoinmentHistory";
import { getClient } from "@/config/graphql/apolloClient";
import { GET_MEDIC } from "@/config/graphql/mutations/serviceMedic/service";
import useUserStore from "@/config/storage/user"; // Importa tu tienda de Zustand
import withAuth from "@/components/utils/withAuth";
import LoadingBar from "@/components/utils/loadingBar";

export const ProfilePage = () => {
  const [medicData, setMedicData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  // Obtén el ID del médico desde Zustand
  const medicId = useUserStore((state) => state.id);

  const handleRequest = async () => {
    try {
      const { data } = await getClient().query({
        query: GET_MEDIC,
        variables: { id: medicId }, // Usar el ID del médico desde Zustand
      });
      console.log("data");
      console.log(data);
      setMedicData(data.medic);
      setLoading(false);
    } catch (e: any) {
      setError(e);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (medicId) {
      handleRequest();
    } else {
      setLoading(false);
      setError(null); // Update the setError function call to pass null instead of the string "No Medic ID found"
    }
  }, [medicId]);

  if (loading) {
    return (
      <div className="flex justify-center items-center">
        <LoadingBar isLoading={loading} />
      </div>
    );
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4 text-text">Mi Perfil</h1>
      <div className="w-full">
        <Tabs
          aria-label="Profile Tabs"
          size="lg"
          radius="md"
          className="w-full mb-4"
          color="primary"
          classNames={{
            tabList: "bg-card",
            cursor: "bg-secondary",
            tab: "text-text data-[selected=true]:text-primary",
          }}
        >
          <Tab key="profile" title="Datos Personales" className="w-full">
            <Card className="mt-4 bg-card shadow-md">
              <CardHeader className="border-b border-primary/20 pb-2">
                <h2 className="text-lg font-semibold text-primary">
                  Datos Personales
                </h2>
              </CardHeader>
              <CardBody className="pt-4">
                {medicData ? (
                  <ProfileDetails userData={medicData} />
                ) : (
                  <div className="text-text">No data available</div>
                )}
              </CardBody>
            </Card>
          </Tab>
          <Tab key="appointments" title="Historial de Citas">
            {medicData ? (
              <AppointmentHistory userData={medicData} />
            ) : (
              <div>No data available</div>
            )}
          </Tab>
        </Tabs>
      </div>
    </div>
  );
};
export default withAuth(ProfilePage, ["Médico"]);
