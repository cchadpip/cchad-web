"use client";
import { useLogic } from "@/components/secretary/manageAppointmentByPatientComponents/useLogic";
import { useState, useEffect } from "react";
import Swal from "sweetalert2";
import { Card, CardBody, CardHeader } from "@nextui-org/react";
import { format } from "date-fns";
import { es } from "date-fns/locale";
import withAuth from "@/components/utils/withAuth";

const AppointmentByPatient = () => {
  const { patients, fetchPatient, patient } = useLogic();
  const [selectedPatient, setSelectedPatient] = useState<string | null>(null);

  const handleSearch = () => {
    if (selectedPatient) {
      fetchPatient(Number(selectedPatient));
    } else {
      Swal.fire("Error", "Seleccione un paciente para buscar citas.", "error");
    }
  };

  return (
    <div>
      <h1 className="text-2xl font-semibold mb-6 text-black">
        Citas por Paciente
      </h1>
      <label htmlFor="patient" className="block font-medium text-black mb-2">
        Paciente
      </label>
      <select
        id="patient"
        value={selectedPatient ?? ""}
        onChange={(e) => setSelectedPatient(e.target.value)}
        className="w-full mt-1 p-2 border rounded mb-4"
      >
        <option value="" disabled>
          Seleccione un Paciente
        </option>
        {patients.map((patient) => (
          <option key={patient.id} value={String(patient.id)}>
            {patient.name} {patient.lastName}
          </option>
        ))}
      </select>
      <button
        onClick={handleSearch}
        className="w-full mt-4 bg-blue-500 text-white p-2 rounded"
      >
        Buscar
      </button>

      {patient && (
        <div className="mt-6">
          <h2 className="text-xl font-semibold mb-4 text-black">
            Citas de {patient.name} {patient.lastName}
          </h2>
          {patient.appointments.length === 0 ? (
            <p className="text-black">No hay citas para este paciente.</p>
          ) : (
            patient.appointments.map((appointment) => (
              <Card key={appointment.id} className="mb-6 bg-white shadow-lg">
                <CardHeader className="bg-blue-100 py-3 px-4 rounded-t-lg">
                  <h3 className="font-semibold text-xl text-black">
                    Detalles de la Cita
                  </h3>
                </CardHeader>
                <CardBody className="p-4">
                  <div className="text-black">
                    <p className="mb-1">
                      <span className="font-semibold">Fecha: </span>
                      {format(
                        new Date(JSON.parse(appointment.slot.time)[0]),
                        "PPPP",
                        { locale: es }
                      )}
                    </p>
                    <p className="mb-1">
                      <span className="font-semibold">Hora: </span>
                      {format(
                        new Date(JSON.parse(appointment.slot.time)[0]),
                        "p",
                        { locale: es }
                      )}
                    </p>
                    <p className="mb-1">
                      <span className="font-semibold">Médico: </span>
                      {appointment.slot.schedule.medic.name}
                    </p>
                    <p className="mb-1">
                      <span className="font-semibold">Especialidad: </span>
                      {appointment.slot.schedule.medic.specialty}
                    </p>
                    <p className="mb-1">
                      <span className="font-semibold">Estado: </span>
                      {appointment.state}
                    </p>
                    <p className="mb-1">
                      <span className="font-semibold">Confirmada: </span>
                      {appointment.confirmed ? "Sí" : "No"}
                    </p>
                    <p className="mb-1">
                      <span className="font-semibold">Tipo: </span>
                      {appointment.type}
                    </p>
                    <p className="mb-1">
                      <span className="font-semibold">Diagnóstico: </span>
                      {appointment.diagnosis || "N/A"}
                    </p>
                    <p className="mb-1">
                      <span className="font-semibold">Tratamiento: </span>
                      {appointment.treatment || "N/A"}
                    </p>
                    <p className="mb-1">
                      <span className="font-semibold">
                        Medicamentos Recetados:{" "}
                      </span>
                      {appointment.prescriptionDrugs || "N/A"}
                    </p>
                  </div>
                </CardBody>
              </Card>
            ))
          )}
        </div>
      )}
    </div>
  );
}
export default withAuth(AppointmentByPatient, ["Secretaria"]);