"use client";
import React, { useState, useRef } from "react";
import {
  Select,
  SelectItem,
  Button,
  Card,
  CircularProgress,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  ModalContent,
} from "@nextui-org/react";
import MyDatePickerComponent from "@/components/secretary/manageSchedulesComponents/panelComponents/timePickerComponent";
import { useMedicScheduleLogic } from "@/components/secretary/manageSchedulesComponents/logicSchedules/useLogicSchedules";
import SecretaryCalendarView from "@/components/secretary/manageSchedulesComponents/calendarComponents/secretaryCalendar";
import SchedulePanel from "@/components/secretary/manageSchedulesComponents/panelComponents/schedulePanel";
import BoxAvailability from "@/components/secretary/manageSchedulesComponents/calendarComponents/boxAvailability/boxAvailability";
import Swal from "sweetalert2";
import { addDays, format, set } from "date-fns";
import LoadingModal from "@/components/utils/LoadingModal";
import moment from "moment";
import LoadingBar from "@/components/utils/loadingBar";
import withAuth from "@/components/utils/withAuth";

const ManageSchedules: React.FC = () => {
  const {
    medics,
    selectedMedic,
    setSelectedMedic,
    generateCalendarEvents,
    handleCreateSchedule,
    activeDays,
    setActiveDays,
    selectedBox,
    setSelectedBox,
    boxes,
    generateCalendarBoxEvents,
    publicSchedule,
    fetchBox,
    fetchMedic,
  } = useMedicScheduleLogic();

  const [blockDuration, setBlockDuration] = useState<string>("01:00");
  const [startOfWeekDate, setStartOfWeekDate] = useState<Date | null>(null);
  const panelRef = useRef<HTMLDivElement>(null);
  const [isDatePickerOpen, setIsDatePickerOpen] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [hasUnsavedChanges, setHasUnsavedChanges] = useState<boolean>(false);
  const [isBoxAvailabilityModalOpen, setIsBoxAvailabilityModalOpen] =
    useState<boolean>(false);

  const onSelectBox = async (value: string) => {
    setIsLoading(true);
    try {
      const boxID = parseInt(value, 10);
      setSelectedBox(boxID);
      await fetchBox(boxID);
      setHasUnsavedChanges(true);
    } finally {
      setIsLoading(false);
    }
  };

  const onSelectMedic = async (value: string) => {
    setIsLoading(true);
    try {
      const medicId = parseInt(value, 10);
      setSelectedMedic(medicId);
      await fetchMedic(medicId);
      setHasUnsavedChanges(true);
    } finally {
      setIsLoading(false);
    }
  };

  const onSelectWeek = (startOfWeekDate: Date) => {
    setStartOfWeekDate(startOfWeekDate);
    if (panelRef.current) {
      panelRef.current.scrollIntoView({ behavior: "smooth" });
    }
    setHasUnsavedChanges(true);
  };

  const handleCalendarOpen = () => {
    setIsDatePickerOpen(true);
  };

  const handleCalendarClose = () => {
    setIsDatePickerOpen(false);
  };

  const getDayName = (date: Date) => {
    return format(date, "EEEE");
  };

  const getWeekDays = () => {
    if (!startOfWeekDate) return [];
    return Array.from({ length: 7 }).map((_, index) => {
      const day = addDays(startOfWeekDate, index);
      return { date: day, name: getDayName(day) };
    });
  };

  const onSaveDay = async (name: string, date: Date): Promise<void> => {
    if (selectedMedic === null) {
      Swal.fire("Error", "No se ha seleccionado un médico.", "error");
      return;
    }

    if (selectedBox === null) {
      Swal.fire("Error", "No se ha seleccionado una caja.", "error");
      return;
    }

    if (!activeDays[name] || activeDays[name].length === 0) {
      Swal.fire("Error", `No hay horarios activos para ${name}.`, "error");
      return;
    }

    for (const timeSlot of activeDays[name]) {
      const { startTime, endTime } = timeSlot;
      const startDateTime = new Date(date);
      startDateTime.setHours(
        parseInt(startTime.split(":")[0]),
        parseInt(startTime.split(":")[1]),
        0,
        0
      );

      const endDateTime = new Date(date);
      endDateTime.setHours(
        parseInt(endTime.split(":")[0]),
        parseInt(endTime.split(":")[1]),
        0,
        0
      );

      const timeRange = `${moment(startDateTime).format(
        "YYYY-MM-DD HH:mm:ss"
      )};${moment(endDateTime).format("YYYY-MM-DD HH:mm:ss")}`;

      const input = {
        time: timeRange,
        slotDuration: blockDuration,
        medicId: selectedMedic,
        boxId: selectedBox,
      };
      setIsLoading(true);
      try {
        await handleCreateSchedule(input);
        if (selectedMedic !== null) {
          await fetchMedic(selectedMedic);
        }
        setHasUnsavedChanges(false);
      } catch (error: any) {
        Swal.fire(
          "Error",
          error.message || "Error al crear el horario.",
          "error"
        );
      } finally {
        setIsLoading(false);
      }
    }
  };

  const handlePublicSchedule = async () => {
    try {
      await publicSchedule();
      Swal.fire("Éxito", "Horarios publicados correctamente.", "success");
      if (selectedMedic !== null) {
        await fetchMedic(selectedMedic);
      }
      setHasUnsavedChanges(false);
    } catch (error: any) {
      Swal.fire(
        "Error",
        error.message || "Error al publicar los horarios.",
        "error"
      );
    }
  };

  const handleRefreshCalendar = async () => {
    if (selectedMedic !== null) {
      setIsLoading(true);
      await fetchMedic(selectedMedic);
      setIsLoading(false);
      setHasUnsavedChanges(false);
    }
  };

  const handleBoxAvailabilityOpen = () => {
    setIsBoxAvailabilityModalOpen(true);
  };

  const handleBoxAvailabilityClose = () => {
    setIsBoxAvailabilityModalOpen(false);
  };

  return (
    <div className="text-black ">
      <LoadingBar isLoading={isLoading} /> {/* Barra de carga */}
      <Card className="bg-white px-6 py-4 shadow w-full mb-4 ">
        <div className="flex items-center justify-between">
          <h1 className="text-2xl font-semibold text-black">
            Administrar Horarios
          </h1>
          {isLoading && <CircularProgress />}
          <div className="flex gap-4">
            <Select
              placeholder="Profesional de la Salud"
              value={selectedMedic ? selectedMedic.toString() : ""}
              onChange={(e) => onSelectMedic(e.target.value)}
              variant="underlined"
              color="secondary"
              className="min-w-64"
            >
              {medics.map((medic) => (
                <SelectItem
                  className="text-white"
                  key={medic.id}
                  value={medic.id.toString()}
                >
                  {`${medic.name} ${medic.lastName} - ${medic.specialty}`}
                </SelectItem>
              ))}
            </Select>
            <Select
              placeholder="Selecciona un Box"
              value={selectedBox ? selectedBox.toString() : ""}
              onChange={(e) => onSelectBox(e.target.value)}
              variant="underlined"
              color="primary"
              className="min-w-64"
            >
              {boxes.map((box) => (
                <SelectItem
                  className="text-white"
                  key={box.id}
                  value={box.id.toString()}
                >
                  {`${box.name}`}
                </SelectItem>
              ))}
            </Select>
          </div>
        </div>
      </Card>
      <div className="flex gap-4">
        <Card className="w-1/3 bg-white shadow flex items-center">
          <div className="p-4 flex-col justify-center">
            <p className="text-lg font-bold text-center mb-4 text-black">
              Opciones de Horarios
            </p>
            <div className="ml-5">
              <MyDatePickerComponent
                onSelectWeek={onSelectWeek}
                onCalendarOpen={handleCalendarOpen}
                onCalendarClose={handleCalendarClose}
              />
            </div>
            <SchedulePanel
              onPublish={handlePublicSchedule}
              onSaveDay={onSaveDay}
              onCancel={() => {
                Swal.fire("Cancelado", "Cambios cancelados.", "info");
                setHasUnsavedChanges(false);
              }}
              activeDays={activeDays}
              setActiveDays={setActiveDays}
              blockDuration={blockDuration}
              setBlockDuration={setBlockDuration}
              weekDays={getWeekDays()}
            />
          </div>
        </Card>

        <Card className="w-2/3 bg-white shadow items-center justify-center">
          <div className="flex gap-4">
            <Button
              onPress={handleRefreshCalendar}
              color="primary"
              className="mb-4 min-w-72 mt-5"
            >
              Refrescar Calendario
            </Button>
            <Button
              onPress={handleBoxAvailabilityOpen}
              color="secondary"
              className="mb-4 min-w-72 mt-5"
            >
              Ver disponibilidad de box
            </Button>
          </div>
          <div className="p-4"></div>
          <SecretaryCalendarView
            events={generateCalendarEvents}
            fetchMedic={fetchMedic}
          />
        </Card>
      </div>
      <Modal
        isOpen={isBoxAvailabilityModalOpen}
        onClose={handleBoxAvailabilityClose}
      >
        <ModalContent>
          <ModalHeader>Disponibilidad de Box</ModalHeader>
          <ModalBody>
            <BoxAvailability events={generateCalendarBoxEvents} />
          </ModalBody>
          <ModalFooter>
            <Button
              variant="flat"
              color="warning"
              onPress={handleBoxAvailabilityClose}
            >
              Cerrar
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </div>
  );
};

export default withAuth(ManageSchedules, ["Secretaria"]);
