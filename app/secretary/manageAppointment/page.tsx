"use client";
import React, { useState, useEffect } from "react";
import {
  Button,
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  useDisclosure,
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Divider,
  Spinner,
} from "@nextui-org/react";
import useSecretaryAppointmentLogic from "@/components/appointmentComponents/logicAppointmentSecretary/useLogicAppointmentSecretary";
import { today, getLocalTimeZone } from "@internationalized/date";
import AppointmentCalendar from "@/components/secretary/manageAppointmentByPatientComponents/ReSchedulemodalComponent";
import withAuth from "@/components/utils/withAuth";

const Component = () => {
  const {
    medics,
    fetchMedics,
    fetchMedic,
    medic,
    confirmAppointment,
    cancelAppointment,
    rescheduleAppointment,
    appointments,
    filterSlotsByDate,
    availableSlots,
  } = useSecretaryAppointmentLogic();

  const { isOpen, onOpen, onClose } = useDisclosure();
  const [selectedDateForReschedule, setSelectedDateForReschedule] = useState(
    today(getLocalTimeZone())
  );
  const [selectedAppointmentId, setSelectedAppointmentId] = useState<
    number | null
  >(null);
  const [selectedMedicId, setSelectedMedicId] = useState<number | null>(null);
  const [loadingDoctorId, setLoadingDoctorId] = useState<number | null>(null);
  const [updatedAppointments, setUpdatedAppointments] = useState<any[]>([]);

  const handleDoctorSelect = async (doctor) => {
    setLoadingDoctorId(doctor.id);
    await fetchMedic(doctor.id);
    setSelectedMedicId(doctor.id);
    setSelectedAppointmentId(null);
    setLoadingDoctorId(null);
  };

  const handleConfirmAppointment = async (appointment) => {
    await confirmAppointment(appointment.id);
    setUpdatedAppointments((prev) =>
      prev.map((a) => (a.id === appointment.id ? { ...a, confirmed: true } : a))
    );
  };

  const handleCancelAppointment = async (appointment) => {
    await cancelAppointment(appointment.id);
    if (selectedMedicId !== null) {
      fetchMedic(selectedMedicId);
    }
  };

  const handleOpenRescheduleModal = (appointmentId) => {
    setSelectedAppointmentId(appointmentId);
    onOpen();
  };

  const handleSlotSelect = async (slotId) => {
    if (selectedAppointmentId) {
      try {
        await rescheduleAppointment(selectedAppointmentId, slotId);
        onClose();
      } catch (error) {
        console.error("Error al reprogramar cita:", error);
      }
    }
  };

  useEffect(() => {
    fetchMedics();
  }, []);

  useEffect(() => {
    if (medic && appointments) {
      setUpdatedAppointments(appointments);
    }
  }, [medic, appointments]);

  return (
    <div className="flex flex-col h-screen">
      <style jsx>{`
        .bg-primary {
          background-color: #3f8f8f;
        }
        .text-primary-foreground {
          color: #ffffff;
        }
        .bg-card {
          background-color: #ffffff;
        }
        .bg-muted {
          background-color: #f0f7ff;
        }
        .text-muted {
          color: #333333;
        }
      `}</style>
      <header className="bg-primary text-primary-foreground py-4 px-6">
        <h1 className="text-2xl font-bold">Detalles de citas</h1>
      </header>
      <main className="flex-1 p-6">
        <div className="grid grid-cols-3 gap-6">
          <Card className="col-span-1 bg-card rounded-lg shadow-md">
            <CardHeader className="text-xl font-bold text-muted text-black">
              Doctores
            </CardHeader>
            <CardBody className="space-y-2 text-black">
              {medics.map((doctor) => (
                <Button
                  key={doctor.id}
                  onClick={() => handleDoctorSelect(doctor)}
                  variant="bordered"
                  color="primary"
                  isLoading={loadingDoctorId === doctor.id}
                  className={`w-full text-left ${
                    medic && medic.id === doctor.id
                      ? "bg-primary text-primary-foreground"
                      : "bg-card text-muted hover:bg-muted"
                  }`}
                >
                  {doctor.name}
                </Button>
              ))}
            </CardBody>
          </Card>
          <Card className="col-span-2 bg-card rounded-lg shadow-md">
            <CardHeader className="text-xl font-bold text-muted mb-4 text-black">
              {medic ? `Citas agendadas ${medic.name}` : "Seleccione un doctor"}
            </CardHeader>
            <CardBody className="space-y-4">
              {medic && updatedAppointments ? (
                updatedAppointments.map((appointment) => (
                  <div
                    key={appointment.id}
                    className="bg-muted rounded-md p-4 flex justify-between items-center"
                  >
                    <div>
                      <p className="font-medium text-muted">
                        {new Date(appointment.time).toLocaleString("es-ES", {
                          weekday: "short",
                          month: "short",
                          day: "numeric",
                          hour: "numeric",
                          minute: "numeric",
                        })}
                      </p>
                      <p
                        className={`font-medium ${
                          appointment.status === "Confirmed"
                            ? "text-success"
                            : appointment.status === "Cancelled"
                            ? "text-danger"
                            : "text-warning"
                        }`}
                      >
                        {appointment.status === "Confirmed"
                          ? "Estado: Confirmado"
                          : appointment.status}
                      </p>
                      {appointment.confirmed && (
                        <p className="font-medium text-success">
                          Estado: Confirmado
                        </p>
                      )}
                      <p className="text-muted">
                        Paciente: {appointment.patient.name}{" "}
                        {appointment.patient.lastName}
                      </p>
                    </div>
                    <div>
                      <Button
                        color="success"
                        onClick={() => handleConfirmAppointment(appointment)}
                        className="mr-2"
                      >
                        Confirmar
                      </Button>
                      <Button
                        color="danger"
                        onClick={() => handleCancelAppointment(appointment)}
                        className="mr-2"
                      >
                        Cancelar
                      </Button>
                      <Button
                        color="primary"
                        onClick={() =>
                          handleOpenRescheduleModal(appointment.id)
                        }
                        className="mr-2"
                      >
                        Reprogramar
                      </Button>
                    </div>
                  </div>
                ))
              ) : (
                <p className="text-muted">
                  Seleccione un doctor para ver las citas agendadas.
                </p>
              )}
            </CardBody>
          </Card>
        </div>
      </main>
      <Modal
        isOpen={isOpen}
        onOpenChange={onClose}
        aria-labelledby="reschedule-appointment-modal"
      >
        <ModalContent>
          {() => (
            <>
              <ModalHeader className="flex flex-col gap-1">
                Reprogramar Cita
              </ModalHeader>
              <ModalBody>
                <ModalBody>
                  <AppointmentCalendar
                    selectedMedicId={selectedMedicId}
                    appointmentId={selectedAppointmentId}
                    onUpdateAppointments={setUpdatedAppointments} // Asegúrate de pasar esta propiedad
                  />
                </ModalBody>
              </ModalBody>
              <ModalFooter>
                <Button color="danger" onClick={onClose}>
                  Cancelar
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </div>
  );
};

export default withAuth(Component, ["Secretaria"]);
