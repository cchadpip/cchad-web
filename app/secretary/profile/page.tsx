"use client";
import { useEffect, useState } from "react";
import { Tabs, Tab, Card, CardBody } from "@nextui-org/react";
import ProfileDetails from "@/components/patient/profileData/profileDetails";
import AppointmentHistory from "@/components/medic/profile/appoinmentHistory";
import { getClient } from "@/config/graphql/apolloClient";
import { GET_PATIENT } from "@/config/graphql/mutations/servicePatient/service";
import useUserStore from "@/config/storage/user"; // Importa tu tienda de Zustand
import withAuth from "@/components/utils/withAuth";
import LoadingBar from "@/components/utils/loadingBar";

export const ProfilePage = () => {
  const [secretaryData, setsecretaryData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  // Obtén el ID del médico desde Zustand
  const secretaryId = useUserStore((state) => state.id);
  console.log("secretaryId " + secretaryId);
  const handleRequest = async () => {
    try {
      const { data } = await getClient().query({
        query: GET_PATIENT,
        variables: { id: secretaryId }, // Usar el ID del médico desde Zustand
      });
      console.log("data");
      console.log(data);
      setsecretaryData(data.patient);
      setLoading(false);
    } catch (e: any) {
      setError(e);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (secretaryId) {
      handleRequest();
    } else {
      setLoading(false);
      setError(null); // Update the setError function call to pass null instead of the string "No Medic ID found"
    }
  }, [secretaryId]);

  if (loading) {
    return (
      <div>
        <LoadingBar isLoading={loading} />
      </div>
    );
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className="container mx-auto p-4 ">
      <h1 className="text-2xl font-bold mb-4 text-background">Mi Perfil</h1>
      <div className="w-full">
        <Tabs
          aria-label="Profile Tabs"
          size="lg"
          radius="md"
          className="w-full mb-4"
        >
          <Tab key="profile" title="Datos Personales" className="w-full">
            <Card className="mt-4">
              <CardBody>
                {secretaryData ? (
                  <ProfileDetails userData={secretaryData} />
                ) : (
                  <div>No data available</div>
                )}
              </CardBody>
            </Card>
          </Tab>
          <Tab key="appointments" title="Historial de Citas">
            <Card className="mt-4">
              <CardBody>proximamente</CardBody>
            </Card>
          </Tab>
        </Tabs>
      </div>
    </div>
  );
};

export default withAuth(ProfilePage, ["Secretaria"]);
