"use client";
import {
  BrainIcon,
  CalendarIcon,
  ClockIcon,
  HeartPulseIcon,
  TreesIcon,
  UserIcon,
} from "@/components/incons";
import useUserStore from "@/config/storage/user";
import Link from "next/link";
import Swal from "sweetalert2";
import { useEffect, useState } from "react";
import { CircularProgress } from "@nextui-org/react";
import LoadingBar from "@/components/utils/loadingBar";

export default function Home() {
  const types = useUserStore((state) => state.type);
  const tokens = useUserStore((state) => state.token);

  const [clientTypes, setClientTypes] = useState<string | null>(null);
  const [clientTokens, setClientTokens] = useState<string | null>(null);
  const [loading, setLoading] = useState(true);
  const [loadingButton, setLoadingButton] = useState(false);

  useEffect(() => {
    // Asignar el estado del cliente después de la hidratación
    setClientTypes(types);
    setClientTokens(tokens);
    setLoading(false);
  }, [types, tokens]);

  const handleClick = (href: string) => {
    setLoadingButton(true);
    window.location.href = href;
  };

  const redirectToAgendarCita = () => {
    if (clientTypes === "Paciente" && clientTokens !== null) {
      handleClick("/patient/appointment");
    } else {
      Swal.fire({
        title: "Error",
        text: "Debes iniciar sesión como paciente para agendar una cita.",
        icon: "error",
        confirmButtonText: "OK",
      });
    }
  };

  if (loading) {
    return (
      <div>
        <LoadingBar isLoading={loading} />
      </div>
    ); // Mostrar un estado de carga mientras se cargan los datos del cliente
  }

  if (clientTypes === "Secretaria" && clientTokens !== null) {
    return (
      <div className="flex flex-col min-h-[100vh]">
        <header className="bg-primary text-primary-foreground w-full">
          <div className="container mx-auto max-w-screen-2xl py-12 md:py-24 lg:py-32 flex justify-center items-center">
            <div className="text-center space-y-4">
              <h1 className="text-4xl font-bold">Bienvenida Secretaria</h1>
              <p className="text-lg">
                Administra citas, horarios y pacientes eficientemente.
              </p>
            </div>
          </div>
        </header>
        <section className="w-full py-12 md:py-24 lg:py-32">
          <div className="container px-4 md:px-6 grid gap-8 md:grid-cols-2 lg:grid-cols-3">
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <UserIcon className="h-12 w-12 text-primary" />
              <h2 className="text-2xl font-bold text-black">
                Registrar Paciente
              </h2>
              <p className="text-muted-foreground text-black">
                Registra un nuevo paciente en el sistema.
              </p>
              <button
                onClick={() =>
                  handleClick("/auth/Register/registerPatientBySecretary")
                }
                className="text-sm font-medium text-primary hover:underline"
                disabled={loadingButton}
              >
                {loadingButton ? (
                  <CircularProgress aria-label="Loading..." />
                ) : (
                  "Registrar Paciente"
                )}
              </button>
            </div>
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <ClockIcon className="h-12 w-12 text-primary" />
              <h2 className="text-2xl font-bold text-black">
                Horarios de trabajo
              </h2>
              <p className="text-muted-foreground text-black">
                Consulta y gestiona horarios de trabajo.
              </p>
              <button
                onClick={() => handleClick("/secretary/manageSchedules")}
                className="text-sm font-medium text-primary hover:underline"
                disabled={loadingButton}
              >
                {loadingButton ? (
                  <CircularProgress aria-label="Loading..." />
                ) : (
                  "Ver Horarios"
                )}
              </button>
            </div>
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <CalendarIcon className="h-12 w-12 text-primary" />
              <h2 className="text-2xl font-bold text-black">
                Citas programadas
              </h2>
              <p className="text-muted-foreground text-black">
                Ver y gestionar citas programadas.
              </p>
              <button
                onClick={() => handleClick("/secretary/manageAppointment")}
                className="text-sm font-medium text-primary hover:underline"
                disabled={loadingButton}
              >
                {loadingButton ? (
                  <CircularProgress aria-label="Loading..." />
                ) : (
                  "Ver Citas"
                )}
              </button>
            </div>
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <UserIcon className="h-12 w-12 text-primary" />
              <h2 className="text-2xl font-bold text-black">
                Registrar Médico
              </h2>
              <p className="text-muted-foreground text-black">
                Registra un nuevo médico en el sistema.
              </p>
              <button
                onClick={() =>
                  handleClick("/auth/Register/healthProfessionalRegister")
                }
                className="text-sm font-medium text-primary hover:underline"
                disabled={loadingButton}
              >
                {loadingButton ? (
                  <CircularProgress aria-label="Loading..." />
                ) : (
                  "Registrar Médico"
                )}
              </button>
            </div>
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <CalendarIcon className="h-12 w-12 text-primary" />
              <h2 className="text-2xl font-bold text-black">
                Agendar nueva Cita
              </h2>
              <p className="text-muted-foreground text-black">
                Agenda una nueva cita médica.
              </p>
              <button
                onClick={() => handleClick("/secretary/appointment")}
                className="text-sm font-medium text-primary hover:underline"
                disabled={loadingButton}
              >
                {loadingButton ? (
                  <CircularProgress aria-label="Loading..." />
                ) : (
                  "Agendar Cita"
                )}
              </button>
            </div>
          </div>
        </section>
      </div>
    );
  }

  if (clientTypes === "Médico" && clientTokens !== null) {
    return (
      <div className="flex flex-col min-h-[100dvh]">
        <header className="bg-primary text-primary-foreground w-full">
          <div className="container mx-auto max-w-screen-2x1 py-12 md:py-24 lg:py-32 flex justify-center items-center">
            <div className="text-center space-y-4">
              <h1 className="text-4xl font-bold text-primary-foreground">
                Bienvenido a Clinica Cchad
              </h1>
              <p className="text-lg text-primary-foreground">
                Brindar servicios médicos de primer nivel a nuestra comunidad.
              </p>
            </div>
          </div>
        </header>
        <section id="profile" className="w-full py-12 md:py-24 lg:py-32">
          <div className="container px-4 md:px-6 grid gap-8 md:grid-cols-2 lg:grid-cols-3">
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <UserIcon className="h-12 w-12 text-primary" />
              <h2 className="text-black text-2xl font-bold">Perfil</h2>
              <p className="text-black text-muted-foreground">
                Ver y administrar la información de su perfil personal.
              </p>
              <button
                onClick={() => handleClick("/medic/profile")}
                className="text-sm font-medium text-primary hover:underline"
                disabled={loadingButton}
              >
                {loadingButton ? (
                  <CircularProgress aria-label="Loading..." />
                ) : (
                  "Ir a Perfil"
                )}
              </button>
            </div>
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <ClockIcon className="h-12 w-12 text-primary" />
              <h2 className="text-black text-2xl font-bold">
                Horario de trabajo
              </h2>
              <p className="text-black text-muted-foreground">
                Consulta tus horarios de trabajo disponibles y programa citas.
              </p>
              <button
                onClick={() => handleClick("/medic/schedule")}
                className="text-sm font-medium text-primary hover:underline"
                disabled={loadingButton}
              >
                {loadingButton ? (
                  <CircularProgress aria-label="Loading..." />
                ) : (
                  "Ver Horarios"
                )}
              </button>
            </div>
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <CalendarIcon className="h-12 w-12 text-primary" />
              <h2 className="text-black text-2xl font-bold">
                Citas programadas
              </h2>
              <p className="text-black text-muted-foreground">
                Ver y gestionar citas médicas programadas.
              </p>
              <button
                onClick={() => handleClick("/medic/appointment")}
                className="text-sm font-medium text-primary hover:underline"
                disabled={loadingButton}
              >
                {loadingButton ? (
                  <CircularProgress aria-label="Loading..." />
                ) : (
                  "Ver Citas"
                )}
              </button>
            </div>
          </div>
        </section>
      </div>
    );
  }
  if (clientTypes === "Administrador" && clientTokens !== null) {
    return (
      <div className="flex flex-col min-h-screen">
        <header className="bg-primary text-primary-foreground w-full">
          <div className="container mx-auto max-w-screen-2xl py-12 md:py-24 lg:py-32 flex justify-center items-center">
            <div className="text-center space-y-4">
              <h1 className="text-4xl font-bold">Bienvenido Administrador</h1>
              <p className="text-lg">Administra funciones del sistema.</p>
            </div>
          </div>
        </header>
        <section className="w-full py-12 md:py-24 lg:py-32">
          <div className="container px-4 md:px-6 grid gap-8 md:grid-cols-2 lg:grid-cols-3">
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <UserIcon className="h-12 w-12 text-primary" />
              <h2 className="text-2xl font-bold text-black">
                Registrar Secretaria
              </h2>
              <p className="text-muted-foreground text-black">
                Registra un nuevo usuario tipo Secretaria en el sistema.
              </p>
              <Link
                href="/auth/Register/secretaryRegister"
                className="text-sm font-medium text-primary hover:underline"
                prefetch={false}
              >
                Registrar Secretaria
              </Link>
              {loading && <CircularProgress aria-label="Loading..." />}
            </div>
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <UserIcon className="h-12 w-12 text-primary" />
              <h2 className="text-2xl font-bold text-black">
                Registrar Administrador
              </h2>
              <p className="text-muted-foreground text-black">
                Registra un nuevo usuario tipo Administrador en el sistema.
              </p>
              <Link
                href="/auth/Register/adminRegister"
                className="text-sm font-medium text-primary hover:underline"
                prefetch={false}
              >
                Registrar Administrador
              </Link>
              {loading && <CircularProgress aria-label="Loading..." />}
            </div>
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <CalendarIcon className="h-12 w-12 text-primary" />
              <h2 className="text-2xl font-bold text-black">Crear Sucursal</h2>
              <p className="text-muted-foreground text-black">
                Crea una nueva sucursal en el sistema.
              </p>
              <Link
                href="/admin/manageBranches"
                className="text-sm font-medium text-primary hover:underline"
                prefetch={false}
              >
                Crear Sucursal
              </Link>
              {loading && <CircularProgress aria-label="Loading..." />}
            </div>
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <ClockIcon className="h-12 w-12 text-primary" />
              <h2 className="text-2xl font-bold text-black">Crear Box</h2>
              <p className="text-muted-foreground text-black">
                Crea un nuevo box de atención en el sistema.
              </p>
              <Link
                href="/admin/manageBoxes"
                className="text-sm font-medium text-primary hover:underline"
                prefetch={false}
              >
                Crear Box
              </Link>
              {loading && <CircularProgress aria-label="Loading..." />}
            </div>
          </div>
        </section>
      </div>
    );
  }

  if (clientTypes === "Paciente" && clientTokens !== null) {
    return (
      <div className="flex flex-col min-h-[100dvh]">
        <header className="bg-primary text-primary-foreground w-full">
          <div className="container mx-auto max-w-screen-2x1 py-12 md:py-24 lg:py-32 flex justify-center items-center">
            <div className="text-center space-y-4">
              <h1 className="text-4xl font-bold text-primary-foreground">
                Bienvenido a Clinica Cchad
              </h1>
              <p className="text-lg text-primary-foreground">
                Brindar servicios médicos de primer nivel a nuestra comunidad.
              </p>
            </div>
          </div>
        </header>
        <section id="profile" className="w-full py-12 md:py-24 lg:py-32">
          <div className="container px-4 md:px-6 grid gap-8 md:grid-cols-2 lg:grid-cols-3">
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <UserIcon className="h-12 w-12 text-primary" />
              <h2 className="text-black text-2xl font-bold">Perfil</h2>
              <p className="text-black text-muted-foreground">
                Ver y administrar la información de su perfil personal.
              </p>
              <button
                onClick={() => handleClick("/patient/profile")}
                className="text-sm font-medium text-primary hover:underline"
                disabled={loadingButton}
              >
                {loadingButton ? (
                  <CircularProgress aria-label="Loading..." />
                ) : (
                  "Ir a Perfil"
                )}
              </button>
            </div>
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <CalendarIcon className="h-12 w-12 text-primary" />
              <h2 className="text-black text-2xl font-bold">
                Agendar nueva Cita
              </h2>
              <p className="text-black text-muted-foreground">
                Programa una nueva cita médica en la clínica.
              </p>
              <button
                onClick={redirectToAgendarCita}
                className="text-sm font-medium text-primary hover:underline"
                disabled={loadingButton}
              >
                {loadingButton ? (
                  <CircularProgress aria-label="Loading..." />
                ) : (
                  "Agendar Cita"
                )}
              </button>
            </div>
            <div className="flex flex-col items-center text-center space-y-4 p-6 border border-gray-200 rounded-lg shadow-sm">
              <HeartPulseIcon className="h-12 w-12 text-primary" />
              <h2 className="text-black text-2xl font-bold">
                Mis citas programadas
              </h2>
              <p className="text-black text-muted-foreground">
                Consulta las citas médicas programadas.
              </p>
              <button
                onClick={() => handleClick("/patient/viewAppointment")}
                className="text-sm font-medium text-primary hover:underline"
                disabled={loadingButton}
              >
                {loadingButton ? (
                  <CircularProgress aria-label="Loading..." />
                ) : (
                  "Ver Citas"
                )}
              </button>
            </div>
          </div>
        </section>
      </div>
    );
  }
  return (
    <div className="min-h-screen bg-background text-text font-body">
      <header className="bg-primary text-card shadow-md p-4 flex justify-between items-center"></header>
      <div className="container mx-auto px-4 md:px-6 py-12">
        <div className="max-w-3xl mx-auto space-y-6 text-center">
          <h1 className="text-4xl font-bold tracking-tight text-black sm:text-5xl md:text-6xl">
            Atención médica excepcional para su comunidad
          </h1>
          <p className="text-lg text-black/80 md:text-xl">
            En nuestro hospital, nos dedicamos a brindar atención médica de la
            más alta calidad a pacientes de todas las edades. Nuestro equipo de
            profesionales experimentados está comprometido a brindar un trato
            personalizado y compasivo.
          </p>
          <Link
            onClick={redirectToAgendarCita}
            className="inline-flex h-10 items-center justify-center rounded-md bg-primary px-8 text-sm font-medium text-primary-foreground shadow transition-colors hover:bg-primary/90 focus-visible:outline-none focus-visible:ring-1 focus-visible:ring-ring disabled:pointer-events-none disabled:opacity-50"
            prefetch={false}
            href={""}
          >
            Agenda una cita!
          </Link>
        </div>
        <section className="py-12 md:py-24 lg:py-32">
          <div className="container mx-auto px-4 md:px-6">
            <div className="space-y-6 text-center">
              <div className="inline-block rounded-lg bg-muted px-3 py-1 text-sm text-black">
                Nuestros servicios
              </div>
              <h2 className="text-3xl font-bold tracking-tighter sm:text-4xl md:text-5xl text-black">
                Servicios médicos destacados
              </h2>
              <p className="max-w-[700px] mx-auto text-muted-foreground md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed text-black">
                Nuestro hospital ofrece una amplia gama de servicios médicos
                para satisfacer las diversas necesidades de nuestra comunidad.
                Desde chequeos de rutina hasta tratamientos especializados,
                estamos aquí para brindar una atención excepcional.
              </p>
            </div>
            <div className="mx-auto grid max-w-5xl grid-cols-1 gap-6 py-12 sm:grid-cols-2 lg:grid-cols-3">
              <div className="rounded-lg border bg-background p-6 shadow-sm">
                <div className="flex h-16 w-16 items-center justify-center rounded-full bg-primary">
                  <HeartPulseIcon className="h-8 w-8 text-primary-foreground" />
                </div>
                <h3 className="mt-4 text-xl font-semibold text-black">
                  Cardiología
                </h3>
                <p className="mt-2 text-muted-foreground text-black">
                  Nuestro departamento de cardiología brinda atención integral
                  para afecciones relacionadas con el corazón.
                </p>
              </div>
              <div className="rounded-lg border bg-background p-6 shadow-sm">
                <div className="flex h-16 w-16 items-center justify-center rounded-full bg-primary">
                  <TreesIcon className="h-8 w-8 text-primary-foreground" />
                </div>
                <h3 className="mt-4 text-xl font-semibold text-black">
                  Neumología
                </h3>
                <p className="mt-2 text-muted-foreground text-black">
                  Nuestro equipo de neumología está especializado en el
                  diagnóstico y tratamiento de enfermedades respiratorias.
                </p>
              </div>
              <div className="rounded-lg border bg-background p-6 shadow-sm">
                <div className="flex h-16 w-16 items-center justify-center rounded-full bg-primary">
                  <BrainIcon className="h-8 w-8 text-primary-foreground" />
                </div>
                <h3 className="mt-4 text-xl font-semibold text-black">
                  Neurología
                </h3>
                <p className="mt-2 text-muted-foreground text-black">
                  Nuestro departamento de neurología brinda atención avanzada
                  para afecciones que afectan el cerebro y el sistema nervioso.
                </p>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}
