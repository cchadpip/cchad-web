"use client";
import { getClient } from "@/config/graphql/apolloClient";
import { GET_BRANCHES2, CREATE_BRANCH, DELETE_BRANCH, DISABLE_BRANCH, ENABLE_BRANCH } from "@/config/graphql/mutations/serviceAdmin/service";
import { useEffect, useState } from "react";
import { Table, TableHeader, TableColumn, TableBody, TableRow, TableCell, Button, Tooltip, ModalContent, ModalHeader, ModalBody, ModalFooter, Modal, Input, useDisclosure } from "@nextui-org/react";
import { FaEdit, FaTimes } from "react-icons/fa";
import Swal from "sweetalert2";
import { Branch2 } from "@/config/Interface/admin";
import withAuth from "@/components/utils/withAuth";

const ManageBranchesPage = () => {
    const [branches, setBranches] = useState<Branch2[]>([]);
    const [newBranchName, setNewBranchName] = useState<string>("");
    const [newBranchAddress, setNewBranchAddress] = useState<string>("");
    const [newOpeningTime, setNewOpeningTime] = useState<string>("");
    const [newClosingTime, setNewClosingTime] = useState<string>("");

    const handleRequest = async () => {
        try {
            const client = getClient();
            const { data } = await client.query({
                query: GET_BRANCHES2,
            });
            setBranches(data.branches);
        } catch (error) {
            console.error("Error fetching branches:", error);
        }
    };

    useEffect(() => {
        handleRequest();
    }, []);

    const handleDisable = async (id: number) => {
        const result = await Swal.fire({
            title: '¿Estás seguro?',
            text: "¿Deseas deshabilitar esta sucursal?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, deshabilitar!'
        });

        if (result.isConfirmed) {
            try {
                const client = getClient();
                await client.mutate({
                    mutation: DISABLE_BRANCH,
                    variables: { id },
                });
                Swal.fire({
                    icon: 'success',
                    title: 'Sucursal deshabilitada',
                    showConfirmButton: false,
                    timer: 1500
                });
                handleRequest();
            } catch (error) {
                console.error("Error deshabilitando la sucursal:", error);
                Swal.fire({
                    icon: 'error',
                    title: 'Error deshabilitando la sucursal',
                    text: 'error.message',
                });
            }
        }
    };

    const handleEnable = async (id: number) => {
        const result = await Swal.fire({
            title: '¿Estás seguro?',
            text: "¿Deseas habilitar esta sucursal?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, habilitar!'
        });

        if (result.isConfirmed) {
            try {
                const client = getClient();
                await client.mutate({
                    mutation: ENABLE_BRANCH,
                    variables: { id },
                });
                Swal.fire({
                    icon: 'success',
                    title: 'Sucursal habilitada',
                    showConfirmButton: false,
                    timer: 1500
                });
                handleRequest();
            } catch (error) {
                console.error("Error habilitando la sucursal:", error);
                Swal.fire({
                    icon: 'error',
                    title: 'Error habilitando la sucursal',
                    text: 'error.message',
                });
            }
        }
    };

    const handleDelete = async (id: number) => {
        const result = await Swal.fire({
            title: '¿Estás seguro?',
            text: "No podrás revertir esto!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar!'
        });

        if (result.isConfirmed) {
            try {
                const client = getClient();
                await client.mutate({
                    mutation: DELETE_BRANCH,
                    variables: { id },
                });
                Swal.fire(
                    'Eliminado!',
                    'La sucursal ha sido eliminada.',
                    'success'
                );
                handleRequest();
            } catch (error) {
                console.error("Error eliminando la sucursal:", error);
                Swal.fire({
                    icon: 'error',
                    title: 'Error eliminando la sucursal',
                    text: 'Hubo un error eliminando la sucursal, por favor intenta nuevamente.',
                });
            }
        }
    };

    const { isOpen, onOpen, onOpenChange } = useDisclosure();
    
    const handleCreate = async () => {
        if (!newBranchName || !newBranchAddress || !newOpeningTime || !newClosingTime) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Por favor proporciona todos los datos de la sucursal.',
            });
            return;
        }

        try {
            const client = getClient();
            await client.mutate({
                mutation: CREATE_BRANCH,
                variables: {
                    input: {
                        name: newBranchName,
                        address: newBranchAddress,
                        openingTime: newOpeningTime,
                        closingTime: newClosingTime
                    }
                }
            });
            Swal.fire({
                icon: 'success',
                title: 'Sucursal creada exitosamente',
                showConfirmButton: false,
                timer: 1500
            });
            handleRequest();
            setNewBranchName("");
            setNewBranchAddress("");
            setNewOpeningTime("");
            setNewClosingTime("");
            onOpenChange();
        } catch (error) {
            console.error("Error creando la sucursal:", error);
            Swal.fire({
                icon: 'error',
                title: 'Error creando la sucursal',
                text: 'Hubo un error creando la sucursal, por favor intenta nuevamente.',
            });
        }
    }

    return (
        <div className="container mx-auto py-8 px-4 md:px-6">
            <div className="flex items-center justify-between mb-6">  
                <h1 className="text-2xl font-bold text-text">Sucursales</h1>
                <Button size="sm" className="bg-primary text-primary-foreground" onPress={onOpen}>
                    Crear Sucursal
                </Button>
                <Modal className="bg-white" isOpen={isOpen} onOpenChange={onOpenChange} title="Crear Sucursal">
                    <ModalContent>
                    {(onClose) => (
                            <>
                                <ModalHeader className="flex flex-col gap-1">
                                    Crear Sucursal
                                </ModalHeader>
                                <ModalBody>
                                    <label htmlFor="branch-name">Nombre de la Sucursal</label>
                                    <Input
                                        id="branch-name"
                                        placeholder="Ingrese el nombre de la sucursal"
                                        value={newBranchName}
                                        onChange={(e) => setNewBranchName(e.target.value)}
                                        variant="bordered"
                                    />  
                                    <label htmlFor="branch-address">Dirección de la Sucursal</label>
                                    <Input
                                        id="branch-address"
                                        placeholder="Ingrese la dirección de la sucursal"
                                        value={newBranchAddress}
                                        onChange={(e) => setNewBranchAddress(e.target.value)}
                                        variant="bordered"
                                    />
                                    <label htmlFor="opening-time">Hora de Apertura</label>
                                    <Input
                                        type="time"
                                        id="opening-time"
                                        placeholder="Ingrese la hora de apertura"
                                        value={newOpeningTime}
                                        onChange={(e) => setNewOpeningTime(e.target.value)}
                                        variant="bordered"
                                    />
                                    <label htmlFor="closing-time">Hora de Cierre</label>
                                    <Input
                                        type="time"
                                        id="closing-time"
                                        placeholder="Ingrese la hora de cierre"
                                        value={newClosingTime}
                                        onChange={(e) => setNewClosingTime(e.target.value)}
                                        variant="bordered"
                                    />
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="primary" variant="flat" onPress={handleCreate}>
                                        Crear
                                    </Button>
                                    <Button color="danger" variant="flat" onPress={onClose}>
                                        Cancelar
                                    </Button>          
                                </ModalFooter>
                            </>
                        )}
                    </ModalContent>
                </Modal>
            </div>
            <div className="border rounded-lg overflow-hidden">
                <Table 
                    aria-label="Lista de sucursales"
                    className="bg-white text-gray-800 border border-gray-200"
                    bgcolor="white"
                    classNames={{
                        table: "bg-white border border-gray-200",
                        th: "bg-gray-200 text-gray-800 border-b border-gray-200",
                        td: "bg-white text-gray-600 border-t border-gray-200",
                    }}
                >
                    <TableHeader>
                        <TableColumn>Nombre</TableColumn>
                        <TableColumn>Dirección</TableColumn>
                        <TableColumn>Hora de Apertura</TableColumn>
                        <TableColumn>Hora de Cierre</TableColumn>
                        <TableColumn>Estado</TableColumn>
                        <TableColumn>Acciones</TableColumn>
                    </TableHeader>
                    <TableBody emptyContent={"No hay sucursales para mostrar"}>
                        {branches.map((branch) => (
                            <TableRow key={branch.id}>
                                <TableCell>{branch.name}</TableCell>
                                <TableCell>{branch.address}</TableCell>
                                <TableCell>{branch.openingTime}</TableCell>
                                <TableCell>{branch.closingTime}</TableCell>
                                <TableCell>
                                    <span style={{ color: branch.enabled ? 'green' : 'red' }}>
                                        {branch.enabled ? 'Habilitada' : 'Deshabilitada'}
                                    </span>
                                </TableCell>
                                <TableCell>
                                    <div className="flex gap-2">
                                        <Tooltip content={branch.enabled ? "Deshabilitar" : "Habilitar"} color={branch.enabled ? "warning" : "success"} className="capitalize">
                                            <Button onPress={() => branch.enabled ? handleDisable(branch.id) : handleEnable(branch.id)} className="capitalize" isIconOnly color={branch.enabled ? "warning" : "success"}>
                                                <FaEdit />
                                            </Button>
                                        </Tooltip>
                                        <Tooltip content="Eliminar" color="danger" className="capitalize">
                                            <Button className="capitalize" isIconOnly color="danger" onPress={() => handleDelete(branch.id)}>
                                                <FaTimes />
                                            </Button>
                                        </Tooltip>
                                    </div>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>  
        </div>
    );
};

export default withAuth(ManageBranchesPage, ['Administrador']);
