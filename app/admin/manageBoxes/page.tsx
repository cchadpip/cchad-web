"use client";
import { getClient } from "@/config/graphql/apolloClient";
import {
  GET_BOXES,
  ENABLE_BOX,
  DELETE_BOX,
  DISABLE_BOX,
  GET_BRANCHES,
  CREATE_BOX,
} from "@/config/graphql/mutations/serviceAdmin/service";
import { useEffect, useState } from "react";
import {
  Table,
  TableHeader,
  TableColumn,
  TableBody,
  TableRow,
  TableCell,
  Button,
  Tooltip,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Modal,
  Input,
  Select,
  SelectItem,
  useDisclosure,
} from "@nextui-org/react";
import { FaEdit, FaTimes } from "react-icons/fa";
import { Box, Branch } from "@/config/Interface/admin";
import Swal from "sweetalert2";
import withAuth from "@/components/utils/withAuth";

const ManageBoxesPage = () => {
  const [boxes, setBoxes] = useState<Box[]>([]);
  const [branches, setBranches] = useState<Branch[]>([]);
  const [newBoxName, setNewBoxName] = useState<string>("");
  const [selectedBranchId, setSelectedBranchId] = useState<number | null>(null);

  const handleRequest = async () => {
    try {
      const client = getClient();
      const { data: boxesData } = await client.query({
        query: GET_BOXES,
      });
      setBoxes(boxesData.boxes);

      const { data: branchesData } = await client.query({
        query: GET_BRANCHES,
      });
      setBranches(branchesData.branches);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    handleRequest();
  }, []);

  const handleDisable = async (id: number) => {
    const result = await Swal.fire({
      title: "¿Estás seguro?",
      text: "¿Deseas desactivar este box?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sí, desactívalo!",
    });

    if (result.isConfirmed) {
      try {
        const client = getClient();
        await client.mutate({
          mutation: DISABLE_BOX,
          variables: { id },
        });
        Swal.fire({
          icon: "success",
          title: "Box desactivado",
          showConfirmButton: false,
          timer: 1500,
        });
        handleRequest();
      } catch (error) {
        console.error("Error desactivando el box:", error);
        Swal.fire({
          icon: "error",
          title: "Error desactivando el box",
          text: "Hubo un error desactivando el box, por favor intenta de nuevo.",
        });
      }
    }
  };

  const handleEnable = async (id: number) => {
    const result = await Swal.fire({
      title: "¿Estás seguro?",
      text: "¿Deseas activar este box?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sí, actívalo!",
    });

    if (result.isConfirmed) {
      try {
        const client = getClient();
        await client.mutate({
          mutation: ENABLE_BOX,
          variables: { id },
        });
        Swal.fire({
          icon: "success",
          title: "Box activado",
          showConfirmButton: false,
          timer: 1500,
        });
        handleRequest();
      } catch (error) {
        console.error("Error activando el box:", error);
        Swal.fire({
          icon: "error",
          title: "Error activando el box",
          text: "Hubo un error activando el box, por favor intenta de nuevo.",
        });
      }
    }
  };

  const handleDelete = async (id: number) => {
    const result = await Swal.fire({
      title: "¿Estás seguro?",
      text: "¡No podrás revertir esto!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Sí, elimínalo!",
    });

    if (result.isConfirmed) {
      try {
        const client = getClient();
        await client.mutate({
          mutation: DELETE_BOX,
          variables: { id },
        });
        Swal.fire("Eliminado!", "El box ha sido eliminado.", "success");
        handleRequest();
      } catch (error) {
        console.error("Error eliminando el box:", error);
        Swal.fire({
          icon: "error",
          title: "Error eliminando el box",
          text: "Hubo un error eliminando el box, por favor intenta de nuevo.",
        });
      }
    }
  };

  const { isOpen, onOpen, onOpenChange } = useDisclosure();

  const handleCreate = async () => {
    if (!newBoxName || !selectedBranchId) {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "Por favor proporciona un nombre válido para el box y selecciona una sucursal.",
      });
      return;
    }

    try {
      const client = getClient();
      await client.mutate({
        mutation: CREATE_BOX,
        variables: {
          input: {
            name: newBoxName,
            branchId: selectedBranchId,
          },
        },
      });
      Swal.fire({
        icon: "success",
        title: "Box creado exitosamente",
        showConfirmButton: false,
        timer: 1500,
      });
      handleRequest();
      setNewBoxName("");
      setSelectedBranchId(null);
      onOpenChange();
    } catch (error) {
      console.error("Error creando el box:", error);
      Swal.fire({
        icon: "error",
        title: "Error creando el box",
        text: "Hubo un error creando el box, por favor intenta de nuevo.",
      });
    }
  };

  return (
    <div className="container mx-auto py-8 px-4 md:px-6">
      <div className="flex items-center justify-between mb-6">
        <h1 className="text-2xl font-bold text-text">Boxes</h1>
        <Button
          size="sm"
          className="bg-primary text-primary-foreground"
          onPress={onOpen}
        >
          Crear Box
        </Button>
        <Modal
          className="bg-white"
          isOpen={isOpen}
          onOpenChange={onOpenChange}
          title="Crear Box"
        >
          <ModalContent>
            {(onClose) => (
              <>
                <ModalHeader className="flex flex-col gap-1">
                  Crear Box
                </ModalHeader>
                <ModalBody>
                  <label htmlFor="box-name">Nombre del Box</label>
                  <Input
                    id="box-name"
                    placeholder="Ingrese el nombre del box"
                    value={newBoxName}
                    onChange={(e) => setNewBoxName(e.target.value)}
                    variant="bordered"
                  />
                  <label htmlFor="branch-select">Sucursal</label>
                  <Select
                    id="branch-select"
                    label="Seleccione una sucursal"
                    value={selectedBranchId?.toString() || ""}
                    onChange={(e) =>
                      setSelectedBranchId(Number(e.target.value))
                    }
                    className="max-w-xs bg-white text-text"
                    variant="bordered"
                  >
                    {branches.map((branch) => (
                      <SelectItem
                        className="text-white"
                        key={branch.id}
                        value={branch.id.toString()}
                      >
                        {branch.name}
                      </SelectItem>
                    ))}
                  </Select>
                </ModalBody>
                <ModalFooter>
                  <Button color="primary" variant="flat" onPress={handleCreate}>
                    Crear
                  </Button>
                  <Button color="danger" variant="flat" onPress={onClose}>
                    Cancelar
                  </Button>
                </ModalFooter>
              </>
            )}
          </ModalContent>
        </Modal>
      </div>
      <div className="border rounded-lg overflow-hidden">
        <Table
          aria-label="Lista de boxes"
          className="bg-white text-gray-800 border border-gray-200"
          bgcolor="white"
          classNames={{
            table: "bg-white border border-gray-200",
            th: "bg-gray-200 text-gray-800 border-b border-gray-200",
            td: "bg-white text-gray-600 border-t border-gray-200",
          }}
        >
          <TableHeader>
            <TableColumn>Nombre</TableColumn>
            <TableColumn>Estado</TableColumn>
            <TableColumn>Sucursales</TableColumn>
            <TableColumn>Acciones</TableColumn>
          </TableHeader>
          <TableBody emptyContent={"No hay boxes para mostrar"}>
            {boxes.map((box) => (
              <TableRow key={box.id}>
                <TableCell>{box.name}</TableCell>
                <TableCell>
                  <span style={{ color: box.enabled ? "green" : "red" }}>
                    {box.enabled ? "Activado" : "Desactivado"}
                  </span>
                </TableCell>
                <TableCell>{box.branch.name}</TableCell>
                <TableCell>
                  <div className="flex gap-2">
                    <Tooltip
                      content={box.enabled ? "Desactivar" : "Activar"}
                      color={box.enabled ? "warning" : "success"}
                      className="capitalize"
                    >
                      <Button
                        onPress={() =>
                          box.enabled
                            ? handleDisable(box.id)
                            : handleEnable(box.id)
                        }
                        className="capitalize"
                        isIconOnly
                        color={box.enabled ? "warning" : "success"}
                      >
                        <FaEdit />
                      </Button>
                    </Tooltip>
                    <Tooltip
                      content="Eliminar"
                      color="danger"
                      className="capitalize"
                    >
                      <Button
                        className="capitalize"
                        isIconOnly
                        color="danger"
                        onPress={() => handleDelete(box.id)}
                      >
                        <FaTimes />
                      </Button>
                    </Tooltip>
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    </div>
  );
};

export default withAuth(ManageBoxesPage, ["Administrador"]);
