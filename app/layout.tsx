import "@/styles/globals.css";
import { Providers } from "./providers";
import { NavBar } from "@/components/navbar/navbar";
import clsx from "clsx";
import "react-big-calendar/lib/css/react-big-calendar.css";
import "../styles/reactBigCalendar.css";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en" suppressHydrationWarning>
      <head />
      <body
        className={clsx(
          "min-h-screen bg-background antialiased",
          "flex flex-col"
        )}
      >
        <Providers themeProps={{ attribute: "class", defaultTheme: "mytheme" }}>
          <NavBar />
          <main className="container mx-auto max-w-screen-2x1 pt-16 px-6 flex-grow min-h-screen ">
            {children}
          </main>
          <footer className="bg-secondary text-card p-4 text-center">
            &copy; 2024 Clinica Chad. Todos los derechos reservados.
          </footer>
        </Providers>
      </body>
    </html>
  );
}
