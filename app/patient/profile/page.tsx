"use client";
import { useEffect, useState } from "react";
import { Tabs, Tab, Card, CardBody, Skeleton } from "@nextui-org/react";
import ProfileDetails from "@/components/patient/profileData/profileDetails";
import AppointmentHistory from "@/components/medic/profile/appoinmentHistory";
import { getClient } from "@/config/graphql/apolloClient";
import { GET_PATIENT_FOR_PROFILE } from "@/config/graphql/mutations/servicePatient/service";
import useUserStore from "@/config/storage/user";
import withAuth from "@/components/utils/withAuth";

const ProfilePage = () => {
  const [patientData, setPatientData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const patientId = useUserStore((state) => state.id);

  const fetchPatientData = async () => {
    try {
      const { data } = await getClient().query({
        query: GET_PATIENT_FOR_PROFILE,
        variables: { id: patientId },
      });
      setPatientData(data.patient);
      setLoading(false);
    } catch (error: any) {
      setError(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (patientId) {
      fetchPatientData();
    } else {
      setLoading(false);
      setError(null);
    }
  }, [patientId]);

  if (loading) {
    return (
      <div className="container mx-auto p-4">
        <h1 className="text-2xl font-bold mb-4 text-primary">Mi Perfil</h1>
        <Tabs
          aria-label="Profile Tabs"
          size="lg"
          radius="md"
          className="w-full mb-4"
        >
          <Tab key="profile" title="Datos Personales" className="w-full">
            <Card className="mt-4 p-4">
              <CardBody>
                <div className="space-y-4">
                  <Skeleton className="w-full rounded-lg">
                    <div className="h-6 w-full rounded-lg bg-card mb-2"></div>
                  </Skeleton>
                  <Skeleton className="w-full rounded-lg">
                    <div className="h-6 w-full rounded-lg bg-card mb-2"></div>
                  </Skeleton>
                  <Skeleton className="w-full rounded-lg">
                    <div className="h-6 w-full rounded-lg bg-card mb-2"></div>
                  </Skeleton>
                  <Skeleton className="w-full rounded-lg">
                    <div className="h-6 w-full rounded-lg bg-card mb-2"></div>
                  </Skeleton>
                </div>
              </CardBody>
            </Card>
          </Tab>
        </Tabs>
      </div>
    );
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-3xl font-bold mb-4 text-black ">Mi Perfil</h1>
      {patientData ? (
        <ProfileDetails userData={patientData} />
      ) : (
        <div>No hay datos disponibles</div>
      )}
    </div>
  );
};

export default withAuth(ProfilePage, ["Paciente"]);
