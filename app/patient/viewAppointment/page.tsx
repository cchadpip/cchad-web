"use client";
import AppointmentView from "@/components/patient/patientViewAppointment/Appointment";
import withAuth from "@/components/utils/withAuth";

const view = () => {
  return (
    <div>
      <AppointmentView />
    </div>
  );
};
export default withAuth(view, ["Paciente"]);
