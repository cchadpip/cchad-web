"use client";
import useAppointmentLogic from "@/components/appointmentComponents/logicAppointment/useLogicAppointment";
import React, { useState } from "react";
import useUserStore from "@/config/storage/user";
import { DatePicker, Button } from "@nextui-org/react";
import { today, getLocalTimeZone } from "@internationalized/date";
import Swal from "sweetalert2";
import withAuth from "@/components/utils/withAuth";

const AppointmentPage = () => {
  const {
    medics,
    fetchMedic,
    medicSchedules,
    selectedSlot,
    setSelectedSlot,
    createAppointment,
    idUser,
  } = useAppointmentLogic();
  const [selectedDoctor, setSelectedDoctor] = useState<string | null>(null);
  const [selectedDate, setSelectedDate] = useState<string | null>(null);
  const [typeAppointment, setTypeAppointment] = useState([
    "Consulta",
    "Control",
    "Procedimiento",
  ]);
  const [selectedType, setSelectedType] = useState<string | null>(null);
  const [value, setValue] = useState(today(getLocalTimeZone()));
  const [isLoadingSearch, setIsLoadingSearch] = useState(false);
  const [isLoadingCreate, setIsLoadingCreate] = useState(false);

  const handleSearch = async () => {
    const formattedDate = formatDate(value);
    if (selectedDoctor && selectedDate) {
      setIsLoadingSearch(true);
      try {
        // Llama a fetchMedic con los parámetros adecuados y maneja la respuesta
        await fetchMedic(Number(selectedDoctor), selectedDate);
        const availableSlots = medicSchedules
          .flatMap((schedule) => schedule.slots)
          .filter(
            (slot) =>
              slot.enabled &&
              !slot.blocked &&
              (!slot.appointments || slot.appointments.length <= 2)
          );
        if (availableSlots.length === 0) {
        }
      } catch (error) {
        console.error("Error al buscar horarios:", error);
        Swal.fire({
          icon: "error",
          title: "Error al buscar horarios",
          text: "Hubo un problema al buscar los horarios. Inténtelo de nuevo más tarde.",
        });
      } finally {
        setIsLoadingSearch(false);
      }
    } else {
      Swal.fire({
        icon: "warning",
        title: "Campos incompletos",
        text: "Seleccione un médico y una fecha para buscar horarios.",
      });
    }
  };

  const formatDate = (date) => {
    const year = date.year;
    const month = date.month.toString().padStart(2, "0");
    const day = date.day.toString().padStart(2, "0");
    return `${year}-${month}-${day}`;
  };

  const handleDateChange = (date) => {
    setValue(date);
    const formattedDate = formatDate(date);
    console.log("Buscando médico con fecha:", formattedDate);
  };

  const handleSlotSelect = (slotId: number) => {
    setSelectedSlot(slotId);
  };

  const handleCreateAppointment = async () => {
    if (selectedSlot && selectedType && idUser) {
      setIsLoadingCreate(true);
      const appointmentData = {
        type: selectedType,
        patientId: parseFloat(idUser),
        slotId: selectedSlot,
      };

      console.log("Creando cita con los datos:", appointmentData);

      try {
        await createAppointment(appointmentData);
        Swal.fire({
          icon: "success",
          title: "Cita creada",
          text: "La cita ha sido agendada exitosamente.",
        }).then((result) => {
          if (result.isConfirmed) {
            window.location.href = "/";
          }
        });
      } catch (error) {
        console.error("Error al crear cita:", error);
        Swal.fire({
          icon: "error",
          title: "Error al crear cita",
          text: "Hubo un problema al agendar la cita. Inténtelo de nuevo más tarde.",
        });
      } finally {
        setIsLoadingCreate(false);
      }
    } else {
      Swal.fire({
        icon: "warning",
        title: "Campos incompletos",
        text: "Seleccione un tipo de cita y un horario disponible.",
      });
    }
  };

  const formatTime = (time: string) => {
    try {
      const date = new Date(time);
      if (isNaN(date.getTime())) {
        return "Fecha Inválida";
      }
      date.setHours(date.getHours() - 4);
      const hours = date.getUTCHours().toString().padStart(2, "0");
      const minutes = date.getUTCMinutes().toString().padStart(2, "0");
      return `${hours}:${minutes}`;
    } catch (error) {
      return "Fecha Inválida";
    }
  };

  const sortAndFilterSlots = (slots: any[]) => {
    return [...slots]
      .filter(
        (slot) =>
          slot.enabled &&
          !slot.blocked &&
          (!slot.appointments || slot.appointments.length === 0)
      )
      .sort((a, b) => {
        const timeA = new Date(JSON.parse(a.time)[0]);
        const timeB = new Date(JSON.parse(b.time)[0]);
        timeA.setHours(timeA.getHours() - 4);
        timeB.setHours(timeB.getHours() - 4);
        return timeA.getTime() - timeB.getTime();
      });
  };

  return (
    <div className="bg-white p-6 rounded-lg shadow-lg max-w-4xl mx-auto mt-8">
      <div className="grid md:grid-cols-2 gap-8">
        <div>
          <h1 className="text-2xl font-bold text-black">Agendar una Cita</h1>
          <p className="text-gray-500">
            Seleccione una fecha y un médico para ver los horarios disponibles.
          </p>
          <div className="mt-6 space-y-4">
            <div>
              <label htmlFor="date" className="block font-medium text-black">
                Fecha
              </label>
              <input
                type="date"
                id="date"
                value={selectedDate ?? ""}
                onChange={(e) => setSelectedDate(e.target.value)}
                className="w-full mt-1 p-2 border rounded text-white"
              />
              {/*<DatePicker
                value={value}
                aria-label="Fecha (Valor Máximo de Fecha)"
                defaultValue={today(getLocalTimeZone())}
                minValue={today(getLocalTimeZone())}
                onChange={handleDateChange}
              />*/}
            </div>
            <div>
              <label htmlFor="doctor" className="block font-medium text-black">
                Médico
              </label>
              <select
                id="doctor"
                value={selectedDoctor ?? ""}
                onChange={(e) => setSelectedDoctor(e.target.value)}
                className="w-full mt-1 p-2 border rounded text-white"
              >
                <option value="" disabled className="text-white">
                  Seleccione un médico
                </option>
                {medics.map((medic) => (
                  <option
                    className="text-white"
                    key={medic.id}
                    value={String(medic.id)}
                  >
                    Dr. {medic.name} {medic.lastName} - {medic.specialty}
                  </option>
                ))}
              </select>
            </div>
            <div>
              <label htmlFor="type" className="block font-medium text-black">
                Tipo de Cita
              </label>
              <select
                id="type"
                value={selectedType ?? ""}
                onChange={(e) => setSelectedType(e.target.value)}
                className="w-full mt-1 p-2 border rounded text-white"
              >
                <option value="" disabled>
                  Seleccione el tipo de cita
                </option>
                {typeAppointment.map((type) => (
                  <option className="text-white" key={type} value={type}>
                    {type}
                  </option>
                ))}
              </select>
            </div>
            <Button
              onClick={handleSearch}
              isLoading={isLoadingSearch}
              className="w-full mt-4  text-white p-2 rounded"
              color="primary"
            >
              Buscar
            </Button>
          </div>
        </div>
        <div>
          <h2 className="text-2xl font-bold text-black">
            Horarios Disponibles
          </h2>
          <p className="text-gray-500">
            Seleccione un horario disponible para reservar su cita.
          </p>
          <div className="mt-6 grid grid-cols-2 gap-4">
            {medicSchedules.map((schedule) =>
              sortAndFilterSlots(schedule.slots).map((slot) => {
                const startTime = JSON.parse(slot.time)[0];
                const formattedTime = formatTime(startTime);
                return (
                  <button
                    key={slot.id}
                    onClick={() => handleSlotSelect(slot.id)}
                    className={`p-2 border rounded ${
                      selectedSlot === slot.id
                        ? "bg-green-500 text-white"
                        : slot.enabled
                        ? "bg-teal-600 text-white"
                        : "bg-gray-300 text-gray-500"
                    }`}
                  >
                    {formattedTime}
                  </button>
                );
              })
            )}
          </div>
          <Button
            onClick={handleCreateAppointment}
            className="w-full mt-4 bg-green-500 text-white p-2 rounded"
            isLoading={isLoadingCreate}
          >
            {isLoadingCreate ? "Creando Cita..." : "Reservar Cita"}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default withAuth(AppointmentPage, ["Paciente"]);
