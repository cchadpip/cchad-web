"use client";
import React from "react";
import { Card, CardHeader, CardBody, Input, Button } from "@nextui-org/react";
import Swal from "sweetalert2";
import { gql } from "@apollo/client";
import { getClient } from "@/config/graphql/apolloClient";
import { ROLES } from "@/config/Interface/interface";

export default function ForgotPassword() {
  const [email, setEmail] = React.useState("");
  const [role, setRole] = React.useState<ROLES | null>(null);

  const ROLE_MAPPINGS: { [key in ROLES]: string } = {
    [ROLES.Admin]: "Administrador",
    [ROLES.Secretary]: "Secretaria",
    [ROLES.Patient]: "Paciente",
    [ROLES.Doctor]: "Médico",
  };

  const validateEmail = (email: string) =>
    email.match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i);

  const isInvalidEmail = React.useMemo(() => {
    if (email === "") return false;
    return validateEmail(email) ? false : true;
  }, [email]);

  const handlePasswordResetRequest = async () => {
    if (email === "" || !validateEmail(email)) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Please enter a valid email address.",
      });
      return;
    }

    try {
      const { data } = await getClient().mutate({
        mutation: gql`
          mutation RecoverPassword($input: recoverPasswordDto!) {
            recoverPassword(input: $input)
          }
        `,
        variables: {
          input: {
            email: email,
            role: "Paciente",
          },
        },
      });

      Swal.fire({
        icon: "success",
        title: "Check your email",
        text:
          "If an account exists for " +
          email +
          ", an email will be sent with further instructions.",
        confirmButtonText: "OK",
      }).then((result) => {
        if (result.isConfirmed) {
          window.location.href = "/";
        }
      });
    } catch (error) {
      // Adecuado manejar aquí el error
      console.error("Failed to send recovery email:", error);
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "Failed to send recovery email. Please try again.",
      });
    }
  };

  return (
    <Card
      style={{ backgroundColor: "black" }}
      className="mx-auto max-w-xs sm:max-w-sm md:max-w-md"
      radius="lg"
      shadow="lg"
      isBlurred
    >
      <CardHeader className="flex justify-center items-center">
        <h4>Recuperar Contraseña</h4>
      </CardHeader>
      <CardBody>
        <div className="flex flex-col items-center justify-center w-full gap-4 mb-2">
          <Input
            isRequired
            variant="bordered"
            label="Email"
            placeholder="Enter your email"
            className="max-w-xs"
            onValueChange={(value) => setEmail(value)}
            isInvalid={isInvalidEmail}
            color={isInvalidEmail ? "danger" : "secondary"}
            errorMessage={isInvalidEmail && "Please enter a valid email"}
          />
          <Button
            color="primary"
            className="mb-2 max-w-xs"
            onClick={handlePasswordResetRequest}
          >
            Enviar Contraseña Temporal
          </Button>
        </div>
      </CardBody>
    </Card>
  );
}
