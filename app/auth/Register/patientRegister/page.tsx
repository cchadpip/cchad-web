"use client";
import React, { useState } from "react";
import DatosPersonales from "@/components/Register/registerPacient/personalData";
import DatosCuenta from "@/components/Register/registerPacient/accountData";
import { Progress } from "@nextui-org/react";
import { Patient } from "@/config/Interface/interface";
import { getClient } from "@/config/graphql/apolloClient";
import Swal from "sweetalert2";
import { REGISTER_PATIENT } from "@/config/graphql/mutations/mutationsRegister/mutations";
import { ApolloError } from "@apollo/client";

const Page: React.FC = () => {
  const [pasoActual, setPasoActual] = useState<number>(1);
  const [datosPaciente, setDatosPaciente] = useState<Partial<Patient>>({
    name: "",
    lastName: "",
    rut: "",
    email: "",
    password: "",
    gender: undefined,
    birthdate: "",
    phone: "",
    address: "",
    forecast: undefined,
  });

  const siguientePaso = () => {
    if (pasoActual === 1) {
      const camposNecesarios = [
        "rut",
        "name",
        "lastName",
        "address",
        "phone",
        "birthdate",
        "gender",
        "forecast",
      ];
      const faltanCampos = camposNecesarios.some((campo) => {
        const valor = datosPaciente[campo as keyof Patient];
        return valor === undefined || valor === "";
      });

      if (faltanCampos) {
        Swal.fire({
          icon: "error",
          title: "Campos incompletos",
          text: "Por favor, completa todos los campos.",
        });
        return;
      }
    }

    setPasoActual(pasoActual + 1);
  };

  const pasoAnterior = () => setPasoActual(pasoActual - 1);

  const valorProgreso = pasoActual === 1 ? 50 : 100;

  const actualizarDatosPaciente = (nuevosDatos: Partial<Patient>) => {
    setDatosPaciente((prevDatos) => ({
      ...prevDatos,
      ...nuevosDatos,
    }));
  };

  function formatBirthdate(dateString: string): string {
    const date = new Date(dateString);
    return date.toISOString();
  }

  async function handleRegister() {
    const camposNecesarios = [
      "rut",
      "name",
      "lastName",
      "address",
      "email",
      "password",
      "phone",
      "birthdate",
    ];
    const faltanCampos = camposNecesarios.some(
      (campo) => !datosPaciente[campo as keyof Patient]
    );

    if (faltanCampos) {
      Swal.fire({
        icon: "error",
        title: "Campos incompletos",
        text: "Por favor, completa todos los campos obligatorios.",
      });
      return;
    }

    const birthdateFormatted = datosPaciente.birthdate
      ? formatBirthdate(datosPaciente.birthdate)
      : undefined;

    const inputData = {
      ...datosPaciente,
      birthdate: birthdateFormatted,
    };

    try {
      const { data } = await getClient().mutate({
        mutation: REGISTER_PATIENT,
        variables: {
          input: inputData,
        },
      });

      Swal.fire("Registrado correctamente", "", "success");
      window.location.href = "/";
      console.log(data);
    } catch (error) {
      if (error instanceof ApolloError) {
        if (error.message.includes("duplicate key value")) {
          const campoDuplicado = error.message.includes(
            "UQ_35c81f692a3381882be74636a77"
          )
            ? "RUT"
            : "Correo Electrónico";

          Swal.fire({
            icon: "error",
            title: "Registro duplicado",
            text: `El ${campoDuplicado} ya está registrado. Por favor, ingresa uno diferente.`,
          });
        } else if (error.message.includes("Failed to fetch")) {
          Swal.fire({
            icon: "error",
            title: "Servidor no disponible",
            text: "El servidor no está disponible en este momento. Por favor, inténtalo más tarde.",
          });
        } else {
          console.error("Error al registrar el paciente:", error);
        }
      }
    }
  }

  return (
    <div className="text-center">
      <h1 className="text-4xl font-bold text-gray-700 mb-8">
        Registro de Paciente
      </h1>
      <div className="flex justify-center items-center mb-4">
        <Progress
          isStriped
          aria-label="Cargando..."
          color="primary"
          value={valorProgreso}
          className="max-w-md"
        />
      </div>

      {pasoActual === 1 && (
        <DatosPersonales
          siguientepaso={siguientePaso}
          actualizarDatosPaciente={actualizarDatosPaciente}
          handleRegister={handleRegister}
          datosPaciente={datosPaciente}
        />
      )}
      {pasoActual === 2 && (
        <DatosCuenta
          pasoAnterior={pasoAnterior}
          actualizarDatosPaciente={actualizarDatosPaciente}
          handleRegister={handleRegister}
          datosPaciente={datosPaciente}
        />
      )}
    </div>
  );
};

export default Page;
