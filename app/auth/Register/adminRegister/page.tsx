"use client";
import React, { useState } from "react";
import { Input, Button, Avatar } from "@nextui-org/react";
import { getClient } from "@/config/graphql/apolloClient";
import { REGISTER_ADMIN } from "@/config/graphql/mutations/mutationsRegister/mutations";
import Swal from "sweetalert2";
import withAuth from "@/components/utils/withAuth";

const App = () => {
  const [adminData, setAdminData] = useState({
    name: "",
    lastName: "",
    email: "",
  });

  const actualizarDatosAdmin = (nuevosDatos: Partial<typeof adminData>) => {
    setAdminData((prevDatos) => ({
      ...prevDatos,
      ...nuevosDatos,
    }));
  };

  async function handleRegister() {
    if (Object.values(adminData).some((value) => value === "")) {
      Swal.fire({
        icon: "error",
        title: "Campos incompletos",
        text: "Por favor, completa todos los campos obligatorios.",
      });
      return;
    }

    try {
      const { data } = await getClient().mutate({
        mutation: REGISTER_ADMIN,
        variables: adminData,
      });

      Swal.fire(
        "Registro exitoso",
        "El administrador ha sido registrado exitosamente",
        "success"
      );
      window.location.href = "/";
      console.log(data);
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Error en el registro",
        text: "Hubo un problema al registrar el administrador. Por favor, inténtalo de nuevo.",
      });
      console.error("Error al registrar el administrador:", error);
    }
  }

  return (
    <div className="w-full max-w-3xl mx-auto bg-background rounded-lg shadow-lg p-6 md:p-8">
      <h1 className="text-4xl font-bold text-gray-700 mb-8 text-center">
        Registro de Administrador
      </h1>
      <div className="flex flex-col md:flex-row items-center md:items-start gap-6 md:gap-8">
        <div className="flex-shrink-0">
          <Avatar
            className="w-20 h-20 md:w-24 md:h-24 text-large"
            src="/placeholder-admin.jpg"
            showFallback
          />
        </div>
        <div className="flex-1 space-y-4 w-full">
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
            <Input
              size="lg"
              label="Nombre"
              value={adminData.name}
              onChange={(e) => actualizarDatosAdmin({ name: e.target.value })}
              variant="underlined"
            />
            <Input
              size="lg"
              label="Apellido"
              value={adminData.lastName}
              onChange={(e) =>
                actualizarDatosAdmin({ lastName: e.target.value })
              }
              variant="underlined"
            />
            <Input
              size="lg"
              type="email"
              label="Correo Electrónico"
              value={adminData.email}
              onChange={(e) => actualizarDatosAdmin({ email: e.target.value })}
              variant="underlined"
            />
          </div>
          <div className="flex justify-end mt-6">
            <Button color="primary" onClick={handleRegister}>
              Registrar Administrador
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withAuth(App, ["Administrador"]);
