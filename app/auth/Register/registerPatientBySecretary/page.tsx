"use client";
import React, { useState } from "react";
import { Input, Button, Select, SelectItem, Avatar } from "@nextui-org/react";
import Swal from "sweetalert2";
import { REGISTER_PATIENT_BY_SECRETARY } from "@/config/graphql/mutations/serviceSecretary/serviceCreatePatient/service";
import { getClient } from "@/config/graphql/apolloClient";
import withAuth from "@/components/utils/withAuth";

const PatientBySecretary = () => {
  const [formData, setFormData] = useState({
    name: "",
    lastName: "",
    rut: "",
    email: "",
    gender: "",
    birthdate: "",
    phone: "",
    address: "",
    forecast: "",
  });

  const genderOptions = [
    { label: "Masculino", value: "Masculino" },
    { label: "Femenino", value: "Femenino" },
  ];

  const forecastOptions = ["FONASA", "ISAPRE", "PARTICULAR"].map((option) => ({
    label: option,
    value: option,
  }));

  const handleInputChange = (field, value) => {
    setFormData((prev) => ({ ...prev, [field]: value }));
  };

  const handleRegisterPatient = async () => {
    if (Object.values(formData).some((field) => field === "")) {
      Swal.fire({
        icon: "error",
        title: "Campos incompletos",
        text: "Todos los campos son obligatorios.",
      });
      return;
    }

    try {
      const { data } = await getClient().mutate({
        mutation: REGISTER_PATIENT_BY_SECRETARY,
        variables: {
          input: formData,
        },
      });

      Swal.fire({
        icon: "success",
        title: "Paciente registrado",
        text: `El paciente ${formData.name} ${formData.lastName} ha sido registrado correctamente.`,
      });
      window.location.href = "/";
      // Clear the form after successful registration
      setFormData({
        name: "",
        lastName: "",
        rut: "",
        email: "",
        gender: "",
        birthdate: "",
        phone: "",
        address: "",
        forecast: "",
      });
    } catch (error) {
      console.error("Error al registrar el paciente:", error);
      Swal.fire({
        icon: "error",
        title: "Error al registrar el paciente",
        text: "Hubo un problema al registrar el paciente. Inténtelo de nuevo más tarde.",
      });
    }
  };

  return (
    <div className="w-full max-w-3xl mx-auto bg-background rounded-lg shadow-lg p-6 md:p-8">
      <h1 className="text-4xl font-bold text-gray-700 mb-8 text-center">
        Registro de Paciente
      </h1>
      <div className="flex flex-col md:flex-row items-center md:items-start gap-6 md:gap-8">
        <div className="flex-shrink-0">
          <Avatar
            className="w-20 h-20 md:w-24 md:h-24 text-large"
            src="/placeholder-patient.jpg"
            showFallback
          />
        </div>
        <div className="flex-1 space-y-4 w-full">
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
            <Input
              size="lg"
              label="Rut del paciente"
              variant="underlined"
              value={formData.rut}
              onChange={(e) => handleInputChange("rut", e.target.value)}
            />
            <Input
              size="lg"
              label="Nombre del Paciente"
              variant="underlined"
              value={formData.name}
              onChange={(e) => handleInputChange("name", e.target.value)}
            />
            <Input
              size="lg"
              label="Apellido del paciente"
              variant="underlined"
              value={formData.lastName}
              onChange={(e) => handleInputChange("lastName", e.target.value)}
            />
            <Input
              size="lg"
              label="Dirección"
              variant="underlined"
              value={formData.address}
              onChange={(e) => handleInputChange("address", e.target.value)}
            />
            <Select
              size="lg"
              label="Género"
              placeholder="Seleccione una opción"
              variant="underlined"
              value={formData.gender}
              onChange={(e) => handleInputChange("gender", e.target.value)}
            >
              {genderOptions.map((item) => (
                <SelectItem
                  className="bg-white"
                  key={item.value}
                  value={item.value}
                >
                  {item.label}
                </SelectItem>
              ))}
            </Select>
            <Input
              size="lg"
              label="N° de celular"
              variant="underlined"
              value={formData.phone}
              onChange={(e) => handleInputChange("phone", e.target.value)}
            />
            <Input
              size="lg"
              label="Fecha de nacimiento"
              variant="underlined"
              value={formData.birthdate}
              onChange={(e) => handleInputChange("birthdate", e.target.value)}
            />
            <Select
              size="lg"
              label="Previsión"
              placeholder="Seleccione una opción"
              variant="underlined"
              value={formData.forecast}
              onChange={(e) => handleInputChange("forecast", e.target.value)}
            >
              {forecastOptions.map((item) => (
                <SelectItem
                  className="bg-white"
                  key={item.value}
                  value={item.value}
                >
                  {item.label}
                </SelectItem>
              ))}
            </Select>
          </div>
          <Input
            size="lg"
            label="Correo Electrónico"
            variant="underlined"
            value={formData.email}
            onChange={(e) => handleInputChange("email", e.target.value)}
            className="w-full"
          />
          <div className="flex justify-end mt-6">
            <Button color="primary" onClick={handleRegisterPatient}>
              Registrar Paciente
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withAuth(PatientBySecretary, ["Secretaria"]);
