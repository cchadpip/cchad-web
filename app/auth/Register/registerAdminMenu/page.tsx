"use client";
import { Button } from "@nextui-org/react";
import NextLink from "next/link";
import withAuth from "@/components/utils/withAuth";

const registerMenu = () => {
  return (
    <div className="flex flex-row items-center justify-center mytheme">
      <div className="flex flex-col px-40">
        <span className="text-2xl font-bold text-purple-950 py-5">
          Registro de Administrador
        </span>
        <NextLink href="/auth/Register/adminRegister" passHref>
          <Button
            color="primary"
            variant="shadow"
            radius="md"
            size="lg"
            className="w-72"
          >
            Administrador
          </Button>
        </NextLink>
      </div>
      <div className="flex flex-col px-40 text-purple-950 items-center">
        <span className="text-2xl font-bold py-5">Registro de Secretaria</span>
        <NextLink href="/auth/Register/secretaryRegister" passHref>
          <Button
            color="primary"
            variant="shadow"
            radius="md"
            size="lg"
            className="w-72"
          >
            Secretaria
          </Button>
        </NextLink>
      </div>
    </div>
  );
};

export default withAuth(registerMenu, ["Administrador"]);
