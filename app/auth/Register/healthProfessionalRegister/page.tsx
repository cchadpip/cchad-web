"use client";
import React, { useState } from "react";
import { Input, Button, Select, SelectItem, Avatar } from "@nextui-org/react";
import { MedicSpecialty } from "@/config/Interface/ENUM";
import { getClient } from "@/config/graphql/apolloClient";
import Swal from "sweetalert2";
import { REGISTER_MEDIC } from "@/config/graphql/mutations/mutationsRegister/mutations";
import withAuth from "@/components/utils/withAuth";

const MedicRegister = () => {
  const [medicData, setMedicData] = useState({
    name: "",
    lastName: "",
    email: "",
    specialty: "",
  });

  const specialtyMapping: { [key in MedicSpecialty]: string } = {
    [MedicSpecialty.CARDIOLOGY]: "Cardiología",
    [MedicSpecialty.DERMATOLOGY]: "Dermatología",
    [MedicSpecialty.GASTROENTEROLOGY]: "Gastroenterología",
    [MedicSpecialty.NEUROLOGY]: "Neurología",
    [MedicSpecialty.GYNECOLOGY]: "Ginecología",
    [MedicSpecialty.OBSTETRICS]: "Obstetricia",
    [MedicSpecialty.OPHTHALMOLOGY]: "Oftalmología",
    [MedicSpecialty.PSYCHIATRY]: "Psiquiatría",
    [MedicSpecialty.INTERNALMEDICINE]: "Medicina Interna",
  };

  const specialties = Object.entries(MedicSpecialty).map(([key, value]) => ({
    value,
    label: specialtyMapping[value],
  }));

  const handleInputChange = (field: string, value: string) => {
    setMedicData((prev) => ({ ...prev, [field]: value }));
  };

  async function handleRegister() {
    if (Object.values(medicData).some((field) => field === "")) {
      Swal.fire({
        icon: "error",
        title: "Campos incompletos",
        text: "Todos los campos son obligatorios.",
      });
      return;
    }

    const specialtyNameInSpanish =
      specialtyMapping[medicData.specialty as MedicSpecialty];

    try {
      const { data } = await getClient().mutate({
        mutation: REGISTER_MEDIC,
        variables: {
          ...medicData,
          specialty: specialtyNameInSpanish,
        },
      });
      Swal.fire(
        "Registro exitoso",
        "El médico ha sido registrado correctamente",
        "success"
      );
      console.log(data);
      window.location.href = "/";
    } catch (error) {
      console.error("Error al registrar el médico:", error);
      Swal.fire({
        icon: "error",
        title: "Error de Registro",
        text: "No se pudo registrar al médico. Por favor, intente nuevamente.",
      });
    }
  }

  return (
    <div className="w-full max-w-3xl mx-auto bg-background rounded-lg shadow-lg p-6 md:p-8">
      <h1 className="text-4xl font-bold text-gray-700 mb-8 text-center">
        Registro de Personal Médico
      </h1>
      <div className="flex flex-col md:flex-row items-center md:items-start gap-6 md:gap-8">
        <div className="flex-shrink-0">
          <Avatar
            className="w-20 h-20 md:w-24 md:h-24 text-large"
            src="/placeholder-doctor.jpg"
            showFallback
          />
        </div>
        <div className="flex-1 space-y-4 w-full">
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
            <Input
              size="lg"
              label="Nombre"
              variant="underlined"
              value={medicData.name}
              onChange={(e) => handleInputChange("name", e.target.value)}
            />
            <Input
              size="lg"
              label="Apellido"
              variant="underlined"
              value={medicData.lastName}
              onChange={(e) => handleInputChange("lastName", e.target.value)}
            />
            <Input
              size="lg"
              type="email"
              label="Correo Electrónico"
              variant="underlined"
              value={medicData.email}
              onChange={(e) => handleInputChange("email", e.target.value)}
            />
            <Select
              size="lg"
              label="Especialidad Médica"
              placeholder="Seleccione una especialidad"
              variant="underlined"
              value={medicData.specialty}
              onChange={(e) => handleInputChange("specialty", e.target.value)}
            >
              {specialties.map((specialty) => (
                <SelectItem
                  variant="bordered"
                  className="bg-white"
                  key={specialty.value}
                  value={specialty.value}
                >
                  {specialty.label}
                </SelectItem>
              ))}
            </Select>
          </div>
          <div className="flex justify-end mt-6">
            <Button color="primary" onClick={handleRegister}>
              Registrar Médico
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withAuth(MedicRegister, ["Secretaria", "Administrador"]);
