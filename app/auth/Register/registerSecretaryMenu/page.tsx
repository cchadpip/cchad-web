import React from "react";
import { Button } from "@nextui-org/react";
import NextLink from "next/link";

// Nombrar explícitamente la función del componente
function RegisterOptions() {
  return (
    <div className="flex flex-row items-center justify-center mytheme">
      <div className="flex flex-col px-40">
        <span className="text-2xl font-bold text-purple-950 py-5 text-center">
          Registro de Paciente
        </span>
        <NextLink href="/auth/Register/registerPatientBySecretary" passHref>
          <Button
            as="a"
            color="primary"
            variant="shadow"
            radius="md"
            size="lg"
            className="w-72"
          >
            Paciente
          </Button>
        </NextLink>
      </div>
      <div className="flex flex-col px-40 text-purple-950 items-center">
        <span className="text-2xl font-bold py-5 text-center">
          Registro de Personal médico
        </span>
        <NextLink href="/auth/Register/healthProfessionalRegister" passHref>
          <Button
            as="a"
            color="primary"
            variant="shadow"
            radius="md"
            size="lg"
            className="w-72"
          >
            Médico
          </Button>
        </NextLink>
      </div>
    </div>
  );
}

// Exportar la función por defecto
export default RegisterOptions;
