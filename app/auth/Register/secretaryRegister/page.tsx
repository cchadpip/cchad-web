"use client";
import React, { useState } from "react";
import { Input, Button, Avatar } from "@nextui-org/react";
import { getClient } from "@/config/graphql/apolloClient";
import Swal from "sweetalert2";
import { REGISTER_SECRETARY } from "@/config/graphql/mutations/mutationsRegister/mutations";
import withAuth from "@/components/utils/withAuth";

const SecretaryRegister = () => {
  const [secretaryData, setSecretaryData] = useState({
    name: "",
    lastName: "",
    email: "",
  });

  const handleInputChange = (field: string, value: string) => {
    setSecretaryData((prev) => ({ ...prev, [field]: value }));
  };

  async function handleRegister() {
    if (Object.values(secretaryData).some((field) => field === "")) {
      Swal.fire({
        icon: "error",
        title: "Campos incompletos",
        text: "Todos los campos son obligatorios.",
      });
      return;
    }

    try {
      const { data } = await getClient().mutate({
        mutation: REGISTER_SECRETARY,
        variables: secretaryData,
      });

      Swal.fire(
        "Registro exitoso de Secretaria",
        "La secretaria ha sido registrada exitosamente",
        "success"
      );
      console.log(data);
      window.location.href = "/";
    } catch (error) {
      console.error("Error al registrar la secretaria:", error);
      Swal.fire({
        icon: "error",
        title: "Error de Registro",
        text: "No se pudo registrar a la secretaria. Por favor, intente nuevamente.",
      });
    }
  }

  return (
    <div className="w-full max-w-3xl mx-auto bg-background rounded-lg shadow-lg p-6 md:p-8">
      <h1 className="text-4xl font-bold text-gray-700 mb-8 text-center">
        Registro de Secretaria
      </h1>
      <div className="flex flex-col md:flex-row items-center md:items-start gap-6 md:gap-8">
        <div className="flex-shrink-0">
          <Avatar
            className="w-20 h-20 md:w-24 md:h-24 text-large"
            src="/placeholder-secretary.jpg"
            showFallback
          />
        </div>
        <div className="flex-1 space-y-4 w-full">
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
            <Input
              size="lg"
              label="Nombre"
              variant="underlined"
              value={secretaryData.name}
              onChange={(e) => handleInputChange("name", e.target.value)}
            />
            <Input
              size="lg"
              label="Apellido"
              variant="underlined"
              value={secretaryData.lastName}
              onChange={(e) => handleInputChange("lastName", e.target.value)}
            />
            <Input
              size="lg"
              type="email"
              label="Correo Electrónico"
              variant="underlined"
              value={secretaryData.email}
              onChange={(e) => handleInputChange("email", e.target.value)}
            />
          </div>
          <div className="flex justify-end mt-6">
            <Button color="primary" onClick={handleRegister}>
              Registrar Secretaria
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withAuth(SecretaryRegister, ["Administrador"]);
