"use client";
import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Image,
  Input,
  Button,
  Spacer,
} from "@nextui-org/react";
import Link from "next/link";
import { EyeFilledIcon, EyeSlashFilledIcon } from "@/components/incons";
import { useLogic } from "@/components/Login/useLogic";
import { ROLES } from "@/config/Interface/interface";
import png from "@/other/pngwing.com.png";
export default function Login() {
  const {
    setEmail,
    setPass,
    handleLogin,
    isInvalid,
    isInvalidPassword,
    toggleVisibility,
    isVisible,
  } = useLogic();

  return (
    <Card
      className="mx-auto max-w-xs sm:max-w-sm md:max-w-md bg-custom-newrichblack mytheme"
      radius="lg"
      shadow="lg"
      isBlurred
    >
      <CardHeader className="flex justify-center items-center">
        <h4 className="text-white">Login</h4>
      </CardHeader>
      <CardBody>
        <div className="flex items-center justify-center">
          <Image
            src="https://dynamic.brandcrowd.com/asset/logo/ac7ef50d-1f94-4b4a-8f13-fdd47b09362f/logo-search-grid-1x?logoTemplateVersion=1&v=638271683053470000"
            alt="login"
            width={200}
            height={200}
          />
        </div>
        <Spacer y={8} />
        <div className="flex flex-col items-center justify-center w-full gap-4 mb-2">
          <Input
            isRequired
            variant="flat"
            label="Email"
            placeholder="Enter your email"
            className="max-w-xs"
            onValueChange={setEmail}
            isInvalid={isInvalid}
            color={isInvalid ? "danger" : "primary"}
            errorMessage={isInvalid && "Please enter a valid email"}
          />
        </div>
        <div className="flex flex-col items-center justify-center w-full gap-4 mb-2">
          <Input
            isRequired
            label="Password"
            variant="flat"
            placeholder="Enter your password"
            onValueChange={setPass}
            isInvalid={isInvalidPassword} // Asegúrate que isInvalidPassword solo retorne true o false
            color={isInvalidPassword ? "danger" : "primary"}
            errorMessage={
              isInvalidPassword
                ? "Password must be at least 8 characters"
                : undefined
            } // Usar ternario para manejar el mensaje de error
            endContent={
              <button
                className="focus:outline-none"
                type="button"
                onClick={toggleVisibility}
              >
                {isVisible ? (
                  <EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none" />
                ) : (
                  <EyeFilledIcon className="text-2xl text-default-400 pointer-events-none" />
                )}
              </button>
            }
            type={isVisible ? "text" : "password"}
            className="max-w-xs"
          />
        </div>
        <Link
          className="inline-block px-4 py-2 flex justify-center items-center mb-2 text-white"
          href="/auth/recovery"
          color="primary"
        >
          Olvidaste tu contraseña?
        </Link>
        <div className="flex flex-col items-center justify-center w-full gap-2">
          <Button
            color="primary"
            className="mb-2 max-w-xs"
            onClick={() => handleLogin(ROLES.Patient)}
            variant="ghost"
          >
            Ingresar como paciente
          </Button>
          <Button
            color="primary"
            className="mb-2 max-w-xs"
            onClick={() => handleLogin(ROLES.Doctor)}
            variant="ghost"
          >
            Ingresar como medico
          </Button>
        </div>
      </CardBody>
    </Card>
  );
}
