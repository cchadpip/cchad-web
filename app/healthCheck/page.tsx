"use client"
import React, { useState, useEffect } from 'react';
import { gql } from '@apollo/client';
import { getClient } from '@/config/graphql/apolloClient';

const TESTING = gql`
  query {
    testQuery
  }
`;

const ServerStatus = () => {
  const [status, setStatus] = useState('Conectando con el servidor...');
  const [error, setError] = useState(null);

  const fetchServerStatus = async () => {
    try {
      const { data } = await getClient().query({ query: TESTING });
      if (data) {
        console.log('Respuesta del servidor:', data.testQuery);
        setStatus('Servidor conectado con éxito: ' + data.testQuery);
      }
    } catch (error) {
      console.error("Error al conectar con el servidor:", error);
      setStatus('Error al conectar con el servidor');
    }
  };

  useEffect(() => {
    fetchServerStatus();
  }, []);

  return (
    <div className='text-black'>
      {error ? <p>{error}</p> : <p>{status}</p>}
    </div>
  );
};

export default ServerStatus;
