// validationTime.ts
export function toISOWithTimezone(date: Date, timeZone: string): string {
  const options: Intl.DateTimeFormatOptions = {
    timeZone,
    hour12: false,
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    fractionalSecondDigits: 3
  };

  const parts = new Intl.DateTimeFormat('en-US', options).formatToParts(date);

  const formatPart = (type: Intl.DateTimeFormatPartTypes): string => {
    const part = parts.find(part => part.type === type);
    if (!part) {
      throw new Error(`Missing part type: ${type}`);
    }
    return part.value;
  };

  const year = formatPart('year');
  const month = formatPart('month');
  const day = formatPart('day');
  const hour = formatPart('hour');
  const minute = formatPart('minute');
  const second = formatPart('second');
  const millisecond = '000';

  return `${year}-${month}-${day}T${hour}:${minute}:${second}.${millisecond}Z`;
}

export function getTimeRangeInTimeZone(start: string, end: string, timeZone: string): string {
  const startDate = new Date(start);
  const endDate = new Date(end);

  const startISO = toISOWithTimezone(startDate, timeZone);
  const endISO = toISOWithTimezone(endDate, timeZone);


  return `${startISO};${endISO}`;
}
