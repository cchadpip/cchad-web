export type Appointment = {
    id: number;
    name: string;
    time: string;
    type: string;
    state : string;
    gender: string;
    rut: string;
    birthdate: string;
};
export type Schedule = {
    time: string;
    box: {
        name: string;
        };
        slots: {
        id: number;
        time: string;
        blocked: boolean;
        enabled: boolean;
        appointments: {
            id: number;
            state: string;
            confirmed: boolean;
            type: string;
            prescriptionDrugs: string;
            patient: {
                id: number;
                name: string;
                lastName: string;
                gender: string;
                rut: string;
                birthdate: string;
            };
        }[];
    }[];
};  
export type Medic = {
    schedules: Schedule[];
}; 
export type TodayAppointmentsProps = {
    appoinmentData: Medic | null;
};
export type Patient = {
    name: string;
    lastName: string;
    rut: string;
    email: string;
    gender: string;
    birthdate: string;
    phone: string;
  };
  
export type Appointments = {
    state: string;
    type: string;
    diagnosis: string | null;
    treatment: string | null;
    prescriptionDrugs: string | null;
    patient: Patient;
    time: string;
  };
  
export type Slot = {
    time: string;
    appointments: Appointments[];
  };
  
export  type Schedules = {
    slots: Slot[];
  };
  
export  type Medics = {
    id: number;
    name: string;
    lastName: string;
    email: string;
    specialty: string;
    enabled: boolean;
    schedules: Schedules[];
  };
  
export  type AppointmentHistoryProps = {
    userData: Medics | null;
  };
  