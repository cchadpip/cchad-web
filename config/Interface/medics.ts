export interface Slot {
    id: number;
    time: string;
    blocked: boolean;
    enabled: boolean;
    appointments: any[];
  }
  
export interface Schedule {
    box: { name: string };
    slots: Slot[];
  }
  
export interface AppoinmentData {
    schedules: Schedule[];
  }
  