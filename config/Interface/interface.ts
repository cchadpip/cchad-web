import { AppointmentState, AppointmentType, MedicSpecialty, PatientForecast, PatientGender } from "./ENUM";

export interface Slot {
    time: string;
    blocked: boolean;
    enabled: boolean;
    id: number;
    appointments: Appointment[];
    schedule: Schedule;
}
export interface Appointment {
    id: number;
    state: AppointmentState;
    confirmed: boolean;
    type: AppointmentType;
    diagnosis: string;
    treatment: string;
    prescriptionDrugs: string;
    patient: Patient;
    slot: Slot;
    createAt: string; // DateTime como string
    updateAt: string; // DateTime como string
}

export interface CalendarEvent {
  id: number;
  title: string;
  start: Date;
  end: Date;
  color: string;
  slot?: Slot; // Hacer que slot sea opcional
  [key: string]: any;
}


export interface SecretaryInput {
    name: string;
    lastName: string;
    email: string;
}

export interface Patient {
    id: number;
    name: string;
    lastName: string;
    rut: string;
    email: string;
    password: string;
    gender: PatientGender;
    birthdate: string;
    phone: string;
    address: string;
    forecast: PatientForecast;
    enabled: boolean;
    appointments: Appointment[];
    createAt: string;
    updateAt: string;
}

export interface User{
    id : number;
    rut : string;
    name: string;
    lastName: string;
}
export interface Medic {
    id: number;
    name: string;
    lastName: string;
    email: string;
    password: string;
    specialty: string;
    enabled: boolean;
    schedules: Schedule[];
    createAt: string;
    updateAt: string;
}

interface Secretary {
    id: number;
    name: string;
    lastName: string;
    email: string;
    password: string;
    enabled: boolean;
    createAt: string;
    updateAt: string;
}

interface Admin {
    id: number;
    name: string;
    lastName: string;
    email: string;
    enabled: boolean;
    createAt: string;
    updateAt: string;
}

export interface Schedule {
    id: number;
    medic: Medic;
    box: Box;
    enable: boolean;
    slots: Slot[]; // Asumiendo que ya has definido la interfaz Slot
    createAt: string; // DateTime como string
    updateAt: string; // DateTime como string
    time: string;
    public: boolean;
}

export interface Box {
    id: number;
    name: string;
    enabled: boolean;
    schedules: Schedule[]; // Asume que Schedule ya ha sido definido.
    branch: Branch; // Utiliza la interfaz Branch aquí
    createAt: string;
    updateAt: string;
}

interface Branch {
    id: number;
    name: string;
    address: string;
    enabled: boolean;
    boxes: Box[]; // Esta es una referencia circular, asegúrate de que Box esté definido antes o simplemente referenciado.
    createAt: string; // DateTime como string, podría necesitar manipulación para Date
    updateAt: string; // DateTime como string
}


export enum ROLES {
    Admin = "ADMIN",
    Secretary = "SECRETARY",
    Patient = "PATIENT",
    Doctor = "DOCTOR",
}


