import { Patient, Slot } from "./interface";


export interface Appointment {
    id: number;
    state: AppointmentState;
    confirmed: boolean;
    type: AppointmentType;
    diagnosis?: string;
    treatment?: string;
    prescriptionDrugs?: string;
    patient: Patient;
    slot: Slot;
    createAt: string;
    updateAt: string;
}

export enum AppointmentState {
  Reservada = 'Reservada',
  Reprogramada = 'Reprogramada',
  Completada = 'Completada'
}

export enum AppointmentType {
  Consulta = 'Consulta',
  Control = 'Control',
  Procedimiento = 'Procedimiento'
}