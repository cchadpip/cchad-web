export interface Branch {
    id: number;
    name: string;
    addres: string;
}

export interface Box {
    id: number;
    name: string;
    enabled: boolean;
    branch: Branch;
}

export interface Branch2 {
    id: number;
    name: string;
    address: string;
    openingTime: string;
    closingTime: string;
    enabled: boolean;
}