import { gql } from '@apollo/client';
const getMedicForSecretaryQuery = `
  query getMedicForSecretaryQuery($id: Int!) {
    medic(id: $id) {
        name
        lastName
        specialty
        schedules {
          public
          time
          box {
            name
            branch {
              name
            }
          }
          slots {
            time
            blocked
            appointments {
              id
              state
              confirmed
              type
              patient {
                id
                name
                lastName                
              }
            }
          }
        }
    }
  }
`;