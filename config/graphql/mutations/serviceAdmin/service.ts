import { gql } from "@apollo/client";

export const GET_BOXES = gql`
    query GetBoxes {
        boxes {
            id
            name
            enabled
            branch{
                id
                name
                address
            }
        }
    }
`;

export const CREATE_BOX = gql`
    mutation CreateBox($input: CreateBoxDto!) {
        createBox(input: $input) {
            id
            name
            branch {
                id
                name
            }
        }
    }
`;

export const DELETE_BOX = gql`
    mutation DeleteBox($id: Float!) {
        deleteBox(id: $id)
    }
`;

export const DISABLE_BOX = gql`
    mutation DisableBox($id: Float!) {
        disableBox(id: $id) {
            id
            name
            enabled
        }
    }
`;

export const ENABLE_BOX = gql`
    mutation EnableBox($id: Float!) {
        enableBox(id: $id) {
        id
        name
        enabled
        }
    }
`;

export const GET_BRANCHES = gql`
    query GetBranches {
        branches {
            id
            name
            address
        }
    }
`;

export const GET_BRANCHES2 = gql`
    query GetBranches {
        branches {
            id
            name
            address
            openingTime
            closingTime
            enabled
        }
    }
`;


export const CREATE_BRANCH = gql`
    mutation CreateBranch($input: CreateBranchDto!) {
        createBranch(input: $input) {
            id
            name
            address
            openingTime
            closingTime
            enabled
        }
    }
`;


export const DELETE_BRANCH = gql`
    mutation DeleteBranch($id: Float!) {
        deleteBranch(id: $id)
    }
`;


export const DISABLE_BRANCH = gql`
    mutation DisableBranch($id: Float!) {
        disableBranch(id: $id) {
            id
            enabled
        }
    }
`;


export const ENABLE_BRANCH = gql`
    mutation EnableBranch($id: Float!) {
        enableBranch(id: $id) {
            id
            enabled
        }
    }
`;