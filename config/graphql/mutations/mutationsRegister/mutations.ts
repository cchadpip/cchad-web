// cpnfig/graphql/mutations/mutationsRegister/mutations.ts

import { gql } from '@apollo/client';

export const REGISTER_SECRETARY = gql `
    mutation RegisterSecretary($name: String!, $lastName: String!, $email: String!) {
        registerSecretary(
            input: {
                name: $name
                lastName: $lastName
                email: $email
            }
        ) {
            id
            name
        }
    }
`;
 

export const REGISTER_PATIENT = gql `
        mutation RegisterPatient($input: CreatePatientDto!) {
            registerPatient(input: $input) {
                    name
                    lastName
                    email
                    password
                    rut
                    gender
                    birthdate
                    phone
                    address
                    forecast
            }
        }
    `;


export const REGISTER_MEDIC = gql `
    mutation RegisterMedic($name: String!, $lastName: String!, $email: String!, $specialty: String!) {
        registerMedic(input: {
            name: $name
            lastName: $lastName
            email: $email
            specialty: $specialty
        }) {
            id
            name
        }
    }
`;


export const REGISTER_ADMIN = gql `
    mutation RegisterAdmin($name: String!, $lastName: String!, $email: String!) {
        registerAdmin(
            input: {
                name: $name
                lastName: $lastName
                email: $email
            }
        ) {
            id
            name
        }
    }
`;