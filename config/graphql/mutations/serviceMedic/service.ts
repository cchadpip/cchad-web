import { gql } from  '@apollo/client'
//obtener los datos del medico agregando todas sus citas
export const GET_MEDIC =gql`
query GetSpecificMedic($id: Int!) {
    medic(id: $id) {
        id
        name
        lastName
        email
        specialty
        enabled
        schedules{
                slots{
                    time
                    appointments{
                        state
                        type
                        diagnosis
                        treatment
                        prescriptionDrugs
                        patient{
                            name
                            lastName
                            rut
                            email
                            gender
                            birthdate
                            phone
                        }
                    }
                }
            }
    }
}
`;
//obtener el horario del medico
export const GET_MEDIC_SCHEDULE =gql`
query GetSpecificMedic($id: Int!) {
    medic(id: $id) {
        id
        name
        lastName
        email
        specialty
        enabled
        schedules{
            time  
            public
            box{
                id 
                name
                enabled
                branch{
                    id
                    name
                }
            }
            slots{
                id 
                time
                blocked
                enabled
            }
        } 
    }
}
`;

//obtener las citas del dia
export const GET_DAILY_APPOINTMENTS  =gql`
query GetDailyAppoinmets($id: Int!) {
    medic(id: $id) {
        schedules{
            time  
            box{
                name
            }
            slots{
                id
                time
                blocked
                enabled
                appointments{
                    id
                    state
                    confirmed
                    type
                    prescriptionDrugs
                    patient{
                        id
                        name
                        lastName
                        rut
                        gender
                        birthdate
                    }
                }
            }
        }
    }
}
`;

export const GET_PATIENT_DATA = gql`
query GetPatientData($id: Int!) {
    patient(id: $id) {
        name
        lastName
        rut
        email
        gender
        birthdate
        phone
        forecast
        appointments{
            state
            type
            diagnosis
            treatment
            prescriptionDrugs
        }
    }
}
`;

export const UPDATE_MEDICAL_RECORD = gql`
    mutation UpdateMedicalRecord($id: Float!, $input: UpdateMedicalRecordDto!) {
        updateMedicalRecord(id: $id, input: $input) {
            id
            diagnosis
            treatment
            prescriptionDrugs
        }
}
`;

export const COMPLETE_APPOINTMENT = gql`
    mutation CompleteAppointment($id: Float!) {
        completeAppointment(id: $id) {
        id
        state
        }
    }
`;

export const CANCEL_APPOINTMENT = gql`
  mutation CancelAppointment($id: Float!) {
    cancelAppointment(id: $id)
  }
`;

export const RESCHEDULE_APPOINTMENT = gql`
  mutation RescheduleAppointment($input: RescheduleAppointmentDto!) {
    rescheduleAppointment(input: $input) {
      id
    }
  }
`;
