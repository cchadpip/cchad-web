import { gql } from '@apollo/client';

export const AUTHENTICATE_USER = gql `
    mutation AuthenticateUser($email: String!, $password: String!, $role: String!) {
        authenticateUser(input: { email: $email, password: $password, role: $role }) {
            token
            user {
                admin {
                    email
                    id
                }
                patient {
                    email
                    id
                }
                medic {
                    id
                    email
                }
            }
        }
    }
`;
