import { gql } from "@apollo/client";


export const GET_PATIENT = gql `
    query GetPatient($id: Int!) {
        patient(id: $id) {
            id
            name
            rut
            lastName
            appointments {
                id
                patient{
                    name
                }
                slot{
                    time
                    schedule{
                        medic{
                            name
                            specialty
                        }
                    }
                }
                diagnosis
                treatment
                prescriptionDrugs
                state
                confirmed
                type
            }
        }
    }
`;

export const GET_PATIENT_FOR_PROFILE = gql `
    query GetPatient($id: Int!) {
        patient(id: $id) {
            id
            name
            rut
            lastName
            email
            phone
            birthdate
        }
    }
`;


export const GET_PATIENTS = gql `
    query GetPatients {
        patients {
            id
            name
            rut
            lastName
        }
    }
`;	