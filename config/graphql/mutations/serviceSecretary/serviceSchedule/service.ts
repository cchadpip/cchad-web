import { gql } from '@apollo/client';

export const GET_MEDIC_FOR_SECRETARY = gql `
  query getMedicForSecretaryQuery($id: Int!) {
    medic(id: $id) {
      name
      lastName
      specialty
      schedules {
        public
        time
        box {
          name
          branch {
            name
          }
        }
        slots {
          id
          schedule {
            id
          }
          time
          blocked
          enabled
          appointments {
            id
            state
            confirmed
            type
            patient {
              id
              name
              lastName
            }
          }
        }
      }
    }
  }
`;



export const GET_BOX = gql `
  query getBox($id: Int!) {
    box(id: $id) {
      id
      name
      enabled
      schedules {
        id
        time
        public
        slots {
          time
          blocked
          enabled
        }
      }
      branch {
        id
        name
      }
      createAt
      updateAt
    }
  }
`;

export const GET_BOXES = gql `
  query {
    boxes { 
      id
      name
    }
  }
`;

export const GET_MEDICS_FOR_SECRETARY = gql `
  query {
    medics {
      id
      name
      lastName
      specialty
    }
  }
`;


export const UNLOCK_SLOT_MUTATION = gql `
mutation unlockSlotMutation($id: Float!) {
  unlockSlot(id: $id) {
    id
  }
}
`;


export const BLOCK_SLOT_MUTATION = gql `
mutation blockSlotMutation($id: Float!) {
  blockSlot(id: $id) {
    id
  }
}
`;


export const CREATE_MEDIC_SCHEDULES_MUTATION = gql `
  mutation CreateMedicSchedule($schedule: CreateMedicScheduleInput!) {
    createMedicSchedule(input: $schedule) {
      id
    }
  }
`;

export const PUBLIC_MEDIC_SCHEDULE_MUTATION = gql `
  mutation PublicMedicSchedule {
    publishSchedules
  }
`;

export const DELETE_MEDIC_SCHEDULE_MUTATION = gql `
  mutation DeleteMedicSchedule($id: Float!) {
    deleteMedicSchedule(id: $id)
  }
`;