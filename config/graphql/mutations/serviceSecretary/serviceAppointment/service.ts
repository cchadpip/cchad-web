import { GET_PATIENT } from './../../servicePatient/service';
import { gql } from '@apollo/client';

export const GET_MEDICS_FOR_SECRETARY = gql `
  query {
    medics {
      id
      name
      lastName
      specialty
    }
  }
`;


export const GET_MEDIC_FOR_SECRETARY = gql `
  query getMedicForSecretaryQuery($id: Int!) {
    medic(id: $id) {
      name
      lastName
      specialty
      schedules {
        public
        time
        box {
          name
          branch {
            name
          }
        }
        slots {
          id
          schedule {
            id
          }
          time
          blocked
          enabled
          appointments {
            id
            state
            confirmed
            type
            patient {
              id
              name
              lastName
            }
          }
        }
      }
    }
  }
`;
export const BOOK_APPOINTMENT = gql`
  mutation BookAppointment($input: CreateAppointmentDto!) {
    bookAppointment(input: $input) {
      id
    }
  }
`;

export const CONFIRM_APPOINTMENT = gql `
  mutation confirmAppointment($id: Float!) {
    confirmAppointment(id: $id) {
      id
      confirmed
    }
  }
`;

export const CANCEL_APPOINTMENT = gql `
  mutation cancelAppointment($id: Float!) {
    cancelAppointment(id: $id)
  }
`;

export const RESCHEDULE_APPOINTMENT = gql `
  mutation rescheduleAppointment($input: RescheduleAppointmentDto!) {
    rescheduleAppointment(input: $input) {
      id
      confirmed
    }
  }
`;

export const GET_PATIENTS = gql `
  query {
    patients {
      id
      name
      lastName
    }
  }
`;

