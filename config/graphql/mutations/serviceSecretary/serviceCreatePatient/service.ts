import { gql } from "@apollo/client";


export const REGISTER_PATIENT_BY_SECRETARY = gql`
    mutation RegisterPatientBySecretary($input: CreatePatientBySecretaryOrAdminDto !) {
        registerPatientBySecretaryOrAdmin(input: $input) {
            name
            lastName
            email
            rut
            gender
            birthdate
            phone
            address
            forecast
        }
    }
`;  