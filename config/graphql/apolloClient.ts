import { ApolloClient, InMemoryCache, HttpLink } from '@apollo/client';

// Crear la instancia de Apollo Client
const createApolloClient = () => {
  return new ApolloClient({
    link: new HttpLink({
      uri: `${process.env.NEXT_PUBLIC_API_URL}`, // Asegúrate de que la URI incluya el endpoint de GraphQL
    }),
    cache: new InMemoryCache(),
  });
};

export const getClient = createApolloClient;
