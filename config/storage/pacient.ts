import { create } from 'zustand';
import { persist } from 'zustand/middleware';


interface PacientState {
    id: number | null;
    idConsultation: number | null;
    setId: (id: number) => void;
    setIdConsultation: (id: number) => void;
    clearPacient: () => void;
}

const usePacientStore = create<PacientState>()(
    persist(
        (set) => ({
        id: null,
        idConsultation: null,
        setIdConsultation: (id: number) => set({ idConsultation: id }),
        setId: (id: number) => set({ id }),
        clearPacient: () => set({ id: null , idConsultation: null}),
        }),
        {
        name: 'pacient-storage', // Nombre para el storage (localStorage o sessionStorage)
        getStorage: () => localStorage, // Puedes cambiar a sessionStorage si prefieres
        }
    )
);

export default usePacientStore;