import { create } from 'zustand';
import { persist } from 'zustand/middleware';

interface UserState {
  id: string | null;
  token: string | null;
  type: string | null;
  setId: (id: string) => void;
  setToken: (token: string) => void;
  setType: (type: string) => void;
  clearUser: () => void;
}

const useUserStore = create<UserState>()(
  persist(
    (set) => ({
      id: null,
      token: null,
      type: null,
      setId: (id: string) => set({ id }),
      setToken: (token: string) => set({ token }),
      setType: (type: string) => set({ type }),
      clearUser: () => set({ id: null, token: null, type: null }),
    }),
    {
      name: 'user-storage', // Nombre para el storage (localStorage o sessionStorage)
      getStorage: () => localStorage, // Puedes cambiar a sessionStorage si prefieres
    }
  )
);

export default useUserStore;
