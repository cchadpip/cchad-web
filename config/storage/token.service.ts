// storage.js

const TOKEN_STORAGE_KEY = '@accessToken';
const TYPE_USER = '@typeUser';

export const storeToken = (token) => {
  if (typeof window !== 'undefined') { // Verifica si estamos en el navegador
    try {
      localStorage.setItem(TOKEN_STORAGE_KEY, token);
    } catch (e) {
      console.error("Error al guardar el token", e);
    }
  }
};

export const getToken = () => {
  if (typeof window !== 'undefined') { // Verifica si estamos en el navegador
    try {
      return localStorage.getItem(TOKEN_STORAGE_KEY);
    } catch (e) {
      console.error("Error al obtener el token", e);
      return null;
    }
  }
  return null;
};

export const removeToken = () => {
  if (typeof window !== 'undefined') { // Verifica si estamos en el navegador
    try {
      localStorage.removeItem(TOKEN_STORAGE_KEY);
    } catch (e) {
      console.error("Error al remover el token", e);
    }
  }
};

export const storeTypeUser = (typeUser) => {
  if (typeof window !== 'undefined') { // Verifica si estamos en el navegador
    try {
      localStorage.setItem(TYPE_USER, typeUser);
    } catch (e) {
      console.error("Error al guardar el tipo de usuario", e);
    }
  }
};

export const getTypeUser = () => {
  if (typeof window !== 'undefined') { // Verifica si estamos en el navegador
    try {
      return localStorage.getItem(TYPE_USER);
    } catch (e) {
      console.error("Error al obtener el tipo de usuario", e);
      return null;
    }
  }
  return null;
};

export const removeTypeUser = () => {
  if (typeof window !== 'undefined') { // Verifica si estamos en el navegador
    try {
      localStorage.removeItem(TYPE_USER);
    } catch (e) {
      console.error("Error al remover el tipo de usuario", e);
    }
  }
};
